
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Under Maintenance</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('template/assets/images/logoicon.png') }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Datta Able Bootstrap admin template made using Bootstrap 4 and it has huge amount of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
    <meta name="keywords" content="admin templates, bootstrap admin templates, bootstrap 4, dashboard, dashboard templets, sass admin templets, html admin templates, responsive, bootstrap admin templates free download,premium bootstrap admin templates, datta able, datta able bootstrap admin template">
    <meta name="author" content="Codedthemes" />

    <link rel="stylesheet" href="{{ asset('template/assets/fonts/fontawesome/css/fontawesome-all.min.css') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/plugins/animation/css/animate.min.css') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/css/style.css') }}">
</head>
<body>

<div class="auth-wrapper offline">
    <div class="text-center">
        <h1 class="mb-4">OFFLINE</h1>
        <h5 class="text-muted mb-4">Oops! Website Is Temporarily Offline</h5>
    </div>
</div>

</body>
</html>
