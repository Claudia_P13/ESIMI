<!DOCTYPE html>
<html lang="en">
<head>
    <title>e-SIMI | Data Permohonan Surat Izin Masuk Instalasi (SIMI)</title>
    <link rel="icon" type="image/png" href="{{ asset('template/assets/images/logoicon.png') }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script>
        //window.print()
    </script>

    <style>
        @media print {
            .cetak {
                visibility: hidden;
            }
        }
        .model-huruf-family {
            font-family: "Trebuchet MS", Verdana, sans-serif;
        }
    </style>

</head>
<body style="margin: 0; padding: 0;">
<div class="container-fluid">
    <center>
        <div class="model-huruf-family"><br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%" height="100%" class="font">
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="padding: 10px 0 10px 0" align="center" width="130px">
                                    <img src="{{ asset('template/assets/images/logo-pertamina-gas-negara.png') }}" width="200px;">
                                </td>
                                <td align="center">
                                    <b style="font-size:18px;">PT PERUSAHAAN GAS NEGARA TBK</b><br>
                                    <b style="font-size:18px;">DATA PERMOHONAN SURAT IZIN MASUK INSTALASI (SIMI)</b><br>
                                    <b style="font-size:18px;">Tahun {{ $tahun }}</b>
                                </td>
                                <td width="130px">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td style="text-align:center"><b>&nbsp;</b></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 90px; text-align:center"></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff" colspan="2" height="100%"><br>
                        <table border="1" rules="all" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="padding: 5px 5px 5px 5px" width="50px">No</th>
                                <th style="padding: 5px 5px 5px 5px" width="100px">Tanggal</th>
                                <th style="padding: 5px 5px 5px 5px" width="100px">Nomor</th>
                                <th style="padding: 5px 5px 5px 5px" width="200px">Nama Pemohon</th>
                                <th style="padding: 5px 5px 5px 5px" width="200px">Instalasi</th>
                                <th style="padding: 5px 5px 5px 5px" width="300px">Keperluan</th>
                                <th style="padding: 5px 5px 5px 5px" width="80px">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataSIMI as $data)
                                <?php
                                    $wilayah = \App\Models\Wilayah::where('nama_wilayah', $data->nama_wilayah)->first();
                                ?>
                                <tr>
                                    <td style="text-align:center; padding: 5px 5px 5px 5px; vertical-align: top">{{ $loop->iteration }}</td>
                                    <td nowrap style="text-align: center; padding: 5px 5px 5px 5px; vertical-align: top">{{ date('d-m-Y', strtotime($data->tgl_simi)) }}</td>
                                    <td nowrap style="text-align: center; padding: 5px 5px 5px 5px; vertical-align: top">{{ $data->no_simi }}</td>
                                    <td style="padding: 5px 5px 5px 5px; vertical-align: top">
                                        <b>{{ $data->nama_pemohon }}</b>
                                        <br>
                                        <span style="color: gray">{{ $data->divisi_pemohon }}</span>
                                    </td>
                                    <td style="padding: 5px 5px 5px 5px; vertical-align: top">
                                        <b>{{ $data->nama_instalasi }}</b>
                                        <br>
                                        <span style="color: gray">{{ $wilayah->inisial }}</span>
                                        <br>
                                        <span style="color: gray">{{ $data->nama_area }}</span>
                                    </td>
                                    <td style="padding: 5px 5px 5px 5px; vertical-align: top">{{ $data->keperluan }}</td>
                                    <td style="text-align: center; padding: 5px 5px 5px 5px; vertical-align: top">{{ $data->status }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <br>
                    </td>
            </table>
        </div>
    </center>
</div>
</body>
</html>
