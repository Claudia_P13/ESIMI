@extends('layout.master')
@section('title', 'e-SIMI | History Request')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10 font-weight-bold">History Request
                            <div class="btn-group float-right">
                                <button class="btn btn-sm btn-link text-muted mr-0" style="text-decoration: none" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="feather icon-filter mr-0"></i></button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#!">Today</a>
                                    <a class="dropdown-item" href="#!">Monthly</a>
                                    <a class="dropdown-item" href="#!">Annual</a>
                                    <a class="dropdown-item" href="#!">Custom Range</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#!">Reset</a>
                                </div>
                            </div>
                            <a href="{{ route('agreement') }}"
                               class="btn btn-sm btn-rounded text-white theme-bg2 float-right"><i
                                    class="feather icon-plus"></i>Create New</a>
                        </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">History & Report</a></li>
                        <li class="breadcrumb-item"><a href="#!">History Request</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="main-body">
        <div class="page-wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card Recent-Users">
                        <div class="card-header">
                            <h5>Data Surat Izin Masuk Instalasi (SIMI)</h5>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table id="example1" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="10px">No</th>
                                        <th class="text-center" width="50px">Tanggal</th>
                                        <th class="text-center" width="100px">Nomor</th>
                                        <th class="text-center" width="200px">Pemohon</th>
                                        <th class="text-center" width="200px">Instalasi</th>
                                        <th class="text-center" width="80px">Status</th>
                                        <th class="text-center" width="50px">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dataSIMI as $data)
                                        <tr>
                                            <td style="text-align: center; vertical-align: top">{{ $loop->iteration }}</td>
                                            <td style="vertical-align: top; text-align: center">{{ date('d-m-Y', strtotime($data->tgl_simi)) }}</td>
                                            <td style="vertical-align: top; text-align: center" nowrao>
                                                <a href="{{ route('formSIMI', encrypt($data->simi_id)) }}" target="_blank">{{ $data->no_simi }}</a>
                                            </td>
                                            <td style="vertical-align: top">
                                                <span class="font-weight-bold">{{ $data->nama_pemohon }}</span>
                                                <br>
                                                {{ $data->divisi_pemohon }}
                                            </td>
                                            <td style="vertical-align: top">
                                                <span class="font-weight-bold">{{ $data->nama_instalasi }}</span>
                                                <br>
                                                {{ $data->nama_area }}
                                            </td>
                                            <td style="vertical-align: top" class="text-center" nowrap>
                                                @if($data->status == 'Draft')
                                                    <span class="badge badge-secondary">Draft</span>
                                                @elseif($data->status == 'Submited')
                                                    <span class="badge badge-warning">Waiting Approved</span>
                                                @elseif($data->status == 'Approved')
                                                    <span class="badge badge-success">Approved</span>
                                                @elseif($data->status == 'Rejected')
                                                    <span class="badge badge-danger">Rejected</span>
                                                @elseif($data->status == 'Canceled')
                                                    <span class="badge badge-danger">Canceled</span>
                                                @else
                                                    <span class="badge badge-info">{{ $data->status }}</span>
                                                @endif
                                            </td>
                                            <td style="vertical-align: top; text-align: center" nowrap>
                                                <div class="btn-group"
                                                     style="margin-top: -8px; margin-right: 0px; margin-right: -18px">
                                                    <button class="btn btn-link" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-settings"></i>
                                                    </button>
                                                    <ul class="list-unstyled dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item">
                                                            <a href="{{ route('detailSIMI', encrypt($data->simi_id)) }}">
                                                                <span><i class="feather icon-eye"></i>View Detail</span>
                                                            </a>
                                                        </li>
                                                        @if($data->status == 'Draft')
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)" class="edit"
                                                               data-id="{{ encrypt($data->simi_id) }}"
                                                               data-mulai="{{ date('d-m-Y', strtotime($data->tgl_mulai)) }}"
                                                               data-selesai="{{ date('d-m-Y', strtotime($data->tgl_selesai)) }}"
                                                               data-kegiatan="{{ $data->keperluan }}"
                                                               data-target=".modal-edit" data-toggle="modal">
                                                                <span><i class="feather icon-edit"></i>Edit</span>
                                                            </a>
                                                        </li>
                                                        @else
                                                            @if(session('user')->role == 'Admin' or session('user')->role == 'Approver')
                                                                <li class="dropdown-item">
                                                                    <a href="javascript:void(0)" class="edit"
                                                                       data-id="{{ encrypt($data->simi_id) }}"
                                                                       data-mulai="{{ date('d-m-Y', strtotime($data->tgl_mulai)) }}"
                                                                       data-selesai="{{ date('d-m-Y', strtotime($data->tgl_selesai)) }}"
                                                                       data-kegiatan="{{ $data->keperluan }}"
                                                                       data-target=".modal-edit" data-toggle="modal">
                                                                        <span><i class="feather icon-edit"></i>Edit</span>
                                                                    </a>
                                                                </li>
                                                            @endif
                                                        @endif
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)" class="cancel"
                                                               data-id="{{ encrypt($data->simi_id) }}">
                                                                <span><i
                                                                        class="feather icon-x-circle"></i>Cancel</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="post" action="{{ action('SIMIController@update') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-edit" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">EDIT SIMI</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="simiID" id="simiID">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Tanggal Mulai <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><i
                                                    class="feather icon-calendar"></i></span>
                                    </div>
                                    <input type="text" name="tanggalMulai" id="tanggalMulai" onchange="ubahTanggal()" value="{{ old('tanggalMulai') }}"
                                           class="form-control date-start">
                                </div>
                                @if($errors->has('tanggalMulai'))
                                    <small class="text-danger">Tanggal mulai kegiatan harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Tanggal Selesai <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><i
                                                    class="feather icon-calendar"></i></span>
                                    </div>
                                    <input type="text" name="tanggalSelesai" id="tanggalSelesai" value="{{ old('tanggalSelesai') }}"
                                           class="form-control date-end">
                                </div>
                                @if($errors->has('tanggalSelesai'))
                                    <small class="text-danger">Tanggal selesai kegiatan harus diisi</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Nama Kegiatan/Keperluan <span class="text-danger">*</span></label>
                            <textarea class="form-control" rows="3" name="namaKegiatan" id="namaKegiatan">{{ old('namaKegiatan') }}</textarea>
                            @if($errors->has('namaKegiatan'))
                                <small class="text-danger">Nama kegiatan harus diisi</small>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Save Changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('footer')
    <script>
        $(document).on('click', '.edit', function (e) {
            document.getElementById("simiID").value = $(this).attr('data-id');
            document.getElementById("tanggalMulai").value = $(this).attr('data-mulai');
            document.getElementById("tanggalSelesai").value = $(this).attr('data-selesai');
            document.getElementById("namaKegiatan").value = $(this).attr('data-kegiatan');
        });
    </script>

    <script>
        $('.cancel').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan membatalkan permohonan ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "/esimi/transaksi/canceledSIMI/" + id + "";
                    }
                });

        });
    </script>

    <script>
        function ubahTanggal() {
            document.getElementById("tanggalSelesai").value = '';
        }
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Data yang anda isi belum lengkap', 'Warning', {closeButton: true});
        @endif
    </script>
@stop
