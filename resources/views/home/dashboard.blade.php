@extends('layout.master')
@section('title', 'e-SIMI | Dashboard')

@section('content')
    <style>
        .zoom {
            /*padding: 50px;*/
            /*background-color: green;*/
            transition: transform .2s; /* Animation */
            /*width: 200px;*/
            /*height: 200px;*/
            /*margin: 0 auto;*/
        }

        .zoom:hover {
            transform: scale(1.05); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
        }
    </style>

    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10 font-weight-bold">Dashboard {{ $tahun }}<br>
                            <small class="font-weight-bold text-muted">{{ $wilayah }}</small>
                            <button class="btn btn-sm btn-light btn-rounded float-right" data-target=".modal-periode"
                                    data-toggle="modal"><i class="feather icon-filter"></i>Filter Data
                            </button>
                        </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="main-body">
        <div class="page-wrapper">
            <div class="row">
                <div class="col-md-4">
                    <div class="card Active-visitor">
                        <div class="card-block text-center">
                            <h5 class="mb-4 font-weight-bold">Total Visitor</h5>
                            <i class="feather icon-users f-30 text-c-green"></i>
                            <h2 class="f-w-300 font-weight-bold mt-3">{{ number_format($jumlahVisitor,0,',','.') }}</h2>
                            <span class="text-muted">Requested Visit</span>
                            <div class="row mt-5 card-active">
                                <div class="col-md-4 col-6">
                                    <h4>{{ number_format($jumlahProcess,0,',','.') }}</h4>
                                    <span class="text-muted">On Process</span>
                                </div>
                                <div class="col-md-4 col-6">
                                    <h4>{{ number_format($jumlahVisited,0,',','.') }}</h4>
                                    <span class="text-muted">Visited</span>
                                </div>
                                <div class="col-md-4 col-12">
                                    <h4>{{ number_format($jumlahClosed,0,',','.') }}</h4>
                                    <span class="text-muted">Closed</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card p-2">
                        <div id="chartMonthly" style="width: 100%; height: 300px;"></div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        @foreach($dataArea as $area)
                            <?php
                            $jumlahAreaProcess = \App\Models\ViewSIMI::whereYear('tgl_simi', $tahun)
                                ->where('nama_area', $area->nama_area)
                                ->whereIn('status', ['Draft', 'Submited', 'Approved', 'Rejected'])
                                ->count();
                            $jumlahAreaVisited = \App\Models\ViewSIMI::whereYear('tgl_simi', $tahun)
                                ->where('nama_area', $area->nama_area)
                                ->whereIn('status', ['Visited'])
                                ->count();
                            $jumlahAreaClosed = \App\Models\ViewSIMI::whereYear('tgl_simi', $tahun)
                                ->where('nama_area', $area->nama_area)
                                ->whereIn('status', ['Closed'])
                                ->count();
                            ?>
                            <div class="col-md-6">
                                <a href="{{ route('indexAreaDashboard', ['year' => encrypt($tahun), 'area' => encrypt($area->nama_area)]) }}">
                                    <div class="card zoom theme-bg ticket-customer">
                                        <div class="card-block">
                                            <div class="row align-items-center justify-content-center">
                                                <div class="col-auto">
                                                    <h2 class="text-danger font-weight-bold mb-0 f-w-300">{{ number_format($area->total,0,',','.') }}</h2>
                                                </div>
                                                <div class="col">
                                                    <div class="row text-center">
                                                        <div class="col-md-4 col-6">
                                                            <span
                                                                class="text-white d-block">{{ $jumlahAreaProcess }}</span>
                                                            <span class="text-white">Process</span>
                                                        </div>
                                                        <div class="col-md-4 col-6">
                                                            <span
                                                                class="text-white d-block">{{ $jumlahAreaVisited }}</span>
                                                            <span class="text-white">Visited</span>
                                                        </div>
                                                        <div class="col-md-4 col-12">
                                                            <span
                                                                class="text-white d-block">{{ $jumlahAreaClosed }}</span>
                                                            <span class="text-white">Closed</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h5 class="text-dark f-w-300 mt-4">{{ $area->nama_area }}</h5>
                                            <i class="fas fa-file-alt text-white f-70"></i>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>

                    <div class="card p-2">
                        <div id="chartInstalasi" style="width: 100%; height: 300px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="post" action="{{ action('DashboardController@postYear') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-periode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="exampleModalLiveLabel"><i
                                class="feather icon-filter mr-2"></i>Filter Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label>Tahun <span class="text-danger">*</span></label>
                                <select name="tahun" class="form-control">
                                    <option>{{ $tahun }}</option>
                                    <option>2018</option>
                                    <option>2019</option>
                                    <option>2020</option>
                                    <option>2021</option>
                                    <option>2022</option>
                                    <option>2023</option>
                                </select>
                                @if($errors->has('tahun'))
                                    <small class="text-danger">Tahun harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-9">
                                <label>Satuan Kerja/Wilayah <span class="text-danger">*</span></label>
                                <select name="wilayah" class="form-control">
                                    <option>{{ $wilayah }}</option>
                                    @foreach($dataWilayah as $satker)
                                        <option>{{ $satker->nama_wilayah }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('wilayah'))
                                    <small class="text-danger">Satuan kerja/wilayah harus diisi</small>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('footer')
    <script src="{{ asset('template/assets/plugins/chart-highchart/js/highcharts.js') }}"></script>
    <script src="{{ asset('template/assets/plugins/chart-highchart/js/highcharts-3d.js') }}"></script>
    <script src="{{ asset('template/assets/js/pages/chart-highchart-custom.js') }}"></script>

    <script>
        Highcharts.chart('chartInstalasi', {
            colors: ['#1de9b6', '#1dc4e9', '#A389D4', '#899FD4', '#f44236', '#f4c22b'],
            credits: {
                enabled: false,
            },
            title: {
                style: {
                    color: '#000',
                    font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
                },
                text: 'Statistik Request Instalasi',
            },
            subtitle: {
                style: {
                    color: '#666666',
                    font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
                },
                text: 'in {{ $tahun }}'
            },
            xAxis: {
                categories: {!! json_encode($dataInstalasi) !!}
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.f}'
                    }
                }
            },
            series: [{
                name: "Total Request",
                type: 'column',
                colorByPoint: true,
                data: {!! json_encode($dataJumlahInstalasi) !!},
                showInLegend: false,
            }]
        });
    </script>

    <script>
        const chart = Highcharts.chart('chartMonthly', {
            colors: ['#A389D4', '#1dc4e9', '#1de9b6', '#f44236', '#f4c22b', '#899FD4'],
            credits: {
                enabled: false,
            },
            title: {
                style: {
                    color: '#000',
                    font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
                },
                text: 'Monthly Report',
            },
            subtitle: {
                style: {
                    color: '#666666',
                    font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
                },
                text: 'in {{ $tahun }}'
            },
            xAxis: {
                categories: {!! json_encode($dataBulan) !!}
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            series: [{
                name: "Total Request",
                type: 'area',
                colorByPoint: false,
                data: {!! json_encode($dataJumlahBulan) !!},
                showInLegend: false,
            }]
        });
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Parameter pencarian anda belum lengkap', 'Warning', {closeButton: true});
        @endif
    </script>
@stop
