@extends('layout.master')
@section('title', 'e-SIMI | My Tasks')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h4 class="m-b-10 font-weight-bold">My Tasks</h4>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#!"><i
                                    class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">My Tasks</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="main-body">
        <div class="page-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="card Recent-Users">
                        <div class="card-header">
                            <h5>Permohonan Surat Izin Masuk Instalasi (SIMI)</h5>
                        </div>
                        <div class="card-block">
                            @if($jumlahSIMI > 0)
                                <div class="table-responsive">
                                    <table class="table table-hover pb-0">
                                        <tbody>
                                        @foreach($dataSIMI as $data)
                                            <tr class="unread">
                                                <td style="vertical-align: top; padding-top: 13px" width="20px">
                                                    <img class="rounded-circle" style="width:40px;" src="{{ asset('template/assets/images/user/avatar-2.jpg') }}" alt="activity-user"></td>
                                                <td style="vertical-align: top" nowrap>
                                                    <h6 class="mb-0 font-weight-bold">
                                                        <a href="{{ route('formSIMI', encrypt($data->simi_id)) }}" target="_blank">{{ $data->no_simi }}</a>
                                                    </h6>
                                                    <p class="m-0">{{ date('d-m-Y H:i', strtotime($data->submited_date))." WIB" }}</p>
                                                </td>
                                                <td style="vertical-align: top">
                                                    <h6 class="mb-0 font-weight-bold">{{ $data->nama_pemohon }}</h6>
                                                    <p class="m-0">{{ $data->divisi_pemohon }}</p>
                                                </td>
                                                <td style="vertical-align: top">
                                                    <h6 class="mb-0 font-weight-bold">{{ $data->nama_instalasi }}</h6>
                                                    <p class="m-0">{{ $data->keperluan }}</p>
                                                </td>
                                                <td style="vertical-align: top" class="text-right" nowrap>
                                                    <a href="{{ route('detailTasklist', encrypt($data->simi_id)) }}" class="label label-primary f-14">View Detail</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <div class="alert alert-primary mb-0" role="alert">
                                    <i class="fas fa-info-circle mr-2 text-c-blue"></i>Saat ini anda tidak memiliki
                                    permohonan persetujuan
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')

@stop
