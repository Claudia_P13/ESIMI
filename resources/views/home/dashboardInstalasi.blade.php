@extends('layout.master')
@section('title', 'e-SIMI | Dashboard')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10 font-weight-bold">Dashboard {{ $tahun }}<br>
                            <small class="font-weight-bold text-muted">{{ $instalasi }}</small>
                            <button class="btn btn-sm btn-light btn-rounded float-right" data-target=".modal-periode"
                                    data-toggle="modal"><i class="feather icon-calendar"></i>Year
                            </button>
                            @if(session('user')->role == "Admin")
                                <a href="{{ route('dashboard') }}"
                                   class="btn btn-sm btn-secondary btn-rounded float-right"><i
                                        class="feather icon-arrow-left"></i>Back</a>
                            @endif
                        </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#!"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="main-body">
        <div class="page-wrapper">
            <div class="row">
                <div class="col-md-4">
                    <div class="card Active-visitor">
                        <div class="card-block text-center">
                            <h5 class="mb-4 font-weight-bold">Total Visitor</h5>
                            <i class="feather icon-users f-30 text-c-green"></i>
                            <h2 class="f-w-300 font-weight-bold mt-3">{{ number_format($jumlahVisitor,0,',','.') }}</h2>
                            <span class="text-muted">Requested Visit</span>
                            <div class="row mt-5 card-active">
                                <div class="col-md-4 col-6">
                                    <h4>{{ number_format($jumlahProcess,0,',','.') }}</h4>
                                    <span class="text-muted">On Process</span>
                                </div>
                                <div class="col-md-4 col-6">
                                    <h4>{{ number_format($jumlahVisited,0,',','.') }}</h4>
                                    <span class="text-muted">Visited</span>
                                </div>
                                <div class="col-md-4 col-12">
                                    <h4>{{ number_format($jumlahClosed,0,',','.') }}</h4>
                                    <span class="text-muted">Closed</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card p-2">
                        <div id="chartMonthly" style="width: 100%; height: 300px;"></div>
                    </div>
                    <div class="card Recent-Users">
                        <div class="card-header">
                            <h5>Data Visited</h5>
                        </div>
                        <div class="card-block">
                            @if($jumlahSIMI > 0)
                                <div class="table-responsive">
                                    <table id="example1" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="10px">No</th>
                                            <th class="text-center" width="50px">Tanggal</th>
                                            <th class="text-center" width="100px">Nomor</th>
                                            <th class="text-center" width="200px">Keperluan</th>
                                            @if(session('user')->role == "Admin Instalasi")
                                                <th class="text-center" width="50px">Action</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($dataSIMI as $data)
                                            <?php
                                            $wilayah = \App\Models\Wilayah::where('nama_wilayah', $data->nama_wilayah)->first();
                                            ?>
                                            <tr>
                                                <td style="text-align: center; vertical-align: top">{{ $loop->iteration }}</td>
                                                <td style="vertical-align: top; text-align: center"
                                                    nowrap>{{ date('d-m-Y', strtotime($data->tgl_simi)) }}</td>
                                                <td style="vertical-align: top; text-align: center" nowrap>
                                                    <a href="{{ route('formSIMI', encrypt($data->simi_id)) }}"
                                                       target="_blank">{{ $data->no_simi }}</a>
                                                </td>
                                                <td style="vertical-align: top">
                                                    {{ $data->keperluan }}
                                                </td>
                                                @if(session('user')->role == "Admin Instalasi")
                                                    <td style="vertical-align: top; text-align: center">
                                                        <a href="javascript:void(0)" class="label theme-bg f-14 text-dark closeSIMI"
                                                           data-id="{{ encrypt($data->simi_id) }}" data-target=".modal-close"
                                                           data-toggle="modal"><i
                                                                class="feather icon-check-circle mr-2"></i>Close</a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <div class="alert alert-primary mb-0" role="alert">
                                    <i class="fas fa-info-circle mr-2 text-c-blue"></i>Saat ini sedang tidak ada
                                    kunjungan
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="post" action="{{ action('SIMIController@closeSIMI') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-close" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="exampleModalLiveLabel">Close SIMI</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="simiID" name="simiID">
                        <div class="form-group">
                            <label>Tanggal Selesai <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><i
                                                    class="feather icon-calendar"></i></span>
                                </div>
                                <input type="text" name="tanggalSelesai" value="{{ old('tanggalSelesai') }}"
                                       class="form-control date">
                            </div>
                            @if($errors->has('tanggalSelesai'))
                                <small class="text-danger">Tanggal selesai kegiatan harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Jam Keluar <span class="text-danger">*</span></label>
                            <input type="text" name="jamKeluar" id="time" class="form-control">
                            @if($errors->has('jamKeluar'))
                                <small class="text-danger">Jam keluar harus diisi</small>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="post" action="{{ action('DashboardController@postInstalasi') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-periode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="exampleModalLiveLabel"><i
                                class="feather icon-filter mr-2"></i>Filter Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Tahun <span class="text-danger">*</span></label>
                            <select name="tahun" class="form-control">
                                <option>{{ $tahun }}</option>
                                <option>2018</option>
                                <option>2019</option>
                                <option>2020</option>
                                <option>2021</option>
                                <option>2022</option>
                                <option>2023</option>
                            </select>
                            @if($errors->has('tahun'))
                                <small class="text-danger">Tahun harus diisi</small>
                            @endif
                        </div>
                        <input type="hidden" name="instalasi" value="{{ $instalasi }}">
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('footer')
    <script src="{{ asset('template/assets/plugins/chart-highchart/js/highcharts.js') }}"></script>
    <script src="{{ asset('template/assets/plugins/chart-highchart/js/highcharts-3d.js') }}"></script>
    <script src="{{ asset('template/assets/js/pages/chart-highchart-custom.js') }}"></script>

    <script>
        $(document).on('click', '.closeSIMI', function (e) {
            document.getElementById("simiID").value = $(this).attr('data-id');
        });
    </script>

    <script>
        const chart = Highcharts.chart('chartMonthly', {
            colors: ['#A389D4', '#1dc4e9', '#1de9b6', '#f44236', '#f4c22b', '#899FD4'],
            credits: {
                enabled: false,
            },
            title: {
                style: {
                    color: '#000',
                    font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
                },
                text: 'Monthly Report',
            },
            subtitle: {
                style: {
                    color: '#666666',
                    font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
                },
                text: 'in {{ $tahun }}'
            },
            xAxis: {
                categories: {!! json_encode($dataBulan) !!}
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.f}'
                    }
                }
            },
            series: [{
                name: "Total Request",
                type: 'column',
                colorByPoint: true,
                data: {!! json_encode($dataJumlahBulan) !!},
                showInLegend: false,
            }]
        });
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Parameter pencarian anda belum lengkap', 'Warning', {closeButton: true});
        @endif
    </script>
@stop
