<?php
if (session('user')->role == "Approver"){
    $jumlahNotifikasi = \App\Models\ViewSIMI::where('status', 'Submited')->where('email_penanggung_jawab', session('user')->email)->count();
}elseif (session('user')->role == "Admin Instalasi"){
    $jumlahNotifikasi = \App\Models\ViewSIMI::where('status', 'Approved')->where('nama_instalasi', session('instalasi'))->count();
}
?>

    <!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description"
          content="Datta Able Bootstrap admin template made using Bootstrap 4 and it has huge amount of ready made feature, UI components, pages which completely fulfills any dashboard needs."/>
    <meta name="keywords"
          content="admin templates, bootstrap admin templates, bootstrap 4, dashboard, dashboard templets, sass admin templets, html admin templates, responsive, bootstrap admin templates free download,premium bootstrap admin templates, datta able, datta able bootstrap admin template">
    <meta name="author" content="Codedthemes"/>

    <link rel="icon" href="{{ asset('template/assets/images/logoicon.png') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/fonts/fontawesome/css/fontawesome-all.min.css') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/plugins/data-tables/css/datatables.min.css') }}">
    {{--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css"/>--}}

    <link rel="stylesheet" href="{{ asset('template/assets/plugins/animation/css/animate.min.css') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/plugins/select2/css/select2.min.css') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/css/style.css') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/plugins/bootstrap-sweetalert/sweetalert.css') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/plugins/toastr/toastr.min.css') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/plugins/smart-wizard/css/smart_wizard.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('template/assets/plugins/smart-wizard/css/smart_wizard_theme_arrows.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('template/assets/plugins/smart-wizard/css/smart_wizard_theme_circles.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('template/assets/plugins/smart-wizard/css/smart_wizard_theme_dots.min.css') }}">

    <link rel="stylesheet"
          href="{{ asset('template/assets/plugins/material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}">

    <link rel="stylesheet"
          href="{{ asset('template/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/assets/fonts/material/css/materialdesignicons.min.css') }}">
</head>
<body>

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>


<nav class="pcoded-navbar menu-light icon-colored">
    <div class="navbar-wrapper">
        <div class="navbar-brand header-logo">
            <a href="javascript:void(0)" class="b-brand">
                <div class="b-bg">
                    <i class="feather icon-feather"></i>
                </div>
                <span class="b-title">electronic <span class="font-weight-bold">SIMI</span></span>
            </a>
            <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
        </div>
        <div class="navbar-content scroll-div">
            <ul class="nav pcoded-inner-navbar">
                <li class="nav-item pcoded-menu-caption">
                    <label>Menu</label>
                </li>
                @if(session('user')->role == "Admin")
                    <li class="nav-item {{ Request::is('dashboard*') ? 'active' : '' }}">
                        <a href="{{ route('dashboard') }}" class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                    </li>
                @elseif(session('user')->role == "Approver")
                    <li class="nav-item {{ Request::is('dashboard*') ? 'active' : '' }}">
                        <a href="{{ route('indexAreaDashboard', ['year' => encrypt(date("Y")), 'area' => encrypt(session('area'))]) }}"
                           class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                    </li>
                @elseif(session('user')->role == "Admin Instalasi")
                    <li class="nav-item {{ Request::is('dashboard*') ? 'active' : '' }}">
                        <a href="{{ route('indexInstalasiDashboard', ['year' => encrypt(date("Y")), 'stasiun' => encrypt(session('instalasi'))]) }}"
                           class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                    </li>
                @else

                @endif
                @if(session('user')->role == 'Approver' or session('user')->role == 'Admin Instalasi')
                    <li class="nav-item {{ Request::is('myTasks*') ? 'active' : '' }}">
                        <a href="{{ route('tasklist') }}" class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-clipboard"></i></span><span
                                class="pcoded-mtext">My Tasks</span>
                            @if($jumlahNotifikasi > 0)
                                <span class="pcoded-badge label label-danger">{{ $jumlahNotifikasi }}</span>
                            @endif
                        </a>
                    </li>
                @endif
                @if(session('user')->role != 'Admin Instalasi')
                    <li class="nav-item {{ Request::is('transaksi/agreement') ? 'active' : '' }} {{ Request::is('transaksi/inputSIMI*') ? 'active' : '' }}">
                        <a href="{{ route('agreement') }}" class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-file-text"></i></span><span
                                class="pcoded-mtext">Create SIMI</span></a>
                    </li>
                @endif
                <li class="nav-item pcoded-menu-caption">
                    <label>History & Report</label>
                </li>
                <li class="nav-item {{ Request::is('transaksi/indexSIMI') ? 'active pcoded-trigger' : '' }} {{ Request::is('transaksi/indexYearSIMI*') ? 'active pcoded-trigger' : '' }} {{ Request::is('transaksi/indexCustomSIMI*') ? 'active pcoded-trigger' : '' }} {{ Request::is('transaksi/indexMonthlySIMI*') ? 'active pcoded-trigger' : '' }} {{ Request::is('transaksi/indexWilayahSIMI*') ? 'active pcoded-trigger' : '' }} {{ Request::is('transaksi/detailSIMI*') ? 'active pcoded-trigger' : '' }}">
                    <a href="{{ route('indexSIMI') }}" class="nav-link"><span class="pcoded-micon"><i
                                class="feather icon-layers"></i></span><span
                            class="pcoded-mtext">History Request</span></a>
                </li>
                @if(session('user')->role == 'Admin')
                    <li class="nav-item pcoded-menu-caption">
                        <label>Master Data</label>
                    </li>
                    <li class="nav-item {{ Request::is('master/indexUser') ? 'active' : '' }}">
                        <a href="{{ route('indexUser') }}" class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-users"></i></span><span
                                class="pcoded-mtext">Users</span></a>
                    </li>
                    <li class="nav-item {{ Request::is('master/indexWilayah') ? 'active' : '' }}">
                        <a href="{{ route('indexWilayah') }}" class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-briefcase"></i></span><span
                                class="pcoded-mtext">Satuan Kerja/Wilayah</span></a>
                    </li>
                    <li class="nav-item {{ Request::is('master/indexArea') ? 'active' : '' }}">
                        <a href="{{ route('indexArea') }}" class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-flag"></i></span><span
                                class="pcoded-mtext">Area/Zona</span></a>
                    </li>
                    <li class="nav-item {{ Request::is('master/indexInstalasi') ? 'active' : '' }} {{ Request::is('master/editInstalasi*') ? 'active' : '' }}">
                        <a href="{{ route('indexInstalasi') }}" class="nav-link"><span class="pcoded-micon"><i
                                    class="feather icon-package"></i></span><span
                                class="pcoded-mtext">Instalasi</span></a>
                    </li>
                @endif
                <li class="nav-item pcoded-menu-caption">
                    <label>Support</label>
                </li>
                <li class="nav-item"><a href="{{ asset('manual.pdf') }}" target="_blank" class="nav-link"><span class="pcoded-micon"><i
                                class="feather icon-book"></i></span><span class="pcoded-mtext">Manual Book</span></a>
                </li>
                <li data-username="Need Support" class="nav-item"><a
                        href="https://api.whatsapp.com/send?phone=6281388888645&text=Mohon%20bantuannya%20terkait%20aplikasi%20e-SIMI"
                        class="nav-link" target="_blank"><span
                            class="pcoded-micon"><i class="feather icon-help-circle"></i></span><span
                            class="pcoded-mtext">Need
                    support ?</span></a></li>
                <li class="nav-item"><a href="{{ route('logout') }}" class="nav-link"><span class="pcoded-micon"><i
                                class="feather icon-power"></i></span><span class="pcoded-mtext">Logout</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<header class="navbar pcoded-header navbar-expand-lg navbar-light">
    <div class="m-header">
        <a class="mobile-menu" id="mobile-collapse1" href="#!"><span></span></a>
        <a href="javascript:void(0)" class="b-brand">
            <div class="b-bg">
                <i class="feather icon-feather"></i>
            </div>
            <span class="b-title">electronic <span class="font-weight-bold">SIMI</span></span>
        </a>
    </div>
    <a class="mobile-menu" id="mobile-header" href="#!">
        <i class="feather icon-more-horizontal"></i>
    </a>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item navbar-collapse">
                <a href="#!" class="full-screen" onclick="javascript:toggleFullScreen()">
                    Selamat Datang, <span class="font-weight-bold">{{ session('user')->nama }}</span>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li>
                <div class="dropdown drp-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon feather icon-settings"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-notification">
                        <div class="pro-head">
                            <img src="{{ asset('template/assets/images/man.jpg') }}" class="img-radius"
                                 alt="User-Profile-Image">
                            <span>{{ session('user')->nama }}</span>
                        </div>
                        <ul class="pro-body">
                            <li><a href="#!" class="dropdown-item"><i class="feather icon-settings"></i> Settings</a>
                            </li>
                            <li><a href="{{ route('logout') }}" class="dropdown-item"><i
                                        class="feather icon-log-out"></i> Logout</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</header>

<section class="header-user-list">
    <div class="h-list-header">
        <div class="input-group">
            <input type="text" id="search-friends" class="form-control" placeholder="Search . . .">
        </div>
    </div>
    <div class="h-list-body">
        <a href="#!" class="h-close-text"><i class="feather icon-chevrons-right"></i></a>
        <div class="main-friend-cont scroll-div">
            <div class="main-friend-list">
                @yield('log')
            </div>
        </div>
    </div>
</section>

<section class="header-chat">
    <div class="h-list-body">
        <div class="main-chat-cont scroll-div">

        </div>
    </div>
</section>

<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content scroll-div">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<!-- Required Js -->
<script src="{{ asset('template/assets/js/vendor-all.min.js') }}"></script>
<script src="{{ asset('template/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/assets/js/pcoded.min.js') }}"></script>

<!-- data tables Js -->
{{--<script src="{{ asset('template/assets/plugins/data-tables/js/datatables.min.js') }}"></script>--}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.11.3/b-2.1.1/b-html5-2.1.1/b-print-2.1.1/cr-1.5.5/fc-4.0.1/fh-3.2.1/r-2.2.9/sc-2.0.5/datatables.min.js"></script>
<script src="{{ asset('template/assets/js/pages/tbl-datatable-custom.js') }}"></script>

<!-- sweet alert Js -->
<script src="{{ asset('template/assets/plugins/sweetalert/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('template/assets/js/pages/ac-alert.js') }}"></script>

<!-- toastr Js -->
<script src="{{ asset('template/assets/plugins/toastr/toastr.min.js') }}"></script>

<!-- select2 Js -->
<script src="{{ asset('template/assets/plugins/select2/js/select2.full.min.js') }}"></script>
{{--<script src="{{ asset('template/assets/js/pages/form-select-custom.js') }}"></script>--}}

<!-- wizard Js -->
<script src="{{ asset('template/assets/plugins/smart-wizard/js/jquery.smartWizard.min.js') }}"></script>
<script src="{{ asset('template/assets/js/pages/wizard-custom.js') }}"></script>

<!-- material datetimepicker Js -->
<script src="{{ asset('template/assets/js/moment.min.js') }}"></script>
<script
    src="{{ asset('template/assets/plugins/material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

<!-- form-picker-custom Js -->
<script src="{{ asset('template/assets/js/pages/form-picker-custom.js') }}"></script>

<script>
    @if(Session::has('gagal'))
    swal("Failed", "{{Session::get('gagal')}}", "error");
    @endif
    @if(Session::has('peringatan'))
    toastr.error("{{Session::get('peringatan')}}", "Warning", {closeButton: true});
    @endif

    @if(Session::has('sukses'))
    swal("Success", "{{Session::get('sukses')}}", "success");
    @endif

    @if(Session::has('berhasil'))
    toastr.success("{{Session::get('berhasil')}}", "Success", {closeButton: true});
    @endif
</script>

<script>
    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>

@yield('footer')
</body>
</html>
