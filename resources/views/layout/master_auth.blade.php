<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="author" content="CodedThemes"/>

    <link rel="icon" href="{{ asset('template/assets/images/logoicon.png') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/fonts/fontawesome/css/fontawesome-all.min.css') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/plugins/animation/css/animate.min.css') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/css/style.css') }}">

    <link rel="stylesheet" href="{{ asset('template/assets/plugins/toastr/toastr.min.css') }}">
</head>
<body>
<div class="auth-wrapper aut-bg-img">
    <div class="auth-content">
        @yield('content')
    </div>
</div>

<script src="{{ asset('template/assets/js/vendor-all.min.js') }}"></script>
<script src="{{ asset('template/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/assets/js/pcoded.min.js') }}"></script>
<script src="{{ asset('template/assets/plugins/toastr/toastr.min.js') }}"></script>

@yield('js')

</body>
</html>
