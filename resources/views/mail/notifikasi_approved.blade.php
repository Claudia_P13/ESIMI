<!DOCTYPE html>
<html>
<body>
<div class='container'>
    <p>Kepada Bapak/Ibu {{ $namaPenerima }}</p>
    <p>Permohonan Surat Izin Masuk Instalasi (SIMI) anda sebagai berikut :</p>
    <table>
        <tr>
            <td style="vertical-align: top">No SIMI</td>
            <td style="vertical-align: top">&nbsp;:</td>
            <td style="vertical-align: top">&nbsp;{{ $noSIMI }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top">Tanggal</td>
            <td style="vertical-align: top">&nbsp;:</td>
            <td style="vertical-align: top">&nbsp;{{ $tanggalSIMI }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top">Periode SIMI</td>
            <td style="vertical-align: top">&nbsp;:</td>
            <td style="vertical-align: top">&nbsp;{{ $periodeSIMI }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top">Kegiatan/Keperluan</td>
            <td style="vertical-align: top">&nbsp;:</td>
            <td style="vertical-align: top">&nbsp;{{ $kegiatan }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top">Instalasi Tujuan</td>
            <td style="vertical-align: top">&nbsp;:</td>
            <td style="vertical-align: top">&nbsp;{{ $instalasi }}</td>
        </tr>
        <tr>
            <td style="vertical-align: top">Area/Zona</td>
            <td style="vertical-align: top">&nbsp;:</td>
            <td style="vertical-align: top">&nbsp;{{ $namaArea }}</td>
        </tr>
    </table>
    <p>Sudah disetujui penanggung jawab zona area</p>
    <p>Untuk melihat detail permohonan, silahkan login aplikasi <a href='https://esimi.pgn.co.id'>e-SIMI</a>.</p>
    <p>
        Catatan :<br>
        E-mail ini dikirim secara otomatis oleh sistem.<br>
        Mohon untuk tidak me-reply e-mail ini.
    </p>
</div>
</body>
</html>
