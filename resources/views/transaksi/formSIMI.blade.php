<?php
error_reporting(1);

$tglsimi = date('Y-m-d', strtotime($data->submited_date));
$jamsimi = date('H:i:s', strtotime($data->submited_date));
$harisimi = date('D', strtotime($data->submited_date));
$tglmulai = $data->tgl_mulai;
$tglselesai = $data->tgl_selesai;
$tglapproval = date('Y-m-d', strtotime($data->approved_date));

function tanggal_indo($tanggal)
{
    $bulan = array(1 => 'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $split = explode('-', $tanggal);
    return $split[2] . ' ' . $bulan[(int)$split[1]] . ' ' . $split[0];
}

$day = date('D', strtotime($tglsimi));
$dayList = array(
    'Sun' => 'Minggu',
    'Mon' => 'Senin',
    'Tue' => 'Selasa',
    'Wed' => 'Rabu',
    'Thu' => 'Kamis',
    'Fri' => 'Jumat',
    'Sat' => 'Sabtu'
);

?>

    <!DOCTYPE html>
<html lang="en">
<head>
    <title>SIMI {{ $data->no_simi }}</title>
    <link rel="icon" href="{{ asset('template/assets/images/logoicon.png') }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .font {
            font-family: sans-serif
        }
    </style>

</head>
<body style="margin: 0; padding: 0;" bgcolor="grey">
<div class="container-fluid">
    <center>
        <div style="width:1000px; height:100%; background-color:white;"><br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%" height="100%" class="font">
                <tr>
                    <td>
                        <table rules="all" border="1" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="padding: 10px 0 10px 0" align="center" width="150px">
                                    <img src="{{ asset('template/assets/images/logi-pertamina-gas-negara.png') }}"
                                         width="180px;">
                                </td>
                                <td align="center">
                                    <b class="font" style="font-size:20px;">PT. PERUSAHAAN GAS NEGARA TBK.</b><br>
                                    <span class="font"
                                          style="font-size:20px; text-transform: uppercase;">{{ $data->nama_wilayah }}</span><br>
                                    <b class="font" style="font-size:30px;">SURAT IZIN MASUK INSTALASI</b>
                                </td>
                                <td width="150px" style="text-align: center">
                                    @if($pass == 3)
                                        <b style="color: red">RED PASS</b>
                                    @elseif($pass == 2)
                                        <b style="color: yellow">YELLOW PASS</b>
                                    @else
                                        <b style="color: green">GREEN PASS</b>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff" colspan="2" height="100%">
                        <table rules="all" border="1" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(1)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 400px">&nbsp;No : {{ $data->no_simi }}</td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(2)</td>
                                <td>&nbsp;Tanggal Permohonan
                                    : {{ $dayList[$harisimi].", " }}{{ tanggal_indo($tglsimi) }}</td>
                            </tr>
                            <tr>
                                <td colspan="4" style="padding: 3px 0 3px 0 ;width: 30px">&nbsp;<b>DATA PEMOHON /
                                        INITIATOR</b></td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(3)</td>
                                <td colspan="4">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 200px">Nama</td>
                                            <td style="width: 5px">:</td>
                                            <td>{{ $data->nama_pemohon }}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(4)</td>
                                <td colspan="4">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 200px">Jabatan</td>
                                            <td style="width: 5px">:</td>
                                            <td>{{ $data->jabatan_pemohon }}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(5)</td>
                                <td colspan="4">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 200px">Divisi / Departemen</td>
                                            <td style="width: 5px">:</td>
                                            <td>{{ $data->divisi_pemohon }}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(6)</td>
                                <td colspan="4">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 200px">Perusahaan / Instansi</td>
                                            <td style="width: 5px">:</td>
                                            <td>{{ $data->perusahaan }}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(7)</td>
                                <td colspan="4">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 200px; vertical-align: top">Keperluan</td>
                                            <td style="width: 5px; vertical-align: top">:</td>
                                            <td>{{ $data->keperluan }}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(8)</td>
                                <td colspan="4">
                                    <table border="0" width="100%">
                                        <tr>
                                            <td style="width: 200px; vertical-align: top">Pengikut</td>
                                            <td style="width: 5px; vertical-align: top">:</td>
                                            <td>
                                                <table border="0" width="100%">
                                                    @foreach($dataDetailPengikut as $dp)
                                                        <tr>
                                                            <td style="width: 250px; vertical-align: top">{{ $loop->iteration.". ". $dp->nama_pengikut }}</td>
                                                            <td style="width: 100px; vertical-align: top">Perusahaan
                                                            </td>
                                                            <td style="width: 5px; vertical-align: top">:</td>
                                                            <td style="; vertical-align: top">{{ $dp->perusahaan }}</td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" style="padding: 3px 0 3px 0 ;width: 30px">&nbsp;<b>INSTALASI TUJUAN</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(9)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 300px">&nbsp;Instalasi
                                    : {{ $data->nama_instalasi }}</td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(10)</td>
                                <td>&nbsp;Tanggal : {{ tanggal_indo($tglmulai)." s.d ".tanggal_indo($tglselesai) }}</td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding: 3px 0 3px 0 ; width: 30px; text-align: center">&nbsp;<b>KEGIATAN
                                        / PEKERJAAN</b></td>
                                <td colspan="2" style="padding: 3px 0 3px 0 ; width: 30px; text-align: center">&nbsp;<b>LOKASI</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(11)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 300px">
                                    <table width="100%">
                                        <tr>
                                            <td>Kunjungan / Peninjauan</td>
                                            <?php
                                            $kegiatan1 = \App\Models\DetailKegiatan::where('no_simi', $data->no_simi)->where('kegiatan', 'Kunjungan / Peninjauan')->count();
                                            ?>
                                            @if($kegiatan1 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(21)</td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>Kantor / Adm Room</td>
                                            <?php
                                            $lokasi1 = \App\Models\DetailLokasiKegiatan::where('no_simi', $data->no_simi)->where('lokasi', 'Kantor / Adm Room')->count();
                                            ?>
                                            @if($lokasi1 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(12)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 300px">
                                    <table width="100%">
                                        <tr>
                                            <td>Rapat / Presentasi</td>
                                            <?php
                                            $kegiatan2 = \App\Models\DetailKegiatan::where('no_simi', $data->no_simi)->where('kegiatan', 'Rapat / Presentasi')->count();
                                            ?>
                                            @if($kegiatan2 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(22)</td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>Control Room</td>
                                            <?php
                                            $lokasi2 = \App\Models\DetailLokasiKegiatan::where('no_simi', $data->no_simi)->where('lokasi', 'Control Room')->count();
                                            ?>
                                            @if($lokasi2 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(13)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 300px">
                                    <table width="100%">
                                        <tr>
                                            <td>Inspeksi / Observasi</td>
                                            <?php
                                            $kegiatan3 = \App\Models\DetailKegiatan::where('no_simi', $data->no_simi)->where('kegiatan', 'Inspeksi / Observasi')->count();
                                            ?>
                                            @if($kegiatan3 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(23)</td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>Ruang Radio</td>
                                            <?php
                                            $lokasi3 = \App\Models\DetailLokasiKegiatan::where('no_simi', $data->no_simi)->where('lokasi', 'Ruang Radio')->count();
                                            ?>
                                            @if($lokasi3 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(14)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 300px">
                                    <table width="100%">
                                        <tr>
                                            <td>Pengambilan Foto / Gambar</td>
                                            <?php
                                            $kegiatan4 = \App\Models\DetailKegiatan::where('no_simi', $data->no_simi)->where('kegiatan', 'Pengambilan Foto / Gambar')->count();
                                            ?>
                                            @if($kegiatan4 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(24)</td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>Ruang Batere</td>
                                            <?php
                                            $lokasi4 = \App\Models\DetailLokasiKegiatan::where('no_simi', $data->no_simi)->where('lokasi', 'Ruang Batere')->count();
                                            ?>
                                            @if($lokasi4 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(15)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 300px">
                                    <table width="100%">
                                        <tr>
                                            <td>Pemeliharaan / Perbaikan</td>
                                            <?php
                                            $kegiatan5 = \App\Models\DetailKegiatan::where('no_simi', $data->no_simi)->where('kegiatan', 'Pemeliharaan / Perbaikan')->count();
                                            ?>
                                            @if($kegiatan5 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(25)</td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>Gudang</td>
                                            <?php
                                            $lokasi5 = \App\Models\DetailLokasiKegiatan::where('no_simi', $data->no_simi)->where('lokasi', 'Gudang')->count();
                                            ?>
                                            @if($lokasi5 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(16)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 300px">
                                    <table width="100%">
                                        <tr>
                                            <td>Konstruksi / Instalasi</td>
                                            <?php
                                            $kegiatan6 = \App\Models\DetailKegiatan::where('no_simi', $data->no_simi)->where('kegiatan', 'Konstruksi / Instalasi')->count();
                                            ?>
                                            @if($kegiatan6 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(26)</td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>Metering / Instalasi Pipeline</td>
                                            <?php
                                            $lokasi6 = \App\Models\DetailLokasiKegiatan::where('no_simi', $data->no_simi)->where('lokasi', 'Metering / Instalasi Pipeline')->count();
                                            ?>
                                            @if($lokasi6 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(17)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 300px">
                                    <table width="100%">
                                        <tr>
                                            <td>Pengiriman / Pengambilan Barang</td>
                                            <?php
                                            $kegiatan7 = \App\Models\DetailKegiatan::where('no_simi', $data->no_simi)->where('kegiatan', 'Pengiriman / Pengambilan Barang')->count();
                                            ?>
                                            @if($kegiatan7 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(27)</td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>Instalasi Kompresor</td>
                                            <?php
                                            $lokasi7 = \App\Models\DetailLokasiKegiatan::where('no_simi', $data->no_simi)->where('lokasi', 'Instalasi Kompresor')->count();
                                            ?>
                                            @if($lokasi7 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(18)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 300px">
                                    <table width="100%">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td style="text-align: center; width: 30px"><i class="fa fa-square-o"></i>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(28)</td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>Genset</td>
                                            <?php
                                            $lokasi8 = \App\Models\DetailLokasiKegiatan::where('no_simi', $data->no_simi)->where('lokasi', 'Genset')->count();
                                            ?>
                                            @if($lokasi8 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(19)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 300px">
                                    <table width="100%">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td style="text-align: center; width: 30px"><i class="fa fa-square-o"></i>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(29)</td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>ROW KP</td>
                                            <?php
                                            $lokasi9 = \App\Models\DetailLokasiKegiatan::where('no_simi', $data->no_simi)->where('lokasi', 'ROW KP')->count();
                                            ?>
                                            @if($lokasi9 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(20)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 300px">
                                    <table width="100%">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td style="text-align: center; width: 30px"><i class="fa fa-square-o"></i>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(30)</td>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td>IT ROOM</td>
                                            <?php
                                            $lokasi10 = \App\Models\DetailLokasiKegiatan::where('no_simi', $data->no_simi)->where('lokasi', 'IT ROOM')->count();
                                            ?>
                                            @if($lokasi10 > 0)
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-check-square-o"></i></td>
                                            @else
                                                <td style="text-align: center; width: 30px"><i
                                                        class="fa fa-square-o"></i></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(31)</td>
                                <td style="padding: 3px 0 3px 0 ;width: 300px">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td style="text-align: center; vertical-align: top; height: 60px">
                                                <b>Diijinkan Oleh</b><br>
                                                <b>{{ $data->jabatan_penanggung_jawab }}</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            @if(in_array($data->status, ['Approved', 'On Progress', 'Closed', 'Visited']))
                                                @if($data->approved_date != "")
                                                    <td style="text-align: center; height: 100px; width: 1200px">
                                                        @if(in_array($data->status, ['Approved', 'On Progress', 'Closed', 'Visited']))
                                                            <img
                                                                src="{{ asset('template/assets/images/approved-bg.gif') }}"
                                                                width="100px">
                                                        @endif
                                                    </td>
                                                @else
                                                    <td style="text-align: center; height: 100px; width: 1200px">
                                                        &nbsp;
                                                    </td>
                                                @endif
                                            @elseif($data->status == "Rejected")
                                                <td style="text-align: center; height: 100px">
                                                    <b style="color: red">REJECTED</b>
                                                </td>
                                            @else
                                                <td style="text-align: center; height: 100px">&nbsp;</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <b>({{ $data->nama_penanggung_jawab }})</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            @if($data->status == "Canceled")
                                                <td style="text-align: center"><b>Tanggal :
                                                        ...............................</b></td>
                                            @else
                                                @if($data->approved_date != "")
                                                    <td style="text-align: center"><b>Tanggal
                                                            : {{ tanggal_indo($tglapproval) }}</b>
                                                    </td>
                                                @else
                                                    <td style="text-align: center"><b>Tanggal :
                                                            ...............................</b></td>
                                                @endif
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(32)</td>
                                <td>
                                    <table width="100%" border="0">
                                        <tr>
                                            <td style="text-align: center; vertical-align: top; height: 60px">
                                                <b>Pemohon / Initiator</b><br>
                                            </td>
                                        </tr>
                                        <tr>
                                            @if($data->status == "Canceled")
                                                <td style="text-align: center; height: 100px">
                                                    <b style="color: red">CANCELED</b>
                                                </td>
                                            @else
                                                <td style="text-align: center; height: 100px">
                                                    <img
                                                        src="{{ asset('template/assets/images/signed-bg.png') }}"
                                                        width="130px">
                                                </td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td style="text-align: center"><b>({{ $data->nama_pemohon }})</b></td>
                                        </tr>
                                        <tr>
                                            @if($data->status == "Canceled")
                                                <td style="text-align: center"><b>Tanggal
                                                        : {{ tanggal_indo($tglapproval) }}</b></td>
                                            @else
                                                <td style="text-align: center"><b>Tanggal
                                                        : {{ tanggal_indo($tglsimi) }}</b></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 3px 0 3px 0 ;text-align: center; width: 50px">(34)</td>
                                <td colspan="4">
                                    <table rules="all" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="text-align: left; padding: 3px 0 3px 0">&nbsp;<b>Catatan/Petunjuk
                                                    OM {{ $data->nama_area }} :</b></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; vertical-align: top; padding: 3px 0 3px 0">
                                                @if($data->petunjuk != "")
                                                    <ol style="margin-left: -15px; margin-top: 10px">
                                                        <li>Personil wajib mematuhi peraturan keamanan & keselamatan
                                                            kerja serta petunjuk petugas selama berada di instalasi.
                                                        </li>
                                                        <li>{{ $data->petunjuk }}</li>
                                                    </ol>
                                                @else
                                                    &nbsp;Personil wajib mematuhi peraturan keamanan & keselamatan kerja
                                                    serta petunjuk petugas selama berada di instalasi
                                                    <br>
                                                    &nbsp;
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
            </table>
            <br>
        </div>
    </center>
</div>
</body>
</html>
