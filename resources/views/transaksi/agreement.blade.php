@extends('layout.master')
@section('title', 'e-SIMI | Informasi dan Pernyataan')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10 font-weight-bold">Informasi dan Pernyataan</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i
                                    class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">e-SIMI</a></li>
                        <li class="breadcrumb-item"><a href="#!">Informasi dan Pernyataan</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="main-body">
        <div class="page-wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <form method="post" action="{{ action('SIMIController@create') }}">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-block">
                                <div class="alert alert-primary text-dark">
                                    <label class="font-weight-bold alert-heading">Pengertian :</label>
                                    <ol style="text-align: justify; margin-left: -15px">
                                        <li>
                                            <b>Instalasi</b> adalah stasiun gas bumi dan fasilitas lain milik PGN yang
                                            berada di stasiun gas tersebut.
                                        </li>
                                        <li>
                                            <b>Pemohon</b> adalah personil yang mengajukan persetujuan SIMI dan memimpin
                                            rencana kegiatan di Instalasi.
                                        </li>
                                        <li>
                                            <b>Pengikut</b> adalah orang-orang yang diikutsertakan untuk memasuki suatu
                                            Instalasi
                                        </li>
                                        <li>
                                            <b>Surat Izin Masuk Instalasi (SIMI)</b> adalah surat izin untuk dapat
                                            memasuki instalasi untuk keperluan peninjauan, koordinasi, kunjungan kerja,
                                            inspeksi atau bekerja di dalam Instalasi yang cara mendapatkan serta
                                            menggunakannya diatur dengan mekanisme tertentu.
                                        </li>
                                        <li>
                                            Pemohon dan pengikut <b>diwajibkan membawa kartu identitas diri yang masih
                                                berlaku</b> pada saat datang ke Instalasi yang dituju.
                                        </li>
                                    </ol>
                                </div>

                                <div class="alert alert-primary text-dark mt-2">
                                    <label class="font-weight-bold alert-heading">Untuk masuk ke instalasi diperlukan SIMI dengan Lampiran Sbb
                                        :</label>
                                    <ol style="text-align: justify; margin-left: -15px">
                                        <li>
                                            ID CARD, Tanda Identitas (ID CARD Perusahaan, KTP/SIM)
                                        </li>
                                        <li>
                                            Perlengkapan & Peralatan (jika ada pekerjaan)
                                        </li>
                                        <li>
                                            Tujuan Dinas (Surat Tugas, Undangan, Work/Delivery Order, Kontrak dsb)
                                        </li>
                                        <li>
                                            Printout Izin Kerja (<b>JSA</b> dan <b>PTW</b>)/<b>IBAPR</b> yang telah disetujui di aplikasi HOLISTIC <a href="https://holistic.pgn.co.id" target="_blank">holistic.pgn.co.id</a> (jika ada pekerjaan)
                                        </li>
                                    </ol>
                                    <b>Catatan</b> : <i class="text-danger">Seluruh lampiran dijadikan 1 file format pdf maksimal 5MB</i>
                                </div>

                                <div class="alert alert-primary text-dark mt-2">
                                    <label class="font-weight-bold alert-heading">Pernyataan Tanggung Jawab Permohonan
                                        e-SIMI :</label>
                                    <ol style="text-align: justify; margin-left: -15px">
                                        <li>
                                            Saya sebagai Pemohon mengajukan persetujuan e-SIMI dan <b>menyatakan bahwa
                                                seluruh informasi yang disampaikan pada Formulir ini adalah benar</b>.
                                        </li>
                                        <li>
                                            Saya menyatakan setuju untuk <b>bertanggung jawab atas rencana kegiatan/
                                                pekerjaan yang akan dilakukan di dalam Instalasi</b>.
                                        </li>
                                        <li>
                                            Saya dan Pengikut menyatakan berada dalam kondisi <b>sehat dan “fit to
                                                work”</b> pada saat akan memasuki Instalasi.
                                        </li>
                                        <li>
                                            Saya menyatakan <b>tunduk dan patuh dengan seluruh ketentuan
                                                perundang-undangan dan peraturan Perusahaan yang berlaku</b>.
                                        </li>
                                    </ol>
                                    <div class="form-group">
                                        <div class="checkbox checkbox-info d-inline">
                                            <input type="checkbox" name="checkbox-w-1" id="terms_and_conditions"
                                                   value="1" class="bukalampiran" onchange="bukalampiran()"
                                                   onclick="terms_changed(this)">
                                            <label for="terms_and_conditions" class="cr">Saya Setuju</label>
                                        </div>
                                    </div>
                                    <div class="form-group instalasi" style="display: none">
                                        <label>Instalasi Tujuan <span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single" name="instalasi" id="instalasi">
                                            <option value=""></option>
                                            @foreach($dataInstalasi as $di)
                                                <option
                                                    value="{{ encrypt($di->instalasi_id) }}">{{ $di->nama_instalasi }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('instalasi'))
                                            <small class="text-danger">Instalasi tujuan harus diisi</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <div class="btn-group">
                                    <a href="{{ route('dashboard') }}" style="text-decoration: none"
                                       class="btn btn-secondary btn-sm">Cancel</a>
                                    <button type="submit" class="btn btn-success btn-sm text-dark" id="submit_button"
                                            disabled>Next<i class="feather icon-arrow-right mr-0 ml-2"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $(document).ready(function () {
            $("#instalasi").select2({
                placeholder: "Pilih Instalasi"
            });
        });
    </script>

    <script>
        function bukalampiran() {
            if ($('.bukalampiran').is(":checked")) {
                $(".instalasi").show();
            } else {
                $(".instalasi").hide();
            }
        }
    </script>

    <script>
        function terms_changed(termsCheckBox) {
            if (termsCheckBox.checked) {
                document.getElementById("submit_button").disabled = false;
            } else {
                document.getElementById("submit_button").disabled = true;
            }
        }
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Pilih instalasi yang akan dituju', 'Warning', {closeButton: true});
        @endif
    </script>
@stop
