@extends('layout.master')
@section('title', 'e-SIMI | History Request')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10 font-weight-bold">History Request<br>
                            <small class="font-weight-bold text-muted">Tahun {{ $tahun }}</small>
                            <div class="btn-group float-right" data-toggle="tooltip" data-placement="left"
                                 title="Filter Data">
                                <button class="btn btn-sm btn-link text-dark mr-0" style="text-decoration: none"
                                        type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i
                                        class="feather icon-filter mr-0"></i></button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="{{ route('indexTodaySIMI') }}">Today</a>
                                    <a class="dropdown-item" data-target=".modal-monthly" data-toggle="modal" href="#!">Monthly</a>
                                    <a class="dropdown-item" data-target=".modal-custom" data-toggle="modal" href="#!">Custom
                                        Range</a>
                                    @if(session('user')->role != 'Admin Instalasi')
                                        <a class="dropdown-item" data-target=".modal-wilayah" data-toggle="modal"
                                           href="#!">Satuan
                                            Kerja/Wilayah</a>
                                    @endif
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('indexSIMI') }}">Reset</a>
                                </div>
                            </div>
                            <button class="btn btn-sm btn-light btn-rounded float-right" data-target=".modal-periode"
                                    data-toggle="modal"><i class="feather icon-calendar"></i>Year
                            </button>
                            <a href="{{ route('agreement') }}"
                               class="btn btn-sm btn-rounded text-white theme-bg2 float-right"><i
                                    class="feather icon-plus"></i>Create New</a>
                        </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">History & Report</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="main-body">
        <div class="page-wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card Recent-Users">
                        <div class="card-header">
                            <h5>Data Permohonan Surat Izin Masuk Instalasi (SIMI)</h5>
                            <div class="card-header-right">
                                <div class="btn-group card-option">
                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        <i class="feather icon-more-horizontal"></i>
                                    </button>
                                    <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                        @if($ket == "All Data")
                                            <li class="dropdown-item">
                                                <a href="{{ route('printYearSIMI', encrypt($tahun)) }}" target="_blank"><i
                                                        class="feather icon-printer mr-2"></i>Print</a>
                                            </li>
                                            <li class="dropdown-item">
                                                <a href="{{ route('exportYearSIMI', encrypt($tahun)) }}"><i
                                                        class="feather icon-file-text mr-2"></i>Export Excel</a>
                                            </li>
                                        @elseif($ket == "Today")
                                            <li class="dropdown-item">
                                                <a href="{{ route('printTodaySIMI') }}" target="_blank"><i
                                                        class="feather icon-printer mr-2"></i>Print</a>
                                            </li>
                                            <li class="dropdown-item">
                                                <a href="{{ route('exportTodaySIMI') }}"><i
                                                        class="feather icon-file-text mr-2"></i>Export Excel</a>
                                            </li>
                                        @elseif($ket == "Monthly")
                                            <li class="dropdown-item">
                                                <a href="{{ route('printMonthlySIMI', ['bulan1'=> encrypt($bulan1), 'bulan2'=> encrypt($bulan2), 'year'=> encrypt($tahun)]) }}"
                                                   target="_blank"><i class="feather icon-printer mr-2"></i>Print</a>
                                            </li>
                                            <li class="dropdown-item">
                                                <a href="{{ route('exportMonthlySIMI', ['bulan1'=> encrypt($bulan1), 'bulan2'=> encrypt($bulan2), 'year'=> encrypt($tahun)]) }}"><i
                                                        class="feather icon-file-text mr-2"></i>Export Excel</a>
                                            </li>
                                        @elseif($ket == "Custom")
                                            <li class="dropdown-item">
                                                <a href="{{ route('printCustomSIMI', ['tanggal1'=> encrypt($tanggal1), 'tanggal2'=> encrypt($tanggal2), 'year'=> encrypt($tahun)]) }}"
                                                   target="_blank"><i class="feather icon-printer mr-2"></i>Print</a>
                                            </li>
                                            <li class="dropdown-item">
                                                <a href="{{ route('exportCustomSIMI', ['tanggal1'=> encrypt($tanggal1), 'tanggal2'=> encrypt($tanggal2), 'year'=> encrypt($tahun)]) }}"><i
                                                        class="feather icon-file-text mr-2"></i>Export Excel</a>
                                            </li>
                                        @elseif($ket == "Wilayah")
                                            <li class="dropdown-item">
                                                <a href="{{ route('printWilayahSIMI', ['year'=> encrypt($tahun), 'wilayah'=> encrypt($wilayah), 'area'=> $area]) }}"
                                                   target="_blank"><i class="feather icon-printer mr-2"></i>Print</a>
                                            </li>
                                            <li class="dropdown-item">
                                                <a href="{{ route('exportWilayahSIMI', ['year'=> encrypt($tahun), 'wilayah'=> encrypt($wilayah), 'area'=> $area]) }}"><i
                                                        class="feather icon-file-text mr-2"></i>Export Excel</a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table id="zero-configuration" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="10px">No</th>
                                        <th class="text-center" width="50px">Tanggal</th>
                                        <th class="text-center" width="100px">Nomor</th>
                                        <th class="text-center" width="200px">Pemohon</th>
                                        <th class="text-center" width="200px">Instalasi</th>
                                        <th class="text-center" width="200px">Keperluan</th>
                                        <th class="text-center" width="80px">Status</th>
                                        <th class="text-center" width="50px">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dataSIMI as $data)
                                        <?php
                                        $wilayah = \App\Models\Wilayah::where('nama_wilayah', $data->nama_wilayah)->first();
                                        ?>
                                        <tr>
                                            <td style="text-align: center; vertical-align: top">{{ $loop->iteration }}</td>
                                            <td style="vertical-align: top; text-align: center"
                                                nowrap>{{ date('d-m-Y', strtotime($data->tgl_simi)) }}</td>
                                            <td style="vertical-align: top; text-align: center" nowrap>
                                                <a href="{{ route('formSIMI', encrypt($data->simi_id)) }}"
                                                   target="_blank">{{ $data->no_simi }}</a>
                                            </td>
                                            <td style="vertical-align: top">
                                                <span class="font-weight-bold">{{ $data->nama_pemohon }}</span>
                                                <br>
                                                {{ $data->divisi_pemohon }}
                                            </td>
                                            <td style="vertical-align: top">
                                                <span class="font-weight-bold">{{ $data->nama_instalasi }}</span>
                                                <br>
                                                <span style="color: gray">{{ $wilayah->inisial }}</span>
                                                <br>
                                                {{ $data->nama_area }}
                                            </td>
                                            <td style="vertical-align: top">
                                                {{ $data->keperluan }}
                                            </td>
                                            <td style="vertical-align: top" class="text-center" nowrap>
                                                @if($data->status == 'Draft')
                                                    <span class="badge badge-secondary">Draft</span>
                                                @elseif($data->status == 'Submited')
                                                    <span class="badge badge-warning">Waiting Approved</span>
                                                @elseif($data->status == 'Approved')
                                                    <span class="badge badge-success">Approved</span>
                                                @elseif($data->status == 'Closed')
                                                    <span class="badge badge-primary">Closed</span>
                                                @elseif($data->status == 'Rejected')
                                                    <span class="badge badge-danger">Rejected</span>
                                                @elseif($data->status == 'Canceled')
                                                    <span class="badge badge-danger">Canceled</span>
                                                @else
                                                    <span class="badge badge-info">{{ $data->status }}</span>
                                                @endif
                                            </td>
                                            <td style="vertical-align: top; text-align: center" nowrap>
                                                <div class="btn-group"
                                                     style="margin-top: -8px; margin-right: 0px; margin-right: -18px">
                                                    <button class="btn btn-link" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-settings"></i>
                                                    </button>
                                                    <ul class="list-unstyled dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item">
                                                            <a href="{{ route('detailSIMI', encrypt($data->simi_id)) }}">
                                                                <span><i class="feather icon-eye"></i>View Detail</span>
                                                            </a>
                                                        </li>
                                                        @if($data->status == 'Draft')
                                                            <li class="dropdown-item">
                                                                <a href="javascript:void(0)" class="edit"
                                                                   data-id="{{ encrypt($data->simi_id) }}"
                                                                   data-mulai="{{ date('d-m-Y', strtotime($data->tgl_mulai)) }}"
                                                                   data-selesai="{{ date('d-m-Y', strtotime($data->tgl_selesai)) }}"
                                                                   data-kegiatan="{{ $data->keperluan }}"
                                                                   data-target=".modal-edit" data-toggle="modal">
                                                                    <span><i class="feather icon-edit"></i>Edit</span>
                                                                </a>
                                                            </li>
                                                        @else
                                                            @if(session('user')->role == 'Admin' or session('user')->role == 'Approver')
                                                                <li class="dropdown-item">
                                                                    <a href="javascript:void(0)" class="edit"
                                                                       data-id="{{ encrypt($data->simi_id) }}"
                                                                       data-mulai="{{ date('d-m-Y', strtotime($data->tgl_mulai)) }}"
                                                                       data-selesai="{{ date('d-m-Y', strtotime($data->tgl_selesai)) }}"
                                                                       data-kegiatan="{{ $data->keperluan }}"
                                                                       data-target=".modal-edit" data-toggle="modal">
                                                                        <span><i class="feather icon-edit"></i>Edit</span>
                                                                    </a>
                                                                </li>
                                                            @endif
                                                        @endif
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)" class="cancel"
                                                               data-id="{{ encrypt($data->simi_id) }}">
                                                                <span><i
                                                                        class="feather icon-x-circle"></i>Cancel</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="post" action="{{ action('SIMIController@update') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-edit" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">EDIT SIMI</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="simiID" id="simiID">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Tanggal Mulai <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><i
                                                    class="feather icon-calendar"></i></span>
                                    </div>
                                    <input type="text" name="tanggalMulai" id="tanggalMulai" onchange="ubahTanggal()"
                                           value="{{ old('tanggalMulai') }}"
                                           class="form-control date-start">
                                </div>
                                @if($errors->has('tanggalMulai'))
                                    <small class="text-danger">Tanggal mulai kegiatan harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Tanggal Selesai <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><i
                                                    class="feather icon-calendar"></i></span>
                                    </div>
                                    <input type="text" name="tanggalSelesai" id="tanggalSelesai"
                                           value="{{ old('tanggalSelesai') }}"
                                           class="form-control date-end">
                                </div>
                                @if($errors->has('tanggalSelesai'))
                                    <small class="text-danger">Tanggal selesai kegiatan harus diisi</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Nama Kegiatan/Keperluan <span class="text-danger">*</span></label>
                            <textarea class="form-control" rows="3" name="namaKegiatan"
                                      id="namaKegiatan">{{ old('namaKegiatan') }}</textarea>
                            @if($errors->has('namaKegiatan'))
                                <small class="text-danger">Nama kegiatan harus diisi</small>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Save Changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="post" action="{{ action('SIMIController@postYearSIMI') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-periode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="exampleModalLiveLabel"><i
                                class="feather icon-filter mr-2"></i>Select Year</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Tahun <span class="text-danger">*</span></label>
                            <select name="tahun" class="form-control">
                                <option></option>
                                <option>2018</option>
                                <option>2019</option>
                                <option>2020</option>
                                <option>2021</option>
                                <option>2022</option>
                                <option>2023</option>
                            </select>
                            @if($errors->has('tahun'))
                                <small class="text-danger">Tahun harus diisi</small>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-secondary btn-block">Search</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="post" action="{{ action('SIMIController@postMonthlySIMI') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-monthly" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="exampleModalLiveLabel"><i
                                class="feather icon-filter mr-2"></i>Filter Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="tahun" value="{{ $tahun }}">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Awal <span
                                        class="text-danger">*</span></label>
                                <select class="form-control" name="bulan1">
                                    <option value=""></option>
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Juni</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustus</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                                @if($errors->has('bulan1'))
                                    <small class="text-danger mt-0">Periode awal harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Akhir <span
                                        class="text-danger">*</span></label>
                                <select class="form-control" name="bulan2">
                                    <option value=""></option>
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Juni</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustus</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                                @if($errors->has('bulan2'))
                                    <small class="text-danger mt-0">Perioe akhir harus diisi</small>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="post" action="{{ action('SIMIController@postWilayahSIMI') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-wilayah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="exampleModalLiveLabel"><i
                                class="feather icon-filter mr-2"></i>Filter Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="tahun" value="{{ $tahun }}">
                        <div class="form-group">
                            <label>Satuan Kerja/Wilayah <span class="text-danger">*</span></label>
                            <select class="form-control" name="namaWilayah" id="namaWilayah">
                                <option></option>
                                @foreach($dataWilayah as $dw)
                                    <option value="{{ $dw->nama_wilayah }}">{{ $dw->nama_wilayah }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('namaWilayah'))
                                <small class="text-danger">Satuan Kerja/wilayah harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Area/Zona <span class="text-danger">*</span></label>
                            <select class="form-control" name="namaArea" id="namaArea"></select>
                            @if($errors->has('namaArea'))
                                <small class="text-danger">Area/zona harus diisi</small>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="post" action="{{ action('SIMIController@postCustomSIMI') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-custom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="exampleModalLiveLabel"><i
                                class="feather icon-filter mr-2"></i>Filter Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="tahun" value="{{ $tahun }}">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Awal <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><i
                                                    class="feather icon-calendar"></i></span>
                                    </div>
                                    <input type="text" name="tanggal1" onchange="ubahTanggal()"
                                           value="{{ old('tanggalMulai') }}"
                                           class="form-control date-awal">
                                </div>
                                @if($errors->has('tanggal1'))
                                    <small class="text-danger">Tanggal awal harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Akhir <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><i
                                                    class="feather icon-calendar"></i></span>
                                    </div>
                                    <input type="text" name="tanggal2" id="tanggalSelesai" value="{{ old('tanggal2') }}"
                                           class="form-control date-akhir">
                                </div>
                                @if($errors->has('tanggal2'))
                                    <small class="text-danger">Tanggal akhir harus diisi</small>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('footer')
    <script>
        $(document).on('click', '.edit', function (e) {
            document.getElementById("simiID").value = $(this).attr('data-id');
            document.getElementById("tanggalMulai").value = $(this).attr('data-mulai');
            document.getElementById("tanggalSelesai").value = $(this).attr('data-selesai');
            document.getElementById("namaKegiatan").value = $(this).attr('data-kegiatan');
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#namaWilayah').change(function () {
                var wilayah_id = $(this).val();

                $.ajax({
                    type: 'GET',
                    url: "esimi/master/dataCariZona/" + wilayah_id + "",
                    success: function (response) {
                        $('#namaArea').html(response);
                        document.getElementById("pic").value = "";
                    }
                });
            })
        })
    </script>

    <script>
        $('.cancel').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan membatalkan permohonan ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "/esimi/transaksi/canceledSIMI/" + id + "";
                    }
                });

        });
    </script>

    <script>
        function ubahTanggal() {
            document.getElementById("tanggalSelesai").value = '';
        }
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Data yang anda isi belum lengkap', 'Warning', {closeButton: true});
        @endif
    </script>
@stop
