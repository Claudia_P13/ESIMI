@extends('layout.master')
@section('title', 'e-SIMI | Detail SIMI')

@section('content')
    <?php
    $dataKegiatan = DB::select("select * from tbl_kegiatan where nama_kegiatan COLLATE utf8mb4_0900_ai_ci not in (select kegiatan COLLATE utf8mb4_0900_ai_ci from tbl_detail_kegiatan where no_simi = '$data->no_simi')");
    $dataLokasi = DB::select("select * from tbl_lokasi_kegiatan where nama_lokasi not in (select lokasi from tbl_detail_lokasi where no_simi = '$data->no_simi')");
    $dataPengikut = DB::select("select * from tbl_auth where nama not in (select nama_pengikut from tbl_detail_pengikut where no_simi = '$data->no_simi') and status = 'Active' order by nama asc");
    ?>

    <style>
        .scroll-kegiatan {
            max-height: 400px;
            overflow-y: auto;
        }
    </style>

    <style>
        .scroll-pengikut {
            max-height: 600px;
            overflow-y: auto;
        }
    </style>

    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10 font-weight-bold">Detail Surat Izin Masuk Instalasi (SIMI)
                            <div class="btn-group float-right">
                                <button class="btn btn-sm btn-link text-muted mr-0" style="text-decoration: none"
                                        type="button" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false"><i
                                        class="feather icon-more-horizontal mr-0"></i></button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    @if($data->status == 'Draft')
                                        @if($jumlahDetailKegiatan > 0 and $jumlahDetailLokasi > 0)
                                            <a class="dropdown-item submitSIMI" data-id="{{ encrypt($data->simi_id) }}"
                                               href="#!"><i class="feather icon-fast-forward mr-2"></i>Submit to
                                                Approver</a>
                                        @endif
                                    @elseif($data->status == 'Rejected')
                                        @if($jumlahDetailKegiatan > 0 and $jumlahDetailLokasi > 0)
                                            <a class="dropdown-item" data-target=".modal-resubmit"
                                               data-toggle="modal"
                                               href="#!"><i class="feather icon-fast-forward mr-2"></i>Resubmit to
                                                Approver</a>
                                        @endif
                                    @else
                                        <a class="dropdown-item resetStatus" data-id="{{ encrypt($data->simi_id) }}"
                                           href="#!"><i class="feather icon-refresh-ccw mr-2"></i>Reset Status</a>
                                    @endif
                                    <a class="dropdown-item" data-target=".modal-log"
                                       data-toggle="modal" href="#!"><i
                                            class="feather icon-clock mr-2"></i>Log Activity</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item cancel" data-id="{{ encrypt($data->simi_id) }}" href="#!"><i
                                            class="feather icon-x-circle mr-2"></i>Cancel</a>
                                </div>
                            </div>
                            <a href="{{ route('indexSIMI') }}"
                               class="btn btn-sm btn-secondary btn-rounded float-right"><i
                                    class="feather icon-arrow-left"></i>Back</a>
                        </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i
                                    class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">History Request</a></li>
                        <li class="breadcrumb-item"><a href="#!">Detail SIMI</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="main-body">
        <div class="page-wrapper">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold">Detail Informasi</h5>
                            <h6 class="card-subtitle mb-5 text-muted">
                                @if($data->status == 'Draft')
                                    <i class="feather icon-info text-primary mr-1"></i>Permohonan baru
                                    dibuat
                                @elseif($data->status == 'Submited')
                                    <i class="feather icon-info text-primary mr-1"></i>Menunggu persetujuan penanggung
                                    jawab area
                                @elseif($data->status == 'Approved')
                                    <i class="feather icon-check-circle text-success mr-1"></i>Permohonan disetujui
                                    penanggung jawab area
                                @elseif($data->status == 'Rejected')
                                    <span class="text-danger">Permohonan ditolak dengan catatan/alasan "<i
                                            class="font-weight-bold">{{ $data->keterangan }}</i>".</span>
                                    <br>
                                    <a href="#!" data-target=".modal-resubmit"
                                       data-toggle="modal"><i class="feather icon-fast-forward mr-2"></i>Resubmit to
                                        Approver</a>
                                @elseif($data->status == 'Closed')
                                    <i class="fa fa-check-circle text-success mr-1"></i>Issue sudah
                                    selesai
                                @else
                                    <i class="feather icon-alert-triangle text-danger mr-1"></i>Issue belum bisa
                                    ditindaklanjuti
                                @endif
                            </h6>
                            <div class="table-responsive">
                                <table class="mb-3" width="100%">
                                    <tbody>
                                    <tr>
                                        <th colspan="2" class="pt-1 pb-2" style="width: 200px; vertical-align: top">
                                            <span class="text-dark f-16">Permohonan</span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Nomor
                                        </th>
                                        <td class="pt-1 pb-1">{{ $data->no_simi }}</td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Tanggal
                                        </th>
                                        <td class="pt-1 pb-1">{{ date('d-m-Y', strtotime($data->tgl_simi)) }}</td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Tanggal Kegiatan
                                        </th>
                                        <td class="pt-1 pb-1"><span
                                                class="font-weight-bold">{{ date('d-m-Y', strtotime($data->tgl_mulai)) }}</span>
                                            s.d <span
                                                class="font-weight-bold">{{ date('d-m-Y', strtotime($data->tgl_selesai)) }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Kegiatan/Keperluan
                                        </th>
                                        <td class="pt-1 pb-1">{{ $data->keperluan }}</td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Lama Kegiatan
                                        </th>
                                        <td class="pt-1 pb-1">{{ $jumlahHari." Hari" }}</td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Status
                                        </th>
                                        <td class="pt-1 pb-1">
                                            @if($data->status == 'Draft')
                                                <span class="badge badge-secondary">Draft</span>
                                            @elseif($data->status == 'Submited')
                                                <span class="badge badge-warning">Waiting Approved</span>
                                            @elseif($data->status == 'Approved')
                                                <span class="badge badge-success">Approved</span>
                                            @elseif($data->status == 'Rejected')
                                                <span class="badge badge-danger">Rejected</span>
                                            @elseif($data->status == 'Canceled')
                                                <span class="badge badge-danger">Canceled</span>
                                            @else
                                                <span class="badge badge-info">{{ $data->status }}</span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Form SIMI
                                        </th>
                                        <td class="pt-1 pb-1">
                                            <a href="{{ route('formSIMI', encrypt($data->simi_id)) }}"
                                               target="_blank"><i class="feather icon-download mr-2"></i>Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Lampiran Pendukung
                                        </th>
                                        <td class="pt-1 pb-1">
                                            @if($data->lampiran != "")
                                                <a href="{{ asset('attachment/'.$data->lampiran.'') }}"
                                                   target="_blank"><i
                                                        class="feather icon-download mr-2"></i>Download</a> |
                                                <a href="#!" data-toggle="modal" data-target=".modal-lampiran"><i
                                                        class="feather icon-edit mr-2"></i>Edit</a>
                                            @else
                                                <a href="#!" data-toggle="modal" data-target=".modal-lampiran"
                                                   target="_blank"><i class="feather icon-paperclip mr-2"></i>Upload
                                                    File</a>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-2" style="vertical-align: top">
                                            &nbsp;
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" class="pt-1 pb-2" style="width: 200px; vertical-align: top">
                                            <span class="text-dark f-16">Pemohon/Initiator</span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Nama
                                        </th>
                                        <td class="pt-1 pb-1">{{ $data->nama_pemohon }}</td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1">
                                            Jabatan
                                        </th>
                                        <td class="pt-1 pb-1">{{ $data->jabatan_pemohon }}</td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="vertical-align: top">
                                            Divisi
                                        </th>
                                        <td class="pt-1 pb-1"
                                            style="vertical-align: top">{{ $data->divisi_pemohon }}</td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="vertical-align: top">
                                            Perusahaan
                                        </th>
                                        <td class="pt-1 pb-1"
                                            style="vertical-align: top">{{ $data->perusahaan }}</td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="vertical-align: top">
                                            Email
                                        </th>
                                        <td class="pt-1 pb-1"
                                            style="vertical-align: top">{{ $data->email_pemohon }}</td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-2" style="vertical-align: top">
                                            &nbsp;
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" class="pt-1 pb-2" style="width: 200px; vertical-align: top">
                                            <span class="text-dark f-16">Instalasi Tujuan</span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Nama Instalasi
                                        </th>
                                        <td class="pt-1 pb-1">{{ $data->nama_instalasi }}</td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Area/Zona
                                        </th>
                                        <td class="pt-1 pb-1">{{ $data->nama_area }}</td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Penanggung Jawab
                                        </th>
                                        <td class="pt-1 pb-1">{{ $data->nama_penanggung_jawab }}</td>
                                    </tr>
                                    <tr>
                                        <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                            Jabatan
                                        </th>
                                        <td class="pt-1 pb-1">{{ $data->jabatan_penanggung_jawab }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5>Detail Kegiatan/Pekerjaan</h5>
                            @if($data->status == "Draft")
                                <a href="#!" class="fa-pull-right text-muted" data-target=".modal-kegiatan"
                                   data-toggle="modal">
                                    <i class="feather icon-plus mr-1"></i>Add New
                                </a>
                            @else
                                @if(session('user')->role == "Admin" or session('user')->role == "Approver")
                                    <a href="#!" class="fa-pull-right text-muted" data-target=".modal-kegiatan"
                                       data-toggle="modal">
                                        <i class="feather icon-plus mr-1"></i>Add New
                                    </a>
                                @endif
                            @endif
                        </div>
                        @if($jumlahDetailKegiatan > 0)
                            <div class="table-responsive">
                                <table class="table-striped" width="100%">
                                    <tbody>
                                    @foreach($dataDetailKegiatan as $dataK)
                                        <?php
                                        $mandatori = \App\Models\ViewDetailKegiatan::where('kegiatan', $dataK->kegiatan)->first();
                                        ?>
                                        <tr>
                                            <td style="text-align: center; vertical-align: top; padding: 10px 10px 10px 25px"
                                                width="10px">{{ $loop->iteration }}.
                                            </td>
                                            <td>
                                                {{ $dataK->kegiatan }}
                                                @if($mandatori->dokumen == 1)
                                                    <span class="text-danger">*</span>
                                                @endif
                                            </td>
                                            <td style="text-align: right; padding-right: 25px">
                                                <a href="javascript:void(0)" class="deleteKegiatan"
                                                   data-id="{{ encrypt($dataK->detail_kegiatan_id) }}"><i
                                                        class="feather icon-trash-2 text-danger"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            @if($dok > 0)
                                <div class="alert alert-warning alert-dismissible ml-3 mr-3 mt-3 fade show"
                                     role="alert">
                                     <span class="text-dark" style="text-align: justify">Lampirkan Printout Izin Kerja (JSA dan PTW) yang
                                        telah disetujui di aplikasi HOLISTIC <a href="https://holistic.pgn.co.id"
                                                                                target="_blank">holistic.pgn.co.id</a>
                                        sebagai dokumen persyaratan melakukan pekerjaan di area instalasi.</span>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                </div>
                            @endif
                        @else
                            <div class="alert alert-danger ml-3 mr-3 mt-3" role="alert">
                                Detail kegiatan/pekerjaan belum ditambahkan
                            </div>
                        @endif
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h5>Detail Lokasi</h5>
                            @if($data->status == "Draft")
                                <a href="#!" class="fa-pull-right text-muted" data-target=".modal-lokasi"
                                   data-toggle="modal">
                                    <i class="feather icon-plus mr-1"></i>Add New
                                </a>
                            @else
                                @if(session('user')->role == "Admin" or session('user')->role == "Approver")
                                    <a href="#!" class="fa-pull-right text-muted" data-target=".modal-lokasi"
                                       data-toggle="modal">
                                        <i class="feather icon-plus mr-1"></i>Add New
                                    </a>
                                @endif
                            @endif
                        </div>
                        @if($jumlahDetailLokasi > 0)
                            <div class="table-responsive">
                                <table class="table-striped" width="100%">
                                    <tbody>
                                    @foreach($dataDetailLokasi as $dataL)
                                        <?php
                                        $zona = \App\Models\LokasiKegiatan::where('nama_lokasi', $dataL->lokasi)->first();
                                        ?>
                                        <tr>
                                            <td style="text-align: center; vertical-align: top; padding: 10px 10px 10px 25px"
                                                width="10px">{{ $loop->iteration }}.
                                            </td>
                                            <td>{{ $dataL->lokasi }}</td>
                                            <td>
                                                @if($zona->nilai == 3)
                                                    <span class="badge badge-danger">Tertutup/<i>Red Zone</i></span>
                                                @elseif($zona->nilai == 2)
                                                    <span
                                                        class="badge badge-warning">Terlarang/<i>Yellow Zone</i></span>
                                                @else
                                                    <span class="badge badge-success">Terbatas/<i>Green Zone</i></span>
                                                @endif
                                            </td>
                                            <td style="text-align: right; padding-right: 25px">
                                                <a href="javascript:void(0)" class="deleteLokasi"
                                                   data-id="{{ encrypt($dataL->detail_lokasi_id) }}"><i
                                                        class="feather icon-trash-2 text-danger"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="alert alert-danger ml-3 mr-3 mt-3" role="alert">
                                Detail lokasi belum ditambahkan
                            </div>
                        @endif
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h5>Detail Pengikut</h5>
                            @if($data->status == "Draft")
                                <div class="card-header-right">
                                    <div class="btn-group card-option">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false">
                                            <i class="feather icon-plus mr-1"></i>Add New
                                        </button>
                                        <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                            <li class="dropdown-item">
                                                <a href="#!" data-target=".modal-pengikutInternal"
                                                   data-toggle="modal"><i
                                                        class="feather icon-user-check mr-2"></i>Internal</a>
                                            </li>
                                            <li class="dropdown-item">
                                                <a href="#!" data-target=".modal-pengikutExternal"
                                                   data-toggle="modal"><i class="feather icon-user-x mr-2"></i>External</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            @else
                                @if(session('user')->role == "Admin" or session('user')->role == "Approver")
                                    <div class="card-header-right">
                                        <div class="btn-group card-option">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                <i class="feather icon-plus mr-1"></i>Add New
                                            </button>
                                            <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                                <li class="dropdown-item">
                                                    <a href="#!" data-target=".modal-pengikutInternal"
                                                       data-toggle="modal"><i
                                                            class="feather icon-user-check mr-2"></i>Internal</a>
                                                </li>
                                                <li class="dropdown-item">
                                                    <a href="#!" data-target=".modal-pengikutExternal"
                                                       data-toggle="modal"><i class="feather icon-user-x mr-2"></i>External</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        </div>
                        @if($jumlahDetailPengikut > 0)
                            <div class="table-responsive">
                                <table class="table-striped" width="100%">
                                    <tbody>
                                    @foreach($dataDetailPengikut as $dataP)
                                        <tr>
                                            <td style="text-align: center; vertical-align: top; padding: 10px 10px 10px 25px"
                                                width="10px">{{ $loop->iteration }}.
                                            </td>
                                            <td style="vertical-align: top; padding-top: 10px; padding-bottom: 10px">
                                                <span class="font-weight-bold">{{ $dataP->nama_pengikut }}</span>
                                                <br>
                                                {{ $dataP->perusahaan }}
                                            </td>
                                            <td style="text-align: right; padding-right: 25px; vertical-align: top; padding-top: 10px; padding-bottom: 10px">
                                                <a href="javascript:void(0)" class="deletePengikut"
                                                   data-id="{{ encrypt($dataP->pengikut_id) }}"><i
                                                        class="feather icon-trash-2 text-danger"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="alert alert-danger ml-3 mr-3 mt-3" role="alert">
                                Detail lokasi belum ditambahkan
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-kegiatan" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">DAFTAR KEGIATAN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="scroll-kegiatan">
                    <input type="hidden" name="simiID" id="simiID" value="{{ encrypt($data->simi_id) }}">
                    <div class="table-responsive">
                        <table class="table-striped" width="100%">
                            <tbody>
                            @foreach($dataKegiatan as $dk)
                                <tr>
                                    <td style="text-align: center; vertical-align: top; padding-top: 10px">
                                        <div class="form-group mb-1 ml-2">
                                            <div class="checkbox checkbox-primary checkbox-fill d-inline">
                                                <input type="checkbox" class="chk_boxes1" name="c1"
                                                       id="check_{{ $dk->kegiatan_id }}"
                                                       data-id="{{ $dk->kegiatan_id }}"
                                                       style="cursor: pointer">
                                                <label for="check_{{ $dk->kegiatan_id }}"
                                                       class="cr"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="vertical-align: top; padding-top: 10px; padding-left: 10px">{{ $dk->nama_kegiatan }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-success btn-sm text-dark" id="btnSelect"><i
                                class="fa fa-check"></i> Add Selected
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-lokasi" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">DAFTAR LOKASI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="scroll-kegiatan">
                    <input type="hidden" name="simiID" id="simiID2" value="{{ encrypt($data->simi_id) }}">
                    <div class="table-responsive">
                        <table class="table-striped" width="100%">
                            <tbody>
                            @foreach($dataLokasi as $dl)
                                <tr>
                                    <td style="text-align: center; vertical-align: top; padding-top: 10px">
                                        <div class="form-group mb-1 ml-2">
                                            <div class="checkbox checkbox-primary checkbox-fill d-inline">
                                                <input type="checkbox" class="chk_boxes1" name="c1"
                                                       id="check2_{{ $dl->lokasi_id }}"
                                                       data-id="{{ $dl->lokasi_id }}"
                                                       style="cursor: pointer">
                                                <label for="check2_{{ $dl->lokasi_id }}"
                                                       class="cr"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="vertical-align: top; padding-top: 10px; padding-left: 10px">{{ $dl->nama_lokasi }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-success btn-sm text-dark" id="btnSelectLokasi"><i
                                class="fa fa-check"></i> Add Selected
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-pengikutInternal" tabindex="-1" role="dialog" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">DAFTAR EMPLOYEE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="scroll-kegiatan">
                    <input type="hidden" name="simiID" id="simiID3" value="{{ encrypt($data->simi_id) }}">
                    <div class="table-responsive p-3">
                        <table class="example3 table-striped" width="100%">
                            <thead>
                            <tr>
                                <th style="text-align: center; vertical-align: top">No</th>
                                <th style="vertical-align: top; padding-left: 10px">Nama</th>
                                <th style="vertical-align: top; padding-left: 10px">Perusahaan</th>
                                <th style="vertical-align: top; text-align: center; padding-left: 10px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataPengikut as $p)
                                <tr>
                                    <td style="text-align: center; vertical-align: top; padding-top: 10px; padding-bottom: 10px">{{ $loop->iteration }}</td>
                                    <td style="vertical-align: top; padding-top: 10px; padding-left: 10px">{{ $p->nama }}</td>
                                    <td style="vertical-align: top; padding-top: 10px; padding-left: 10px">{{ $p->perusahaan }}</td>
                                    <td style="vertical-align: top; text-align: center; padding-top: 10px; padding-left: 10px">
                                        <a href="#!" class="addPengikut" user-id="{{ $p->user_id }}"
                                           simi-id="{{ encrypt($data->simi_id) }}">Pilih</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>

    <form method="post" enctype="multipart/form-data" action="{{ action('SIMIController@uploadLampiran') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-lampiran" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">UPLOAD FILE<br>
                            <small><a href="#!" data-toggle="modal" data-target=".modal-info" class="text-right"><i
                                        class="fas fa-hand-point-right"></i> View Information</a></small>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="simiID" value="{{ encrypt($data->simi_id) }}">
                        <div class="form-group">
                            <label>Lampiran Pendukung <span class="text-danger">*</span></label>
                            <br>
                            <input type="file" name="lampiran" accept="application/pdf">
                            @if($errors->has('lampiran'))
                                <br>
                                <small class="text-danger">Lampiran pendukung harus diisi</small>
                            @endif
                            <br>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Save Changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="modal fade modal-info" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">INFORMATION</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-primary text-dark mb-0">
                        <label class="font-weight-bold alert-heading">Untuk masuk ke instalasi diperlukan SIMI dengan
                            Lampiran Sbb
                            :</label>
                        <ol style="text-align: justify; margin-left: -15px">
                            <li>
                                ID CARD, Tanda Identitas (ID CARD Perusahaan, KTP/SIM)
                            </li>
                            <li>
                                Daftar Visitor, Perlengkapan & Peralatan
                            </li>
                            <li>
                                Tujuan Dinas (Surat Tugas, Undangan, Work/Delivery Order, Kontrak dsb)
                            </li>
                            <li>
                                Printout Izin Kerja (<b>JSA</b> dan <b>PTW</b>)/<b>IBAPR</b> yang telah disetujui di aplikasi HOLISTIC <a
                                    href="https://holistic.pgn.co.id" target="_blank">holistic.pgn.co.id</a> (jika ada
                                pekerjaan)
                            </li>
                            <li>
                                Isian Formulir Hasil Self Assessment COVID-19 (<a
                                    href="https://pgn.id/FormSelfAssessmentRisiko" target="_blank">https://pgn.id/FormSelfAssessmentRisiko</a>)
                            </li>
                            <li>
                                WAJIB Hasil Tes COVID-19 Antigen H-1 atau PCR H-3
                            </li>
                        </ol>
                        <b>Catatan</b> : <i class="text-danger">Seluruh lampiran dijadikan 1 file format pdf <2MB</i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="post" action="{{ action('SIMIController@resubmit') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-resubmit" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">RESUBMIT</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="simiID" value="{{ encrypt($data->simi_id) }}">
                        <div class="form-group">
                            <label>Catatan <span class="text-danger">*</span></label>
                            <textarea rows="3" class="form-control" name="catatan" maxlength="150"
                                      placeholder="Berikan penjelasan dari hasil koreksi anda maksimal 150 karakter"></textarea>
                            @if($errors->has('catatan'))
                                <small class="text-danger">Catatan harus diisi</small>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="modal fade modal-log" tabindex="-1" role="dialog" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="myLargeModalLabel"><i
                            class="feather icon-clock mr-2"></i>Log Activity</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <tbody>
                        @foreach($dataLog as $log)
                            <?php
                            $updateBy = \App\Models\User::where('email', $log->created_by)->first();
                            ?>
                            <tr class="unread">
                                <td width="10px">
                                    <img class="rounded-circle" style="width:60px;"
                                         src="{{ asset('template/assets/images/user/avatar-2.jpg') }}"
                                         alt="activity-user">
                                </td>
                                <td width="500px" style="padding-left: -20px">
                                    <h6 class="mb-1 font-weight-bold">{{ $updateBy->nama }}</h6>
                                    <p class="m-0">{{ $log->keterangan }}</p>
                                    <small
                                        class="m-0 text-danger">{{ date('d-m-Y H:i', strtotime($log->created_at)) }}</small>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="post" action="{{ action('SIMIController@storePengikut') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-pengikutExternal" tabindex="-1" role="dialog" aria-hidden="true"
             style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">PENGIKUT EXTERNAL</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" name="simiID" id="simiID"
                               value="{{ encrypt($data->simi_id) }}">
                        <div class="form-group">
                            <label>Nama Lengkap <span class="text-danger">*</span></label>
                            <input type="text" class="form-control text-capitalize" name="nama"
                                   placeholder="Nama sesuai kartu identitas">
                            @if($errors->has('nama'))
                                <small class="text-danger">Nama pengikut harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Perusahaan <span class="text-danger">*</span></label>
                            <input type="text" class="form-control text-uppercase" name="perusahaan"
                                   value="{{ old('perusahaan') }}">
                            @if($errors->has('perusahaan'))
                                <small class="text-danger">Perusahaan harus diisi</small>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('footer')
    <script>
        $(document).ready(function () {
            $('#btnSelect').click(function () {
                swal({
                    title: "Warning",
                    text: "Anda akan menambahkan kegiatan/pekerjaan ini?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: false,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            var id = [];
                            $(':checkbox:checked').each(function (i) {
                                id[i] = $(this).attr('data-id');
                                id_simi = document.getElementById("simiID").value;
                            });

                            if (id.length === 0) {
                                swal("Warning", "Anda belum memilih kegiatan/pekerjaan manapun", "error");
                            } else {
                                window.location = "/esimi/transaksi/addKegiatan/" + id_simi + "/" + id + "";
                            }
                        }
                    });
            });
        });
    </script>

    <script>
        $('.deleteKegiatan').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan menghapus kegiatan/pekerjaan ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "/esimi/transaksi/deleteKegiatan/" + id + "";
                    }
                });

        });
    </script>

    <script>
        $(document).ready(function () {
            $('#btnSelectLokasi').click(function () {
                swal({
                    title: "Warning",
                    text: "Anda akan menambahkan lokasi ini?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: false,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            var id = [];
                            $(':checkbox:checked').each(function (i) {
                                id[i] = $(this).attr('data-id');
                                id_simi = document.getElementById("simiID2").value;
                            });

                            if (id.length === 0) {
                                swal("Warning", "Anda belum memilih lokasi manapun", "error");
                            } else {
                                window.location = "/esimi/transaksi/addLokasi/" + id_simi + "/" + id + "";
                            }
                        }
                    });
            });
        });
    </script>

    <script>
        $('.deleteLokasi').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan menghapus lokasi ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "/esimi/transaksi/deleteLokasi/" + id + "";
                    }
                });
        });
    </script>

    <script>
        $('.addPengikut').click(function () {
            var simi_id = $(this).attr('simi-id');
            var user_id = $(this).attr('user-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan menambahkan pengikut ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "/esimi/transaksi/addPengikut/" + simi_id + "/" + user_id + "";
                    }
                });
        });
    </script>

    <script>
        $('.deletePengikut').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan menghapus pengikut ini?",
                icon: "warning",
                buttons: true,
                dangerMode: false,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "/esimi/transaksi/deletePengikut/" + id + "";
                    }
                });
        });
    </script>

    <script>
        $('.submitSIMI').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan meneruskan permohonan ini ke penanggung jawab area?",
                icon: "warning",
                buttons: true,
                dangerMode: false,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "/esimi/transaksi/submitSIMI/" + id + "";
                    }
                });
        });
    </script>

    <script>
        $('.resetStatus').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan mereset status permohonan ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "/esimi/transaksi/resetStatus/" + id + "";
                    }
                });

        });
    </script>

    <script>
        $('.cancel').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan membatalkan permohonan ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "/esimi/transaksi/canceledSIMI/" + id + "";
                    }
                });

        });
    </script>

    <script>
        function ubahTanggal() {
            document.getElementById("tanggalSelesai").value = '';
        }
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Data yang anda isi belum lengkap', 'Warning', {closeButton: true});
        @endif
    </script>
@stop
