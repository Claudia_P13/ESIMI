@extends('layout.master')
@section('title', 'e-SIMI | Create SIMI')

@section('content')
    <form method="post" enctype="multipart/form-data" action="{{ action('SIMIController@store') }}">
        {{ csrf_field() }}
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10 font-weight-bold">Form Surat Izin Masuk Instalasi (SIMI)<br>
                                <small class="text-c-red font-weight-bold">{{ $data->nama_instalasi }}</small>
                                <button type="submit" class="btn btn-success btn-rounded btn-sm text-dark float-right"><i
                                        class="feather icon-save"></i>Save as Draft
                                </button>
                                <a href="{{ route('agreement') }}"
                                   class="btn btn-sm btn-secondary btn-rounded float-right"><i
                                        class="feather icon-arrow-left"></i>Back</a>
                            </h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i
                                        class="feather icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Create SIMI</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-body">
            <div class="page-wrapper">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Detail Informasi</h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="mb-3" width="100%">
                                        <tbody>
                                        <tr>
                                            <th colspan="2" class="pt-1 pb-2" style="width: 200px; vertical-align: top">
                                                <span class="text-dark f-16">Pemohon/Initiator</span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                                Nama
                                            </th>
                                            <td class="pt-1 pb-1">{{ session('user')->nama }}</td>
                                        </tr>
                                        <tr>
                                            <th class="pt-1 pb-1">
                                                Jabatan
                                            </th>
                                            <td class="pt-1 pb-1">{{ session('user')->jabatan }}</td>
                                        </tr>
                                        <tr>
                                            <th class="pt-1 pb-1" style="vertical-align: top">
                                                Divisi
                                            </th>
                                            <td class="pt-1 pb-1"
                                                style="vertical-align: top">{{ session('user')->divisi }}</td>
                                        </tr>
                                        <tr>
                                            <th class="pt-1 pb-1" style="vertical-align: top">
                                                Perusahaan
                                            </th>
                                            <td class="pt-1 pb-1"
                                                style="vertical-align: top">{{ session('user')->perusahaan }}</td>
                                        </tr>
                                        <tr>
                                            <th class="pt-1 pb-1" style="vertical-align: top">
                                                Email
                                            </th>
                                            <td class="pt-1 pb-1"
                                                style="vertical-align: top">{{ session('user')->email }}</td>
                                        </tr>
                                        <tr>
                                            <th class="pt-1 pb-2" style="vertical-align: top">
                                                &nbsp;
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="2" class="pt-1 pb-2" style="width: 200px; vertical-align: top">
                                                <span class="text-dark f-16">Instalasi Tujuan</span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                                Nama Instalasi
                                            </th>
                                            <td class="pt-1 pb-1">{{ $data->nama_instalasi }}</td>
                                        </tr>
                                        <tr>
                                            <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                                Area/Zona
                                            </th>
                                            <td class="pt-1 pb-1">{{ $data->nama_area }}</td>
                                        </tr>
                                        <tr>
                                            <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                                Penanggung Jawab
                                            </th>
                                            <td class="pt-1 pb-1">{{ $data->nama_pic }}</td>
                                        </tr>
                                        <tr>
                                            <th class="pt-1 pb-1" style="width: 200px; vertical-align: top">
                                                Jabatan
                                            </th>
                                            <td class="pt-1 pb-1">{{ $data->jabatan_pic }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h5>Kegiatan</h5>
                            </div>
                            <div class="card-body">
                                <input type="hidden" name="instalasiID" value="{{ encrypt($data->instalasi_id) }}">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Tanggal Mulai <span class="text-danger">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><i
                                                    class="feather icon-calendar"></i></span>
                                            </div>
                                            <input type="text" name="tanggalMulai" onchange="ubahTanggal()" value="{{ old('tanggalMulai') }}"
                                                   class="form-control date-start">
                                        </div>
                                        @if($errors->has('tanggalMulai'))
                                            <small class="text-danger">Tanggal mulai kegiatan harus diisi</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Tanggal Selesai <span class="text-danger">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><i
                                                    class="feather icon-calendar"></i></span>
                                            </div>
                                            <input type="text" name="tanggalSelesai" id="tanggalSelesai" value="{{ old('tanggalSelesai') }}"
                                                   class="form-control date-end">
                                        </div>
                                        @if($errors->has('tanggalSelesai'))
                                            <small class="text-danger">Tanggal selesai kegiatan harus diisi</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Nama Kegiatan/Pekerjaan <span class="text-danger">*</span></label>
                                    <textarea class="form-control" rows="3" name="namaKegiatan">{{ old('namaKegiatan') }}</textarea>
                                    @if($errors->has('namaKegiatan'))
                                        <small class="text-danger">Nama kegiatan harus diisi</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Lampiran Pendukung <span class="text-danger">*</span>&nbsp;<a href="#!" data-toggle="modal" data-target=".modal-info" class="text-right"><i class="fas fa-hand-point-right"></i> View Information</a></label>
                                    <br>
                                    <input type="file" name="lampiran" accept="application/pdf">
                                    @if($errors->has('lampiran'))
                                        <br>
                                        <small class="text-danger">Lampiran pendukung harus diisi</small>
                                    @endif
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="modal fade modal-info" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">INFORMATION</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-primary text-dark mb-0">
                        <label class="font-weight-bold alert-heading">Untuk masuk ke instalasi diperlukan SIMI dengan Lampiran Sbb
                            :</label>
                        <ol style="text-align: justify; margin-left: -15px">
                            <li>
                                ID CARD, Tanda Identitas (ID CARD Perusahaan, KTP/SIM)
                            </li>
                            <li>
                                Perlengkapan & Peralatan (jika ada pekerjaan)
                            </li>
                            <li>
                                Tujuan Dinas (Surat Tugas, Undangan, Work/Delivery Order, Kontrak dsb)
                            </li>
                            <li>
                                Printout Izin Kerja (<b>JSA</b> dan <b>PTW</b>)/<b>IBAPR</b> yang telah disetujui di aplikasi HOLISTIC <a href="https://holistic.pgn.co.id" target="_blank">holistic.pgn.co.id</a> (jika ada pekerjaan)
                            </li>
                        </ol>
                        <b>Catatan</b> : <i class="text-danger">Seluruh lampiran dijadikan 1 file format pdf maksimal 5MB</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $(document).ready(function () {
            $('#namaWilayah').change(function () {
                var wilayah_id = $(this).val();

                $.ajax({
                    type: 'GET',
                    url: "/esimi/master/dataZona/" + wilayah_id + "",
                    success: function (response) {
                        $('#namaArea').html(response);
                        document.getElementById("pic").value = "";
                    }
                });
            })
        })
    </script>

    <script>
        $(document).ready(function () {
            $('#namaArea').change(function () {
                var area_id = $(this).val();

                $.ajax({
                    type: 'GET',
                    url: "/esimi/master/dataPIC/" + area_id + "",
                    success: function (response) {
                        $('#pic').html(response);
                    }
                });
            })
        })
    </script>

    <script>
        function ubahTanggal() {
            document.getElementById("tanggalSelesai").value = '';
        }
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Data yang anda isi belum lengkap', 'Warning', {closeButton: true});
        @endif
    </script>
@stop
