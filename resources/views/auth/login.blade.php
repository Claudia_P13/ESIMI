@extends('layout.master_auth')
@section('title', 'e-SIMI | Login Page')

@section('content')
    <style>
        .captcha-base__reset {
            position: relative;
            top: -58px;
            left: -40px;
            border: none;
            background: #d8d8d8;
        }

        .captcha-base__input {
            display: block;
            position: relative;
            top: -20px;
            width: 190px;
        }
    </style>

    <div class="card">
        <div class="card-body text-center">
            <form method="post" id="formLogin" action="{{ action('LoginController@login') }}">
                {{ csrf_field() }}
                <div class="mb-4">
                    <img src="{{ asset('template/assets/images/logo-pertamina-gas-negara.png') }}" width="200px">
                </div>
                <h4 class="mb-4 font-weight-bold">Login Session</h4>
                <div class="form-group mb-2">
                    <input type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username" autofocus>
                </div>
                <div class="form-group mb-3">
                    <input type="password" class="form-control form-password" name="password" value="{{ old('password') }}" placeholder="Password">
                </div>
                <div class="form-group text-left">
                    <div class="checkbox checkbox-fill d-inline">
                        <input type="checkbox" class="showPassword" name="checkbox-fill-1"
                               id="checkbox-fill-a1">
                        <label for="checkbox-fill-a1" class="cr"> Show Password</label>
                    </div>
                </div>
                <!-- <div class="captcha-base"></div> -->
                <button type="submit" class="btn btn-secondary btn-block shadow-2 mb-4">LOGIN</button>
                <p class="mb-2 text-muted font-weight-bold">e-SIMI Versi 2.0</p>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('template/assets/js/captcha.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('.showPassword').click(function () {
                if ($(this).is(':checked')) {
                    $('.form-password').attr('type', 'text');
                } else {
                    $('.form-password').attr('type', 'password');
                }
            });
        });
    </script>

    <script>
        @if (Session::has('session'))
        toastr.warning("{{Session::get('session')}}", "Session Timeout", {closeButton: true});
        @endif
    </script>

    <script>
        @if (Session::has('credential'))
        toastr.error("{{Session::get('credential')}}", "Credential Account", {closeButton: true});
        @endif
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Username atau Password harus diisi', 'Login Failed', {closeButton: true});
        @endif
    </script>
@stop
