<table>
    <thead>
    <tr>
        <th>Tanggal</th>
        <th>Nomor</th>
        <th>Nama Pemohon</th>
        <th>Satuan Kerja</th>
        <th>Instalasi</th>
        <th>Wilayah</th>
        <th>Area/Zona</th>
        <th>Keperluan</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($dataSIMI as $data)
        <tr>
            <td>{{ date('d-m-Y', strtotime($data->tgl_simi)) }}</td>
            <td>{{ $data->no_simi }}</td>
            <td>{{ $data->nama_pemohon }}</td>
            <td>{{ $data->divisi_pemohon }}</td>
            <td>{{ $data->nama_instalasi }}</td>
            <td>{{ $data->nama_wilayah }}</td>
            <td>{{ $data->nama_area }}</td>
            <td>{{ $data->keperluan }}</td>
            <td>{{ $data->status }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
