@extends('layout.master')
@section('title', 'e-SIMI | Area/Zona')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10 font-weight-bold">Area/Zona
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".modal-input"
                               class="btn btn-sm text-white theme-bg2 float-right"><i
                                    class="feather icon-plus"></i>Create New</a>
                        </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Master Data</a></li>
                        <li class="breadcrumb-item"><a href="#!">Area/Zona</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="main-body">
        <div class="page-wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card Recent-Users">
                        <div class="card-header">
                            <h5>Data Area</h5>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table id="example1" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="10px">No</th>
                                        <th class="text-center" width="300px">Wilayah</th>
                                        <th class="text-center" width="200px">Area/Zona</th>
                                        <th class="text-center" width="50px">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dataArea as $data)
                                        <tr>
                                            <td style="text-align: center; vertical-align: top">{{ $loop->iteration }}</td>
                                            <td style="vertical-align: top;">{{ $data->nama_wilayah }}</td>
                                            <td style="vertical-align: top">{{ $data->nama_area }}</td>
                                            <td style="vertical-align: top; text-align: center" nowrap>
                                                <div class="btn-group"
                                                     style="margin-top: -8px; margin-right: 0px; margin-right: -20px">
                                                    <button class="btn btn-link" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-settings"></i>
                                                    </button>
                                                    <ul class="list-unstyled dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)" class="edit"
                                                               data-id="{{ encrypt($data->area_id) }}"
                                                               data-namaArea="{{ $data->nama_area }}"
                                                               data-namaWilayah="{{ $data->nama_wilayah }}"
                                                               data-target=".modal-edit" data-toggle="modal">
                                                                <span><i class="feather icon-edit"></i>Edit</span></a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)" class="delete"
                                                               data-id="{{ encrypt($data->area_id) }}">
                                                                <span><i
                                                                        class="feather icon-trash-2"></i>Delete</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="post" action="{{ action('AreaController@store') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-input" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">INPUT AREA/ZONA</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Satuan Kerja/Wilayah <span class="text-danger">*</span></label>
                            <select class="form-control" name="namaWilayah">
                                <option>{{ old('namaWilayah') }}</option>
                                @foreach($dataWilayah as $dw)
                                    <option>{{ $dw->nama_wilayah }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('namaWilayah'))
                                <small class="text-danger">Satuan Kerja/wilayah harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Area/Zona <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="namaArea"
                                   value="{{ old('namaArea') }}"/>
                            @if($errors->has('namaArea'))
                                <small class="text-danger">Area/zona harus diisi</small>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="post" action="{{ action('AreaController@update') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-edit" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">EDIT AREA/ZONA</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" name="areaID" id="areaID">
                        <div class="form-group">
                            <label>Satuan Kerja/Wilayah <span class="text-danger">*</span></label>
                            <select class="form-control" name="namaWilayah" id="namaWilayah">
                                <option>{{ old('namaWilayah') }}</option>
                                @foreach($dataWilayah as $dw)
                                    <option>{{ $dw->nama_wilayah }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('namaWilayah'))
                                <small class="text-danger">Satuan Kerja/wilayah harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Area/Zona <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="namaArea" id="namaArea"
                                   value="{{ old('namaArea') }}"/>
                            @if($errors->has('namaArea'))
                                <small class="text-danger">Area/zona harus diisi</small>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Save Changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="modal fade modal-addPIC" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">DATA APPROVER</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="area" id="area">
                    <div class="table-responsive">
                        <table class="example1 table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center" width="10px">Pilih</th>
                                <th class="text-center" width="800px">Nama & Jabatan</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataApprover as $dp)
                                <tr>
                                    <td style="text-align: center; vertical-align: top">
                                        <div class="form-group mb-1 ml-2">
                                            <div class="checkbox checkbox-primary checkbox-fill d-inline">
                                                <input type="checkbox" class="chk_boxes1" name="c1"
                                                       id="check_{{ $dp->user_id }}"
                                                       data-id="{{ $dp->user_id }}"
                                                       style="cursor: pointer">
                                                <label for="check_{{ $dp->user_id }}"
                                                       class="cr"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="vertical-align: top">
                                        <span class="font-weight-bold">{{ $dp->nama }}</span><br>
                                        {{ $dp->jabatan }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-success btn-sm text-dark" id="btnSelect"><i
                                class="fa fa-check"></i> Add Selected
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $(document).on('click', '.edit', function (e) {
            document.getElementById("areaID").value = $(this).attr('data-id');
            document.getElementById("namaArea").value = $(this).attr('data-namaArea');
            document.getElementById("namaWilayah").value = $(this).attr('data-namaWilayah');
            document.getElementById("namaPIC2").value = $(this).attr('data-namaPIC');
            document.getElementById("jabatanPIC2").value = $(this).attr('data-jabatanPIC');
            document.getElementById("emailPIC2").value = $(this).attr('data-emailPIC');
        });
    </script>

    <script>
        $(document).on('click', '.addPIC', function (e) {
            document.getElementById("area").value = $(this).attr('data-id');
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#btnSelect').click(function () {
                swal({
                    title: "Warning",
                    text: "Anda akan menambahkan penanggung jawab ini?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: false,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            var id = [];
                            $(':checkbox:checked').each(function (i) {
                                id[i] = $(this).attr('data-id');
                                id_area = document.getElementById("area").value;
                            });

                            if (id.length === 0) {
                                swal("Warning", "Anda belum memilih nama penanggung jawab manapun", "error");
                            } else {
                                window.location = "/esimi/master/addPIC/" + id_area + "/" + id + "";
                            }
                        }
                    });
            });
        });
    </script>

    <script>
        $('.deletePIC').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan menghapus penanggung jawab ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "deletePIC/" + id + "";
                    }
                });

        });
    </script>

    <script>
        $('.delete').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan menghapus data ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "deleteArea/" + id + "";
                    }
                });

        });
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Data yang anda isi kurang lengkap', 'Warning', {closeButton: true});
        @endif
    </script>
@stop
