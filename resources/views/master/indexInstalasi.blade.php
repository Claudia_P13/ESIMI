@extends('layout.master')
@section('title', 'e-SIMI | Instalasi')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10 font-weight-bold">Instalasi
                            <div class="btn-group float-right" data-toggle="tooltip" data-placement="left" title="Filter Data">
                                <button class="btn btn-sm btn-light mr-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        class="feather icon-filter mr-0"></i></button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#!"><i class="feather icon-briefcase mr-2"></i>Satuan Kerja/Wilayah</a>
                                    <a class="dropdown-item" href="#!"><i class="feather icon-flag mr-2"></i>Zona/Area</a>
                                </div>
                            </div>
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".modal-input"
                            class="btn btn-sm btn-rounded text-white theme-bg2 float-right"><i
                                    class="feather icon-plus"></i>Create New</a>
                        </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Master Data</a></li>
                        <li class="breadcrumb-item"><a href="#!">Instalasi</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="main-body">
        <div class="page-wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card Recent-Users">
                        <div class="card-header">
                            <h5>Data Instalasi</h5>
                            <div class="card-header-right">
                                <div class="btn-group card-option">
                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="feather icon-more-horizontal"></i>
                                    </button>
                                    <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                        <li class="dropdown-item"><a href="#!"><i class="feather icon-printer mr-2"></i>Print</a></li>
                                        <li class="dropdown-item"><a href="#!"><i class="feather icon-file-text mr-2"></i>Export Excel</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table id="example1" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="10px">No</th>
                                        <th class="text-center" width="200px">Instalasi</th>
                                        <th class="text-center" width="200px">Wilayah</th>
                                        <th class="text-center" width="200px">Area/Zona</th>
                                        <th class="text-center" width="200px">Penanggung Jawab</th>
                                        <th class="text-center" width="50px">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dataInstalasi as $data)
                                        <tr>
                                            <td style="text-align: center; vertical-align: top">{{ $loop->iteration }}</td>
                                            <td style="vertical-align: top;">{{ $data->nama_instalasi }}</td>
                                            <td style="vertical-align: top;">{{ $data->nama_wilayah }}</td>
                                            <td style="vertical-align: top">{{ $data->nama_area }}</td>
                                            <td style="vertical-align: top">
                                                <span class="font-weight-bold">{{ $data->nama_pic }}</span>
                                                <br>
                                                {{ $data->jabatan_pic }}
                                            </td>
                                            <td style="vertical-align: top; text-align: center" nowrap>
                                                <div class="btn-group"
                                                     style="margin-top: -8px; margin-right: 0px; margin-right: -18px">
                                                    <button class="btn btn-link" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-settings"></i>
                                                    </button>
                                                    <ul class="list-unstyled dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item">
                                                            <a href="{{ route('editInstalasi', encrypt($data->instalasi_id)) }}">
                                                                <span><i class="feather icon-edit"></i>Edit</span></a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)" class="delete"
                                                               data-id="{{ encrypt($data->instalasi_id) }}">
                                                                <span><i
                                                                        class="feather icon-trash-2"></i>Delete</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="post" action="{{ action('InstalasiController@store') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-input" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">INPUT INSTALASI</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Instalasi <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="instalasi" value="{{ old('instalasi') }}">
                                @if($errors->has('instalasi'))
                                    <small class="text-danger">Instalasi harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Satuan Kerja/Wilayah <span class="text-danger">*</span></label>
                                <select class="form-control" name="namaWilayah" id="namaWilayah">
                                    <option></option>
                                    @foreach($dataWilayah as $dw)
                                        <option value="{{ $dw->nama_wilayah }}">{{ $dw->nama_wilayah }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('namaWilayah'))
                                    <small class="text-danger">Satuan Kerja/wilayah harus diisi</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Area/Zona <span class="text-danger">*</span></label>
                                <select class="form-control" name="namaArea" id="namaArea">
                                    <option></option>
                                </select>
                                @if($errors->has('namaArea'))
                                    <small class="text-danger">Area/zona harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Penanggung Jawab <span class="text-danger">*</span></label>
                                <select class="form-control" name="namaPIC" id="namaPIC">
                                    <option></option>
                                    @foreach($dataApprover as $approver)
                                        <option jabatan="{{ $approver->jabatan }}" email="{{ $approver->email }}">{{ $approver->nama }}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" class="form-control" name="jabatanPIC" id="jabatanPIC">
                                <input type="hidden" class="form-control" name="emailPIC" id="emailPIC">
                                @if($errors->has('pic'))
                                    <small class="text-danger">Penanggung jawab harus diisi</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Alamat <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="alamat" value="{{ old('alamat') }}">
                            @if($errors->has('alamat'))
                                <small class="text-danger">Alamat harus diisi</small>
                            @endif
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>No Telepon</label>
                                <input type="text" class="form-control" onkeypress="return hanyaAngka(event)"
                                       name="noTelp" value="{{ old('noTelp') }}">
                                @if($errors->has('noTelp'))
                                    <small class="text-danger">No telepon harus diisi</small>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('footer')
    <script>
        $('#namaPIC').on('change', function () {
            // ambil nilai
            var jabatan = $('#namaPIC option:selected').attr('jabatan');
            var email = $('#namaPIC option:selected').attr('email');

            // pindahkan nilai ke input
            $('#jabatanPIC').val(jabatan);
            $('#emailPIC').val(email);
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#namaWilayah').change(function () {
                var wilayah_id = $(this).val();
                console.log(wilayah_id);

                $.ajax({
                    type: 'GET',
                    url: "/esimi/master/dataZona/" + wilayah_id + "",
                    success: function (response) {
                        $('#namaArea').html(response);
                        document.getElementById("pic").value = "";
                    }
                });
            })
        })
    </script>

    <script>
        $('.delete').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan menghapus instalasi ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "deleteInstalasi/" + id + "";
                    }
                });

        });
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Data yang anda isi belum lengkap', 'Warning', {closeButton: true});
        @endif
    </script>
@stop
