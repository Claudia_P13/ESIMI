@extends('layout.master')
@section('title', 'e-SIMI | Users')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10 font-weight-bold">Users
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".modal-input"
                               class="btn btn-sm text-white theme-bg2 float-right"><i
                                    class="feather icon-user-plus"></i>Create User</a>
                        </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Master Data</a></li>
                        <li class="breadcrumb-item"><a href="#!">Users</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="main-body">
        <div class="page-wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card Recent-Users">
                        <div class="card-header">
                            <h5>Data User</h5>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table id="example1" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="10px">No</th>
                                        <th class="text-center" width="100px">Username</th>
                                        <th class="text-center" width="300px">Nama</th>
                                        <th class="text-center" width="300px">Divisi</th>
                                        <th class="text-center" width="100px">Role</th>
                                        <th class="text-center" width="100px">Status</th>
                                        <th class="text-center" width="50px">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dataUser as $data)
                                        <tr>
                                            <td style="text-align: center; vertical-align: top">{{ $loop->iteration }}</td>
                                            <td style="vertical-align: top"
                                                nowrap>{{ strtolower($data->username) }}</td>
                                            <td style="vertical-align: top">
                                                <span class="font-weight-bold">{{ $data->nama }}</span><br>
                                                {{ $data->email }}
                                            </td>
                                            <td style="vertical-align: top">
                                                <span class="font-weight-bold">{{ $data->jabatan }}</span><br>
                                                {{ $data->divisi }}
                                            </td>
                                            <td style="vertical-align: top" class="text-center"
                                                nowrap>{{ $data->role }}</td>
                                            <td style="vertical-align: top" class="text-center" nowrap>
                                                @if($data->status == 'Active')
                                                    <span class="badge badge-success">Active</span>
                                                @else
                                                    <span class="badge badge-danger">Non Active</span>
                                                @endif
                                            </td>
                                            <td style="vertical-align: top; text-align: center" nowrap>
                                                <div class="btn-group"
                                                     style="margin-top: -8px; margin-right: 0px; margin-right: -12px">
                                                    <button class="btn btn-link" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-settings"></i>
                                                    </button>
                                                    <ul class="list-unstyled dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)" class="edit"
                                                               data-id="{{ encrypt($data->user_id) }}"
                                                               data-username="{{ $data->username }}"
                                                               data-email="{{ $data->email }}" data-nama="{{ $data->nama }}"
                                                               data-jabatan="{{ $data->jabatan }}"
                                                               data-divisi="{{ $data->divisi }}"
                                                               data-perusahaan="{{ $data->perusahaan }}"
                                                               data-kategori="{{ $data->kategori }}"
                                                               data-role="{{ $data->role }}"
                                                               data-status="{{ $data->status }}"
                                                               data-target=".modal-edit" data-toggle="modal">
                                                                <span><i class="feather icon-edit"></i>Edit</span>
                                                            </a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)" class="delete"
                                                               data-id="{{ encrypt($data->user_id) }}">
                                                                <span><i
                                                                        class="feather icon-trash-2"></i>Delete</span>
                                                            </a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)" class="resetPassword"
                                                               data-id="{{ encrypt($data->user_id) }}">
                                                                <span><i
                                                                        class="fas fa-key"></i>Reset Password</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="post" action="{{ action('UserController@store') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-input" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">INPUT USER</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Username <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="username"
                                       value="{{ old('username') }}"/>
                                @if($errors->has('username'))
                                    <small class="text-danger">Username harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Email <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email"
                                       value="{{ old('email') }}"/>
                                @if($errors->has('email'))
                                    <small class="text-danger">Email harus diisi</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Nama <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="nama"
                                       value="{{ old('nama') }}"/>
                                @if($errors->has('nama'))
                                    <small class="text-danger">Nama harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Jabatan <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="jabatan" value="{{ old('jabatan') }}">
                                @if($errors->has('divisi'))
                                    <small class="text-danger">Satuan kerja harus diisi</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Divisi <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="divisi" value="{{ old('divisi') }}">
                                @if($errors->has('divisi'))
                                    <small class="text-danger">Divisi harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Perusahaan <span class="text-danger">*</span></label>
                                <select name="perusahaan" class="form-control">
                                    <option>{{ old('perusahaan') }}</option>
                                    @foreach($dataPerusahaan as $perusahaan)
                                        <option>{{ $perusahaan->nama_perusahaan }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('perusahaan'))
                                    <small class="text-danger">Perusahaan harus diisi</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Kategori <span class="text-danger">*</span></label>
                                <select name="kategori" class="form-control">
                                    <option>{{ old('kategori') }}</option>
                                    <option>PWTT</option>
                                    <option>PWT</option>
                                    <option>Mitra Kerja</option>
                                    <option>Instalasi</option>
                                </select>
                                @if($errors->has('kategori'))
                                    <small class="text-danger">Kategori harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Role <span class="text-danger">*</span></label>
                                <select name="role" class="form-control">
                                    <option>{{ old('role') }}</option>
                                    <option>Admin</option>
                                    <option>User</option>
                                    <option>Approver</option>
                                    <option>Admin Instalasi</option>
                                </select>
                                @if($errors->has('role'))
                                    <small class="text-danger">Role harus diisi</small>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="post" action="{{ action('UserController@update') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-edit" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">EDIT USER</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" name="userID" id="userID">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Username <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="username" id="username"
                                       value="{{ old('username') }}"/>
                                @if($errors->has('username'))
                                    <small class="text-danger">Username harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Email <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" id="email"
                                       value="{{ old('email') }}"/>
                                @if($errors->has('email'))
                                    <small class="text-danger">Email harus diisi</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Nama <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="nama" id="nama"
                                       value="{{ old('nama') }}"/>
                                @if($errors->has('nama'))
                                    <small class="text-danger">Nama harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Jabatan <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="jabatan" id="jabatan" value="{{ old('jabatan') }}">
                                @if($errors->has('divisi'))
                                    <small class="text-danger">Satuan kerja harus diisi</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Divisi <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="divisi" id="divisi" value="{{ old('divisi') }}">
                                @if($errors->has('divisi'))
                                    <small class="text-danger">Divisi harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Perusahaan <span class="text-danger">*</span></label>
                                <select name="perusahaan" id="perusahaan" class="form-control">
                                    <option>{{ old('perusahaan') }}</option>
                                    @foreach($dataPerusahaan as $perusahaan)
                                        <option>{{ $perusahaan->nama_perusahaan }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('perusahaan'))
                                    <small class="text-danger">Perusahaan harus diisi</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Kategori <span class="text-danger">*</span></label>
                                <select name="kategori" id="kategori" class="form-control">
                                    <option>{{ old('kategori') }}</option>
                                    <option>PWTT</option>
                                    <option>PWT</option>
                                    <option>Mitra Kerja</option>
                                    <option>Instalasi</option>
                                </select>
                                @if($errors->has('kategori'))
                                    <small class="text-danger">Kategori harus diisi</small>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label>Role <span class="text-danger">*</span></label>
                                <select name="role" id="role" class="form-control">
                                    <option>{{ old('role') }}</option>
                                    <option>Admin</option>
                                    <option>User</option>
                                    <option>Approver</option>
                                    <option>Admin Instalasi</option>
                                </select>
                                @if($errors->has('role'))
                                    <small class="text-danger">Role harus diisi</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Status <span class="text-danger">*</span></label>
                                <select name="status" id="status" class="form-control">
                                    <option>{{ old('status') }}</option>
                                    <option>Active</option>
                                    <option>Non Active</option>
                                </select>
                                @if($errors->has('status'))
                                    <small class="text-danger">Status harus diisi</small>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Save Changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('footer')
    <script>
        $(document).on('click', '.edit', function (e) {
            document.getElementById("userID").value = $(this).attr('data-id');
            document.getElementById("username").value = $(this).attr('data-username');
            document.getElementById("email").value = $(this).attr('data-email');
            document.getElementById("nama").value = $(this).attr('data-nama');
            document.getElementById("jabatan").value = $(this).attr('data-jabatan');
            document.getElementById("divisi").value = $(this).attr('data-divisi');
            document.getElementById("perusahaan").value = $(this).attr('data-perusahaan');
            document.getElementById("kategori").value = $(this).attr('data-kategori');
            document.getElementById("role").value = $(this).attr('data-role');
            document.getElementById("status").value = $(this).attr('data-status');
        });
    </script>

    <script>
        $('.delete').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan menghapus user ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "deleteUser/" + id + "";
                    }
                });

        });
    </script>

    <script>
        $('.resetPassword').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan reset password user ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "resetPassword/" + id + "";
                    }
                });

        });
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Data yang anda isi kurang lengkap', 'Warning', {closeButton: true});
        @endif
    </script>
@stop
