@extends('layout.master')
@section('title', 'e-SIMI | Satuan Kerja/Wilayah')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10 font-weight-bold">Satuan Kerja/Wilayah
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".modal-input"
                               class="btn btn-sm text-white theme-bg2 float-right"><i
                                    class="feather icon-plus"></i>Create New</a>
                        </h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="feather icon-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Master Data</a></li>
                        <li class="breadcrumb-item"><a href="#!">Satuan Kerja/Wilayah</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="main-body">
        <div class="page-wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card Recent-Users">
                        <div class="card-header">
                            <h5>Data Satuan Kerja/Wilayah</h5>
                        </div>
                        <div class="card-block">
                            <div class="table-responsive">
                                <table id="example1" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="10px">No</th>
                                        <th class="text-center" width="100px">Inisial</th>
                                        <th width="700px">Satuan Kerja/Wilayah</th>
                                        <th class="text-center" width="50px">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dataWilayah as $data)
                                        <tr>
                                            <td style="text-align: center; vertical-align: top">{{ $loop->iteration }}</td>
                                            <td style="vertical-align: top; text-align: center">{{ $data->inisial }}</td>
                                            <td style="vertical-align: top">{{ $data->nama_wilayah }}</td>
                                            <td style="vertical-align: top; text-align: center" nowrap>
                                                <div class="btn-group"
                                                     style="margin-top: -8px; margin-right: 0px; margin-right: -12px">
                                                    <button class="btn btn-link" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-settings"></i>
                                                    </button>
                                                    <ul class="list-unstyled dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)" class="edit"
                                                               data-id="{{ encrypt($data->wilayah_id) }}"
                                                               data-inisial="{{ $data->inisial }}"
                                                               data-nama="{{ $data->nama_wilayah }}"
                                                               data-target=".modal-edit" data-toggle="modal">
                                                                <span><i class="feather icon-edit"></i>Edit</span></a>
                                                        </li>
                                                        <li class="dropdown-item">
                                                            <a href="javascript:void(0)" class="delete"
                                                               data-id="{{ encrypt($data->wilayah_id) }}">
                                                                <span><i
                                                                        class="feather icon-trash-2"></i>Delete</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="post" action="{{ action('WilayahController@store') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-input" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">INPUT WILAYAH</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Inisial <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="inisial"
                                   value="{{ old('inisial') }}"/>
                            @if($errors->has('inisial'))
                                <small class="text-danger">Inisial harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Satuan Kerja/Wilayah <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="nama"
                                   value="{{ old('nama') }}"/>
                            @if($errors->has('nama'))
                                <small class="text-danger">Satuan Kerja/wilayah harus diisi</small>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form method="post" action="{{ action('WilayahController@update') }}">
        {{ csrf_field() }}
        <div class="modal fade modal-edit" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-weight-bold" id="myLargeModalLabel">EDIT WILAYAH</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" class="form-control" name="wilayahID" id="wilayahID">
                        <div class="form-group">
                            <label>Inisial <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="inisial" id="inisial"
                                   value="{{ old('inisial') }}"/>
                            @if($errors->has('inisial'))
                                <small class="text-danger">Inisial harus diisi</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Satuan Kerja/Wilayah <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="nama" id="nama"
                                   value="{{ old('nama') }}"/>
                            @if($errors->has('nama'))
                                <small class="text-danger">Satuan Kerja/wilayah harus diisi</small>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button type="button" class="btn btn-secondary btn-sm text-left" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-success btn-sm text-dark text-left"><i
                                    class="fa fa-check mr-2"></i>Save Changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('footer')
    <script>
        $(document).on('click', '.edit', function (e) {
            document.getElementById("wilayahID").value = $(this).attr('data-id');
            document.getElementById("inisial").value = $(this).attr('data-inisial');
            document.getElementById("nama").value = $(this).attr('data-nama');
        });
    </script>

    <script>
        $('.delete').click(function () {
            var id = $(this).attr('data-id');
            swal({
                title: "Warning",
                text: "Anda yakin akan menghapus data ini?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "deleteWilayah/" + id + "";
                    }
                });

        });
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Data yang anda isi kurang lengkap', 'Warning', {closeButton: true});
        @endif
    </script>
@stop
