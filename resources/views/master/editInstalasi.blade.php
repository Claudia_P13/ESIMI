@extends('layout.master')
@section('title', 'e-SIMI | Instalasi')

@section('content')
    <form method="post" action="{{ action('InstalasiController@update') }}">
        {{ csrf_field() }}
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10 font-weight-bold">Instalasi
                                <button type="submit" class="btn btn-success btn-sm text-dark float-right"><i
                                        class="feather icon-check"></i>Save Changes
                                </button>
                                <a href="{{ route('indexInstalasi') }}"
                                   class="btn btn-sm btn-light float-right"><i
                                        class="feather icon-arrow-left"></i>Back</a>
                            </h5>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i
                                        class="feather icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data</a></li>
                            <li class="breadcrumb-item"><a href="#!">Instalasi</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-body">
            <div class="page-wrapper">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Edit Instalasi</h5>
                            </div>
                            <div class="card-block">
                                <input type="hidden" class="form-control" name="instalasiID"
                                       value="{{ encrypt($data->instalasi_id) }}">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Instalasi <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="instalasi" value="{{ $data->nama_instalasi }}">
                                        @if($errors->has('instalasi'))
                                            <small class="text-danger">Instalasi harus diisi</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Satuan Kerja/Wilayah <span class="text-danger">*</span></label>
                                        <select class="form-control" name="namaWilayah" id="namaWilayah">
                                            <option value="{{ $data->nama_wilayah }}">{{ $data->nama_wilayah }}</option>
                                            @foreach($dataWilayah as $dw)
                                                <option value="{{ $dw->nama_wilayah }}">{{ $dw->nama_wilayah }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('namaWilayah'))
                                            <small class="text-danger">Satuan Kerja/wilayah harus diisi</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Area/Zona <span class="text-danger">*</span></label>
                                        <select class="form-control" name="namaArea" id="namaArea">
                                            <option value="{{ encrypt($data->area_id) }}">{{ $data->nama_area }}</option>
                                        </select>
                                        @if($errors->has('namaArea'))
                                            <small class="text-danger">Area/zona harus diisi</small>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Penanggung Jawab <span class="text-danger">*</span></label>
                                        <select class="form-control" name="namaPIC" id="namaPIC">
                                            <option jabatan="{{ $data->jabatan_pic }}" email="{{ $data->email_pic }}">{{ $data->nama_pic }}</option>
                                            @foreach($dataApprover as $approver)
                                                <option jabatan="{{ $approver->jabatan }}" email="{{ $approver->email }}">{{ $approver->nama }}</option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" class="form-control" name="jabatanPIC" id="jabatanPIC" value="{{ $data->jabatan_pic }}">
                                        <input type="hidden" class="form-control" name="emailPIC" id="emailPIC" value="{{ $data->email_pic }}">
                                        @if($errors->has('pic'))
                                            <small class="text-danger">Penanggung jawab harus diisi</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Alamat <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="alamat" value="{{ $data->alamat }}">
                                    @if($errors->has('alamat'))
                                        <small class="text-danger">Alamat harus diisi</small>
                                    @endif
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>No Telepon</label>
                                        <input type="text" class="form-control" onkeypress="return hanyaAngka(event)"
                                               name="noTelp" value="{{ $data->no_telp }}">
                                        @if($errors->has('noTelp'))
                                            <small class="text-danger">No telepon harus diisi</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('footer')
    <script>
        $('#namaPIC').on('change', function () {
            // ambil nilai
            var jabatan = $('#namaPIC option:selected').attr('jabatan');
            var email = $('#namaPIC option:selected').attr('email');

            // pindahkan nilai ke input
            $('#jabatanPIC').val(jabatan);
            $('#emailPIC').val(email);
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#namaWilayah').change(function () {
                var wilayah_id = $(this).val();

                $.ajax({
                    type: 'GET',
                    url: "/esimi/master/dataZona/" + wilayah_id + "",
                    success: function (response) {
                        $('#namaArea').html(response);
                        document.getElementById("pic").value = "";
                    }
                });
            })
        })
    </script>

    <script>
        @if (count($errors) > 0)
        toastr.error('Data yang anda isi belum lengkap', 'Warning', {closeButton: true});
        @endif
    </script>
@stop
