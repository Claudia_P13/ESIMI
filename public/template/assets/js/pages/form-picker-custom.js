'use strict';
$(document).ready(function() {
    $('#date').bootstrapMaterialDatePicker({
        weekStart: 0,
        format: 'DD-MM-YYYY',
        time: false
    });

    $('#date2').bootstrapMaterialDatePicker({
        weekStart: 0,
        format: 'DD-MM-YYYY',
        time: false
    });

    $('.date').bootstrapMaterialDatePicker({
        weekStart: 0,
        format: 'DD-MM-YYYY',
        time: false,
    });

    $('.date2').bootstrapMaterialDatePicker({
        weekStart: 0,
        minDate: new Date(),
        format: 'DD-MM-YYYY',
        time: false,
    });

    $('.date-end').bootstrapMaterialDatePicker({
        weekStart: 0,
        format: 'DD-MM-YYYY',
        //maxDate: new Date('2022-11-25'),
        time: false,
    });
    $('.date-start').bootstrapMaterialDatePicker({
        weekStart: 0,
        minDate: new Date(),
        format: 'DD-MM-YYYY',
        time: false,
    }).on('change', function(e, date) {
        $('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });

    $('.date-akhir').bootstrapMaterialDatePicker({
        weekStart: 0,
        format: 'DD-MM-YYYY',
        time: false,
    });
    $('.date-awal').bootstrapMaterialDatePicker({
        weekStart: 0,
        //minDate: new Date(),
        format: 'DD-MM-YYYY',
        time: false,
    }).on('change', function(e, date) {
        $('.date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });

    $('.date-format').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY HH:mm'
    });

    $('#time').bootstrapMaterialDatePicker({
        date: false,
        format: 'HH:mm'
    });

    $('#date-format').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY HH:mm',
        maxDate: new Date(),
    });

    $('#date-fr').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY HH:mm',
        lang: 'fr',
        weekStart: 1,
        cancelText: 'ANNULER'
    });

    $('#min-date').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY HH:mm',
        minDate: new Date()
    });

    $('#date-end').bootstrapMaterialDatePicker({
        weekStart: 0,
        format: 'dddd DD MMMM YYYY - HH:mm'
    });
    $('#date-start').bootstrapMaterialDatePicker({
        weekStart: 0,
        format: 'dddd DD MMMM YYYY - HH:mm'
    }).on('change', function(e, date) {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });

    $('.demo').each(function() {
        $(this).minicolors({
            control: $(this).attr('data-control') || 'hue',
            defaultValue: $(this).attr('data-defaultValue') || '',
            format: $(this).attr('data-format') || 'hex',
            keywords: $(this).attr('data-keywords') || '',
            inline: $(this).attr('data-inline') === 'true',
            letterCase: $(this).attr('data-letterCase') || 'lowercase',
            opacity: $(this).attr('data-opacity'),
            position: $(this).attr('data-position') || 'bottom',
            swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
            change: function(value, opacity) {
                if (!value) return;
                if (opacity) value += ', ' + opacity;
                if (typeof console === 'object') {
                }
            },
            theme: 'bootstrap'
        });
    });
});
