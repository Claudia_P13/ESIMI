<?php

use Illuminate\Support\Facades\Route;

//====================LOGIN====================//
Route::get('/', 'LoginController@auth')->name('auth');

Route::group(['prefix' => 'auth'], function () {
    Route::get('/login', 'LoginController@index')->name('login');
    Route::post('/login', 'LoginController@login');
    Route::get('/logout', 'LoginController@logout')->name('logout');
});

Route::group(['middleware' => 'timeOut'], function () {
    Route::group(['middleware' => 'cred.login'], function () {
        Route::group(['prefix' => 'dashboard', 'middleware' => 'isUser'], function () {
            Route::get('/', 'DashboardController@index')->name('dashboard');
            Route::post('postYearDashboard', 'DashboardController@postYear');
            Route::get('indexYearDashboard/{year}/{wil}', 'DashboardController@indexYear')->name('indexYearDashboard');
            Route::post('postAreaDashboard', 'DashboardController@postArea');
            Route::get('indexAreaDashboard/{year}/{area}', 'DashboardController@indexArea')->name('indexAreaDashboard');
            Route::post('postInstalasiDashboard', 'DashboardController@postInstalasi');
            Route::get('indexInstalasiDashboard/{year}/{stasiun}', 'DashboardController@indexInstalasi')->name('indexInstalasiDashboard');
        });

        Route::group(['prefix' => 'myTasks', 'middleware' => 'isApprover'], function () {
            Route::get('/', 'TasklistController@index')->name('tasklist');
            Route::get('detailTasklist/{simiID}', 'TasklistController@view')->name('detailTasklist');
            Route::post('approve', 'TasklistController@approve');
            Route::post('reject', 'TasklistController@reject');
            Route::get('approveAll/{simiID}', 'TasklistController@approveAll');
            Route::get('verifikasi/{simiID}', 'TasklistController@verifikasi');
        });

        Route::group(['prefix' => 'master', 'middleware' => 'isAdmin'], function () {
            Route::get('indexUser', 'UserController@index')->name('indexUser');
            Route::post('storeUser', 'UserController@store');
            Route::post('updateUser', "UserController@update");
            Route::get('deleteUser/{userID}', "UserController@delete");
            Route::get('resetPassword/{userID}', "UserController@resetPassword");

            Route::get('indexWilayah', 'WilayahController@index')->name('indexWilayah');
            Route::post('storeWilayah', 'WilayahController@store');
            Route::post('updateWilayah', "WilayahController@update");
            Route::get('deleteWilayah/{wilayahID}', "WilayahController@delete");

            Route::get('indexArea', 'AreaController@index')->name('indexArea');
            Route::post('storeArea', 'AreaController@store');
            Route::post('updateArea', "AreaController@update");
            Route::get('deleteArea/{areaID}', "AreaController@delete");
            Route::get('addPIC/{areaID}/{userID}', "AreaController@addPIC");
            Route::get('deletePIC/{picID}', "AreaController@deletePIC");

            Route::get('indexInstalasi', 'InstalasiController@index')->name('indexInstalasi');
            Route::post('storeInstalasi', 'InstalasiController@store');
            Route::get('editInstalasi/{instalasiID}', "InstalasiController@edit")->name('editInstalasi');
            Route::post('updateInstalasi', "InstalasiController@update");
            Route::get('deleteInstalasi/{instalasiID}', "InstalasiController@delete");

            Route::get('dataZona/{wilayahID}', "APIController@dataZona");
            Route::get('dataCariZona/{wilayahID}', "APIController@dataCariZona");
            Route::get('dataPIC/{areaID}', "APIController@dataPIC");
        });

        Route::group(['prefix' => 'transaksi', 'middleware' => 'isUser'], function () {
            Route::get('indexSIMI', 'SIMIController@index')->name('indexSIMI');
            Route::post('postYearSIMI', 'SIMIController@postYearSIMI');
            Route::get('indexTodaySIMI', 'SIMIController@indexToday')->name('indexTodaySIMI');
            Route::get('indexYearSIMI/{year}', 'SIMIController@indexTahun')->name('indexYearSIMI');
            Route::post('postMonthlySIMI', 'SIMIController@postMonthlySIMI');
            Route::get('indexMonthlySIMI/{bulan1}/{bulan2}/{year}', 'SIMIController@indexMonthly')->name('indexMonthlySIMI');
            Route::post('postCustomSIMI', 'SIMIController@postCustomSIMI');
            Route::get('indexCustomSIMI/{tanggal1}/{tanggal2}', 'SIMIController@indexCustom')->name('indexCustomSIMI');
            Route::post('postWilayahSIMI', 'SIMIController@postWilayahSIMI');
            Route::get('indexWilayahSIMI/{year}/{wilayah}/{area}', 'SIMIController@indexWilayah')->name('indexWilayahSIMI');
            Route::get('agreement', 'SIMIController@agreement')->name('agreement');
            Route::post('createSIMI', 'SIMIController@create');
            Route::get('inputSIMI/{instansiID}', 'SIMIController@inputSIMI')->name('inputSIMI');
            Route::post('storeSIMI', 'SIMIController@store');
            Route::post('updateSIMI', 'SIMIController@update');
            Route::post('uploadLampiran', 'SIMIController@uploadLampiran');
            Route::get('canceledSIMI/{simiID}', 'SIMIController@canceled');
            Route::get('detailSIMI/{simiID}', 'SIMIController@view')->name('detailSIMI');
            Route::get('addKegiatan/{simiID}/{kegiatanID}', "SIMIController@addKegiatan");
            Route::get('deleteKegiatan/{detailID}', "SIMIController@deleteKegiatan");
            Route::get('addLokasi/{simiID}/{lokasiID}', "SIMIController@addLokasi");
            Route::get('deleteLokasi/{detailID}', "SIMIController@deleteLokasi");
            Route::post('storePengikut', "SIMIController@storePengikut");
            Route::get('addPengikut/{simiID}/{pengikutID}', "SIMIController@addPengikut");
            Route::get('deletePengikut/{detailID}', "SIMIController@deletePengikut");
            Route::get('submitSIMI/{simiID}', "SIMIController@submit");
            Route::post('resubmitSIMI', "SIMIController@resubmit");
            Route::get('resetStatus/{simiID}', "SIMIController@resetStatus");
            Route::post('closeSIMI', "SIMIController@closeSIMI");
            Route::get('formSIMI/{simiID}', "SIMIController@formSIMI")->name('formSIMI');
        });

        Route::group(['prefix' => 'report', 'middleware' => 'isUser'], function () {
            Route::get('exportTodaySIMI', 'ReportController@exportIndexToday')->name('exportTodaySIMI');
            Route::get('exportYearSIMI/{year}', 'ReportController@exportIndexTahun')->name('exportYearSIMI');
            Route::get('exportMonthlySIMI/{bulan1}/{bulan2}/{year}', 'ReportController@exportIndexMonthly')->name('exportMonthlySIMI');
            Route::get('exportCustomSIMI/{tanggal1}/{tanggal2}', 'ReportController@exportIndexCustom')->name('exportCustomSIMI');
            Route::get('exportWilayahSIMI/{year}/{wilayah}/{area}', 'ReportController@exportIndexWilayah')->name('exportWilayahSIMI');

            Route::get('printTodaySIMI', 'ReportController@printIndexToday')->name('printTodaySIMI');
            Route::get('printYearSIMI/{year}', 'ReportController@printIndexTahun')->name('printYearSIMI');
            Route::get('printMonthlySIMI/{bulan1}/{bulan2}/{year}', 'ReportController@printIndexMonthly')->name('printMonthlySIMI');
            Route::get('printCustomSIMI/{tanggal1}/{tanggal2}', 'ReportController@printIndexCustom')->name('printCustomSIMI');
            Route::get('printWilayahSIMI/{year}/{wilayah}/{area}', 'ReportController@printIndexWilayah')->name('printWilayahSIMI');
        });

    });

});
