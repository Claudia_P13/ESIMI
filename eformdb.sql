-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: eformdb
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_area`
--

DROP TABLE IF EXISTS `tbl_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_area` (
  `area_id` int NOT NULL AUTO_INCREMENT,
  `nama_wilayah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `nama_area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`area_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_area`
--

LOCK TABLES `tbl_area` WRITE;
/*!40000 ALTER TABLE `tbl_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_auth`
--

DROP TABLE IF EXISTS `tbl_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_auth` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jabatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `divisi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `perusahaan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `kategori` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `role` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `remember_token` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15765 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_auth`
--

LOCK TABLES `tbl_auth` WRITE;
/*!40000 ALTER TABLE `tbl_auth` DISABLE KEYS */;
INSERT INTO `tbl_auth` VALUES (45,'est','donavon.kautzer@example.org','Jose Cassin','accusamus','quae','Nusantara Regas','cupiditate','$2y$10$vKrmZPzwKWQCPU.8.2RrWOcFpJa2DMmwd1YU8WhyH7q8eNDwJHZEa','Admin','Active','phiQl0lZjG','2023-09-27 10:36:46','2023-09-27 10:36:46'),(138,'eos','htorphy@example.com','Arnoldo Willms','nihil','culpa','Nusantara Regas','deleniti','$2y$10$nvuagQAc13RZy4.rarNYsOIEPEHgGsKiBcW5Y0cVMtmK/1OABSgvy','Admin','aktif','uDJONPmC3w','2023-09-27 10:34:38','2023-09-27 10:34:38'),(445,'voluptatem','reza.mansur@example.net','Humaira Nasyidah','minima','facere','Nusantara Regas','IT','password','qui','aktif','Jbq7XPqBiT','2023-09-26 15:38:26','2023-09-26 15:38:26'),(925,'deserunt','anggraini.oskar@example.net','Harjaya Sihombing','rem','non','Nusantara Regas','ex','password','IT','aktif','agC1Ffl8vc','2023-09-26 15:43:01','2023-09-26 15:43:01');
/*!40000 ALTER TABLE `tbl_auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_detail_apd`
--

DROP TABLE IF EXISTS `tbl_detail_apd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_detail_apd` (
  `id_detail_apd` int NOT NULL AUTO_INCREMENT,
  `no_simi` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `apd` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id_detail_apd`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_detail_apd`
--

LOCK TABLES `tbl_detail_apd` WRITE;
/*!40000 ALTER TABLE `tbl_detail_apd` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_detail_apd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_detail_kegiatan`
--

DROP TABLE IF EXISTS `tbl_detail_kegiatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_detail_kegiatan` (
  `detail_kegiatan_id` int NOT NULL AUTO_INCREMENT,
  `no_simi` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `kegiatan` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`detail_kegiatan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=86743 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_detail_kegiatan`
--

LOCK TABLES `tbl_detail_kegiatan` WRITE;
/*!40000 ALTER TABLE `tbl_detail_kegiatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_detail_kegiatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_detail_lokasi`
--

DROP TABLE IF EXISTS `tbl_detail_lokasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_detail_lokasi` (
  `detail_lokasi_id` int NOT NULL AUTO_INCREMENT,
  `no_simi` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lokasi` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`detail_lokasi_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=127991 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_detail_lokasi`
--

LOCK TABLES `tbl_detail_lokasi` WRITE;
/*!40000 ALTER TABLE `tbl_detail_lokasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_detail_lokasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_detail_pengikut`
--

DROP TABLE IF EXISTS `tbl_detail_pengikut`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_detail_pengikut` (
  `pengikut_id` int NOT NULL AUTO_INCREMENT,
  `no_simi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `nama_pengikut` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `perusahaan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pengikut_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=117742 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_detail_pengikut`
--

LOCK TABLES `tbl_detail_pengikut` WRITE;
/*!40000 ALTER TABLE `tbl_detail_pengikut` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_detail_pengikut` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_instalasi`
--

DROP TABLE IF EXISTS `tbl_instalasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_instalasi` (
  `instalasi_id` int NOT NULL AUTO_INCREMENT,
  `nama_instalasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET latin2 COLLATE latin2_general_ci DEFAULT NULL,
  `no_telp` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_wilayah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `area_id` int DEFAULT NULL,
  `pic_id` int DEFAULT NULL,
  `nama_pic` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email_pic` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jabatan_pic` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`instalasi_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_instalasi`
--

LOCK TABLES `tbl_instalasi` WRITE;
/*!40000 ALTER TABLE `tbl_instalasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_instalasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_kegiatan`
--

DROP TABLE IF EXISTS `tbl_kegiatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_kegiatan` (
  `kegiatan_id` int NOT NULL AUTO_INCREMENT,
  `nama_kegiatan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `dokumen` int DEFAULT NULL,
  `craeted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kegiatan_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_kegiatan`
--

LOCK TABLES `tbl_kegiatan` WRITE;
/*!40000 ALTER TABLE `tbl_kegiatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_kegiatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_log`
--

DROP TABLE IF EXISTS `tbl_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_log` (
  `log_id` int NOT NULL AUTO_INCREMENT,
  `no_simi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14788 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_log`
--

LOCK TABLES `tbl_log` WRITE;
/*!40000 ALTER TABLE `tbl_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_lokasi_kegiatan`
--

DROP TABLE IF EXISTS `tbl_lokasi_kegiatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_lokasi_kegiatan` (
  `lokasi_id` int NOT NULL AUTO_INCREMENT,
  `nama_lokasi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `nilai` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`lokasi_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_lokasi_kegiatan`
--

LOCK TABLES `tbl_lokasi_kegiatan` WRITE;
/*!40000 ALTER TABLE `tbl_lokasi_kegiatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_lokasi_kegiatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_perusahaan`
--

DROP TABLE IF EXISTS `tbl_perusahaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_perusahaan` (
  `perusahaan_id` int NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`perusahaan_id`) USING BTREE,
  UNIQUE KEY `nama_perusahaan` (`nama_perusahaan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_perusahaan`
--

LOCK TABLES `tbl_perusahaan` WRITE;
/*!40000 ALTER TABLE `tbl_perusahaan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_perusahaan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pic`
--

DROP TABLE IF EXISTS `tbl_pic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_pic` (
  `pic_id` int NOT NULL AUTO_INCREMENT,
  `area_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `nama_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `jabatan_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `email_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pic_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pic`
--

LOCK TABLES `tbl_pic` WRITE;
/*!40000 ALTER TABLE `tbl_pic` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_pic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_simi`
--

DROP TABLE IF EXISTS `tbl_simi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_simi` (
  `simi_id` int NOT NULL AUTO_INCREMENT,
  `no_simi` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_simi` datetime DEFAULT NULL,
  `nama_pemohon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jabatan_pemohon` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email_pemohon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `divisi_pemohon` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `perusahaan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `instalasi_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_penanggung_jawab` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email_penanggung_jawab` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jabatan_penanggung_jawab` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `keperluan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `status` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `petunjuk` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `keterangan` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `lampiran` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `closed_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `canceled_date` datetime DEFAULT NULL,
  `canceled_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `submited_date` datetime DEFAULT NULL,
  `rejected_date` datetime DEFAULT NULL,
  `verified_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `verified_date` datetime DEFAULT NULL,
  `closed_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`simi_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=43689 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_simi`
--

LOCK TABLES `tbl_simi` WRITE;
/*!40000 ALTER TABLE `tbl_simi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_simi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_simi_copy1`
--

DROP TABLE IF EXISTS `tbl_simi_copy1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_simi_copy1` (
  `simi_id` int NOT NULL AUTO_INCREMENT,
  `no_simi` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_simi` datetime DEFAULT NULL,
  `nama_pemohon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jabatan_pemohon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email_pemohon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `divisi_pemohon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `perusahaan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `instalasi_id` int DEFAULT NULL,
  `nama_penanggung_jawab` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email_penanggung_jawab` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jabatan_penanggung_jawab` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `keperluan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `petunjuk` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `lampiran` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `submited_date` datetime DEFAULT NULL,
  `canceled_date` datetime DEFAULT NULL,
  `canceled_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `rejected_date` datetime DEFAULT NULL,
  `verified_date` datetime DEFAULT NULL,
  `verified_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `closed_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `closed_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`simi_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28610 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_simi_copy1`
--

LOCK TABLES `tbl_simi_copy1` WRITE;
/*!40000 ALTER TABLE `tbl_simi_copy1` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_simi_copy1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_simi_gdmr1`
--

DROP TABLE IF EXISTS `tbl_simi_gdmr1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_simi_gdmr1` (
  `id_simi` int NOT NULL AUTO_INCREMENT,
  `no_simi` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_simi` datetime DEFAULT NULL,
  `nama_pemohon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jabatan_pemohon` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email_pemohon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `divisi_pemohon` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `perusahaan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `id_stasiun` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_penanggung_jawab` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email_penanggung_jawab` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jabatan_penanggung_jawab` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `keperluan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `status` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_approval` datetime DEFAULT NULL,
  `petunjuk` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `keterangan` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `lampiran` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_close` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_simi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9698 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_simi_gdmr1`
--

LOCK TABLES `tbl_simi_gdmr1` WRITE;
/*!40000 ALTER TABLE `tbl_simi_gdmr1` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_simi_gdmr1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_simi_gdmr2`
--

DROP TABLE IF EXISTS `tbl_simi_gdmr2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_simi_gdmr2` (
  `id_simi` int NOT NULL AUTO_INCREMENT,
  `no_simi` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_simi` datetime DEFAULT NULL,
  `nama_pemohon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jabatan_pemohon` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email_pemohon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `divisi_pemohon` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `perusahaan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `id_stasiun` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_penanggung_jawab` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email_penanggung_jawab` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jabatan_penanggung_jawab` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `keperluan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_approval` datetime DEFAULT NULL,
  `petunjuk` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `keterangan` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `lampiran` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_close` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_simi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5551 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_simi_gdmr2`
--

LOCK TABLES `tbl_simi_gdmr2` WRITE;
/*!40000 ALTER TABLE `tbl_simi_gdmr2` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_simi_gdmr2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_simi_gdmr3`
--

DROP TABLE IF EXISTS `tbl_simi_gdmr3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_simi_gdmr3` (
  `id_simi` int NOT NULL AUTO_INCREMENT,
  `no_simi` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_simi` datetime DEFAULT NULL,
  `nama_pemohon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jabatan_pemohon` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email_pemohon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `divisi_pemohon` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `perusahaan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `id_stasiun` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_penanggung_jawab` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email_penanggung_jawab` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jabatan_penanggung_jawab` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `keperluan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_approval` datetime DEFAULT NULL,
  `petunjuk` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `keterangan` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `lampiran` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_close` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_simi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5355 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_simi_gdmr3`
--

LOCK TABLES `tbl_simi_gdmr3` WRITE;
/*!40000 ALTER TABLE `tbl_simi_gdmr3` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_simi_gdmr3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_wilayah`
--

DROP TABLE IF EXISTS `tbl_wilayah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_wilayah` (
  `wilayah_id` int NOT NULL AUTO_INCREMENT,
  `inisial` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `nama_wilayah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`wilayah_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_wilayah`
--

LOCK TABLES `tbl_wilayah` WRITE;
/*!40000 ALTER TABLE `tbl_wilayah` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_wilayah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_detail_kegiatan`
--

DROP TABLE IF EXISTS `v_detail_kegiatan`;
/*!50001 DROP VIEW IF EXISTS `v_detail_kegiatan`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_detail_kegiatan` AS SELECT 
 1 AS `detail_kegiatan_id`,
 1 AS `no_simi`,
 1 AS `kegiatan`,
 1 AS `dokumen`,
 1 AS `created_at`,
 1 AS `updated_at`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_detail_lokasi`
--

DROP TABLE IF EXISTS `v_detail_lokasi`;
/*!50001 DROP VIEW IF EXISTS `v_detail_lokasi`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_detail_lokasi` AS SELECT 
 1 AS `detail_lokasi_id`,
 1 AS `no_simi`,
 1 AS `lokasi`,
 1 AS `nilai`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_instalasi`
--

DROP TABLE IF EXISTS `v_instalasi`;
/*!50001 DROP VIEW IF EXISTS `v_instalasi`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_instalasi` AS SELECT 
 1 AS `instalasi_id`,
 1 AS `nama_instalasi`,
 1 AS `alamat`,
 1 AS `no_telp`,
 1 AS `nama_wilayah`,
 1 AS `area_id`,
 1 AS `nama_area`,
 1 AS `pic_id`,
 1 AS `nama_pic`,
 1 AS `email_pic`,
 1 AS `jabatan_pic`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_pic`
--

DROP TABLE IF EXISTS `v_pic`;
/*!50001 DROP VIEW IF EXISTS `v_pic`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_pic` AS SELECT 
 1 AS `pic_id`,
 1 AS `area_id`,
 1 AS `nama_wilayah`,
 1 AS `nama_area`,
 1 AS `user_id`,
 1 AS `nama_pic`,
 1 AS `jabatan_pic`,
 1 AS `email_pic`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_simi`
--

DROP TABLE IF EXISTS `v_simi`;
/*!50001 DROP VIEW IF EXISTS `v_simi`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_simi` AS SELECT 
 1 AS `simi_id`,
 1 AS `no_simi`,
 1 AS `tgl_simi`,
 1 AS `nama_pemohon`,
 1 AS `jabatan_pemohon`,
 1 AS `email_pemohon`,
 1 AS `divisi_pemohon`,
 1 AS `perusahaan`,
 1 AS `instalasi_id`,
 1 AS `nama_instalasi`,
 1 AS `alamat`,
 1 AS `no_telp`,
 1 AS `nama_wilayah`,
 1 AS `area_id`,
 1 AS `nama_area`,
 1 AS `nama_penanggung_jawab`,
 1 AS `email_penanggung_jawab`,
 1 AS `jabatan_penanggung_jawab`,
 1 AS `tgl_mulai`,
 1 AS `tgl_selesai`,
 1 AS `keperluan`,
 1 AS `status`,
 1 AS `approved_date`,
 1 AS `petunjuk`,
 1 AS `keterangan`,
 1 AS `lampiran`,
 1 AS `submited_date`,
 1 AS `canceled_date`,
 1 AS `canceled_by`,
 1 AS `rejected_date`,
 1 AS `verified_date`,
 1 AS `verified_by`,
 1 AS `closed_date`,
 1 AS `closed_by`,
 1 AS `tanggal`,
 1 AS `bulan`,
 1 AS `tahun`,
 1 AS `created_at`,
 1 AS `updated_at`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_detail_kegiatan`
--

/*!50001 DROP VIEW IF EXISTS `v_detail_kegiatan`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_detail_kegiatan` AS select `tbl_detail_kegiatan`.`detail_kegiatan_id` AS `detail_kegiatan_id`,`tbl_detail_kegiatan`.`no_simi` AS `no_simi`,`tbl_detail_kegiatan`.`kegiatan` AS `kegiatan`,`tbl_kegiatan`.`dokumen` AS `dokumen`,`tbl_detail_kegiatan`.`created_at` AS `created_at`,`tbl_detail_kegiatan`.`updated_at` AS `updated_at` from (`tbl_detail_kegiatan` join `tbl_kegiatan` on(((convert(`tbl_detail_kegiatan`.`kegiatan` using utf8mb4) collate utf8mb4_general_ci) = (`tbl_kegiatan`.`nama_kegiatan` collate utf8mb4_general_ci)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_detail_lokasi`
--

/*!50001 DROP VIEW IF EXISTS `v_detail_lokasi`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_detail_lokasi` AS select `tbl_detail_lokasi`.`detail_lokasi_id` AS `detail_lokasi_id`,`tbl_detail_lokasi`.`no_simi` AS `no_simi`,`tbl_detail_lokasi`.`lokasi` AS `lokasi`,`tbl_lokasi_kegiatan`.`nilai` AS `nilai` from (`tbl_detail_lokasi` join `tbl_lokasi_kegiatan` on(((convert(`tbl_detail_lokasi`.`lokasi` using utf8mb4) collate utf8mb4_general_ci) = (`tbl_lokasi_kegiatan`.`nama_lokasi` collate utf8mb4_general_ci)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_instalasi`
--

/*!50001 DROP VIEW IF EXISTS `v_instalasi`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_instalasi` AS select `tbl_instalasi`.`instalasi_id` AS `instalasi_id`,`tbl_instalasi`.`nama_instalasi` AS `nama_instalasi`,`tbl_instalasi`.`alamat` AS `alamat`,`tbl_instalasi`.`no_telp` AS `no_telp`,`tbl_instalasi`.`nama_wilayah` AS `nama_wilayah`,`tbl_instalasi`.`area_id` AS `area_id`,`tbl_area`.`nama_area` AS `nama_area`,`tbl_instalasi`.`pic_id` AS `pic_id`,`tbl_instalasi`.`nama_pic` AS `nama_pic`,`tbl_instalasi`.`email_pic` AS `email_pic`,`tbl_instalasi`.`jabatan_pic` AS `jabatan_pic` from (`tbl_instalasi` join `tbl_area` on((`tbl_instalasi`.`area_id` = `tbl_area`.`area_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_pic`
--

/*!50001 DROP VIEW IF EXISTS `v_pic`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_pic` AS select `tbl_pic`.`pic_id` AS `pic_id`,`tbl_pic`.`area_id` AS `area_id`,`tbl_area`.`nama_wilayah` AS `nama_wilayah`,`tbl_area`.`nama_area` AS `nama_area`,`tbl_pic`.`user_id` AS `user_id`,`tbl_pic`.`nama_pic` AS `nama_pic`,`tbl_pic`.`jabatan_pic` AS `jabatan_pic`,`tbl_pic`.`email_pic` AS `email_pic` from (`tbl_pic` join `tbl_area` on((`tbl_pic`.`area_id` = `tbl_area`.`area_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_simi`
--

/*!50001 DROP VIEW IF EXISTS `v_simi`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_simi` AS select `tbl_simi`.`simi_id` AS `simi_id`,`tbl_simi`.`no_simi` AS `no_simi`,`tbl_simi`.`tgl_simi` AS `tgl_simi`,`tbl_simi`.`nama_pemohon` AS `nama_pemohon`,`tbl_simi`.`jabatan_pemohon` AS `jabatan_pemohon`,`tbl_simi`.`email_pemohon` AS `email_pemohon`,`tbl_simi`.`divisi_pemohon` AS `divisi_pemohon`,`tbl_simi`.`perusahaan` AS `perusahaan`,`tbl_simi`.`instalasi_id` AS `instalasi_id`,`tbl_instalasi`.`nama_instalasi` AS `nama_instalasi`,`tbl_instalasi`.`alamat` AS `alamat`,`tbl_instalasi`.`no_telp` AS `no_telp`,`tbl_instalasi`.`nama_wilayah` AS `nama_wilayah`,`tbl_instalasi`.`area_id` AS `area_id`,`tbl_area`.`nama_area` AS `nama_area`,`tbl_simi`.`nama_penanggung_jawab` AS `nama_penanggung_jawab`,`tbl_simi`.`email_penanggung_jawab` AS `email_penanggung_jawab`,`tbl_simi`.`jabatan_penanggung_jawab` AS `jabatan_penanggung_jawab`,`tbl_simi`.`tgl_mulai` AS `tgl_mulai`,`tbl_simi`.`tgl_selesai` AS `tgl_selesai`,`tbl_simi`.`keperluan` AS `keperluan`,`tbl_simi`.`status` AS `status`,`tbl_simi`.`approved_date` AS `approved_date`,`tbl_simi`.`petunjuk` AS `petunjuk`,`tbl_simi`.`keterangan` AS `keterangan`,`tbl_simi`.`lampiran` AS `lampiran`,`tbl_simi`.`submited_date` AS `submited_date`,`tbl_simi`.`canceled_date` AS `canceled_date`,`tbl_simi`.`canceled_by` AS `canceled_by`,`tbl_simi`.`rejected_date` AS `rejected_date`,`tbl_simi`.`verified_date` AS `verified_date`,`tbl_simi`.`verified_by` AS `verified_by`,`tbl_simi`.`closed_date` AS `closed_date`,`tbl_simi`.`closed_by` AS `closed_by`,date_format(`tbl_simi`.`tgl_simi`,'%Y-%m-%d') AS `tanggal`,month(`tbl_simi`.`tgl_simi`) AS `bulan`,year(`tbl_simi`.`tgl_simi`) AS `tahun`,`tbl_simi`.`created_at` AS `created_at`,`tbl_simi`.`updated_at` AS `updated_at` from ((`tbl_simi` join `tbl_instalasi` on((`tbl_simi`.`instalasi_id` = `tbl_instalasi`.`instalasi_id`))) join `tbl_area` on((`tbl_instalasi`.`area_id` = `tbl_area`.`area_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-27 11:06:22
