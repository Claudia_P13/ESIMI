<?php

namespace App\Http\Controllers;

use App\Exports\ExportCustomSIMI;
use App\Exports\ExportMonthlySIMI;
use App\Exports\ExportTodaySIMI;
use App\Exports\ExportYearSIMI;
use App\Exports\RealisasiAll;
use App\Models\ViewSIMI;
use App\Models\Wilayah;
use Illuminate\Http\Request;
use DB;
use Exception;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{

    public function exportIndexTahun($year)
    {
        try {
            $tahun = decrypt($year);
        } catch (Exception $e) {
            abort(404);
        }

        $tanggalMenit = date("dmYHis");

        $namaFile = "rekap_permohonan_simi".$tanggalMenit.".xlsx";
        return Excel::download(new ExportYearSIMI($tahun), $namaFile);
    }

    public function exportIndexToday()
    {
        $tanggalMenit = date("dmYHis");
        $tahun = date("Y");
        $tanggal = date("Y-m-d");

        $namaFile = "rekap_permohonan_simi".$tanggalMenit.".xlsx";
        return Excel::download(new ExportTodaySIMI($tahun, $tanggal), $namaFile);
    }

    public function exportIndexMonthly($bulan1, $bulan2, $year)
    {
        try {
            $bln1 = decrypt($bulan1);
            $bln2 = decrypt($bulan2);
            $tahun = decrypt($year);
        } catch (Exception $e) {
            abort(404);
        }

        $tanggalMenit = date("dmYHis");

        $namaFile = "rekap_permohonan_simi".$tanggalMenit.".xlsx";
        return Excel::download(new ExportMonthlySIMI($bln1, $bln2, $tahun), $namaFile);
    }

    public function exportIndexCustom($tanggal1, $tanggal2)
    {
        try {
            $date1 = decrypt($tanggal1);
            $date2 = decrypt($tanggal2);
        } catch (Exception $e) {
            abort(404);
        }

        $tanggalMenit = date("dmYHis");

        $namaFile = "rekap_permohonan_simi".$tanggalMenit.".xlsx";
        return Excel::download(new ExportCustomSIMI($date1, $date2), $namaFile);
    }

    public function exportIndexWilayah($year, $wilayah, $area)
    {
        try {
            $tahun = decrypt($year);
            $wil = decrypt($wilayah);
        } catch (Exception $e) {
            abort(404);
        }

        $tanggalMenit = date("dmYHis");

        $namaFile = "rekap_permohonan_simi".$tanggalMenit.".xlsx";
        return Excel::download(new ExportCustomSIMI($tahun, $wil, $area), $namaFile);
    }

    public function printIndexTahun($year)
    {
        try {
            $tahun = decrypt($year);
        } catch (Exception $e) {
            abort(404);
        }

        $email = session('user')->email;
        $role = session('user')->role;

        if ($role == 'Admin'){
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->orderBy('simi_id', 'ASC')->get();
        }else{
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('email_pemohon', $email)->orderBy('simi_id', 'ASC')->get();
        }

        return view('print.printIndexSIMI')
            ->with([
                'tahun' => $tahun,
                'dataSIMI' => $simi,
            ]);
    }

    public function printIndexToday()
    {
        $tahun = date("Y");
        $tanggal = date("Y-m-d");

        $email = session('user')->email;
        $role = session('user')->role;

        if ($role == 'Admin'){
            $simi = ViewSIMI::whereDate('tgl_simi', $tanggal)->orderBy('simi_id', 'ASC')->get();
        }else{
            $simi = ViewSIMI::whereDate('tgl_simi', $tanggal)->where('email_pemohon', $email)->orderBy('simi_id', 'ASC')->get();
        }

        return view('print.printIndexSIMI')
            ->with([
                'tahun' => $tahun,
                'dataSIMI' => $simi,
            ]);
    }

    public function printIndexMonthly($bulan1, $bulan2, $year)
    {
        try {
            $tahun = decrypt($year);
            $bln1 = decrypt($bulan1);
            $bln2 = decrypt($bulan2);
        } catch (Exception $e) {
            abort(404);
        }

        $email = session('user')->email;
        $role = session('user')->role;

        if ($role == 'Admin'){
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->whereBetween('bulan', [$bln1, $bln2])->orderBy('simi_id', 'ASC')->get();
        }else{
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->whereBetween('bulan', [$bln1, $bln2])->where('email_pemohon', $email)->orderBy('simi_id', 'ASC')->get();
        }

        return view('print.printIndexSIMI')
            ->with([
                'tahun' => $tahun,
                'dataSIMI' => $simi,
            ]);
    }

    public function printIndexCustom($tanggal1, $tanggal2)
    {
        try {
            $date1 = decrypt($tanggal1);
            $date2 = decrypt($tanggal2);
        } catch (Exception $e) {
            abort(404);
        }

        $tgl1 = date("Y-m-d", strtotime($date1));
        $tgl2 = date("Y-m-d", strtotime($date2));

        $tahun = date("Y", strtotime($date1));

        $email = session('user')->email;
        $role = session('user')->role;

        if ($role == 'Admin'){
            $simi = ViewSIMI::whereBetween('tanggal', [$tgl1, $tgl2])->orderBy('simi_id', 'ASC')->get();
        }else{
            $simi = ViewSIMI::whereBetween('tanggal', [$tgl1, $tgl2])->where('email_pemohon', $email)->orderBy('simi_id', 'ASC')->get();
        }

        return view('print.printIndexSIMI')
            ->with([
                'tahun' => $tahun,
                'dataSIMI' => $simi,
            ]);
    }

    public function printIndexWilayah($year, $wilayah, $area)
    {
        try {
            $tahun = decrypt($year);
            $wil = decrypt($wilayah);
        } catch (Exception $e) {
            abort(404);
        }

        $email = session('user')->email;
        $role = session('user')->role;

        if ($role == 'Admin'){
            if ($area == 'All Area/Zona'){
                $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_wilayah', $wil)->orderBy('simi_id', 'ASC')->get();
            }else{
                $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_wilayah', $wil)->where('nama_area', $area)->orderBy('simi_id', 'ASC')->get();
            }
        }else{
            if ($area == 'All Area/Zona'){
                $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_wilayah', $wilayah)->where('nama_area', $area)->where('email_pemohon', $email)->orderBy('simi_id', 'ASC')->get();
            }else{
                $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_wilayah', $wilayah)->where('email_pemohon', $email)->orderBy('simi_id', 'ASC')->get();
            }
        }

        return view('print.printIndexSIMI')
            ->with([
                'tahun' => $tahun,
                'dataSIMI' => $simi,
            ]);
    }
}
