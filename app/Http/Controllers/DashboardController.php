<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\ViewSIMI;
use App\Models\Wilayah;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use Mail;

class DashboardController extends Controller
{
    public function index()
    {
        $tahun = date("Y");
        $wilayah = "Operation & Maintenance Management";
        $jumlahVisitor = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            ->whereNotIn('status', ['Canceled'])
            ->count();

        $jumlahProcess = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            ->whereIn('status', ['Draft', 'Submited', 'Approved', 'Rejected'])
            ->count();
        $jumlahVisited = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            ->whereIn('status', ['Visited'])
            ->count();
        $jumlahClosed = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            ->whereIn('status', ['Closed'])
            ->count();

        $area = DB::table('v_simi')
            ->select(DB::raw('nama_area, count(*) as total'))
            ->whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            ->whereNotIn('status', ['Canceled'])
            ->groupBy('nama_area')
            ->get();

        $instalasi = DB::table('v_simi')
            ->select(DB::raw('nama_instalasi, count(*) as total'))
            ->whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            //->whereNotIn('status', ['Draft', 'Submited', 'Approved', 'Canceled', 'Rejected'])
            ->whereNotIn('status', ['Canceled'])
            ->groupBy('nama_instalasi')
            ->get();

        $dataInstalasi = [];
        foreach ($instalasi as $in) {
            $dataInstalasi[] = $in->nama_instalasi;
        }


        $dataJumlahInstalasi = [];
        foreach ($instalasi as $in2) {
            $dataJumlahInstalasi[] = round($in2->total);
        }

        $monthly = DB::table('v_simi')
            ->select(DB::raw('bulan, count(*) as total'))
            ->whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            //->whereNotIn('status', ['Draft', 'Submited', 'Approved', 'Canceled', 'Rejected'])
            ->whereNotIn('status', ['Canceled'])
            ->groupBy('bulan')
            ->get();

        function bulan($bln)
        {
            $bulan = $bln;
            switch ($bulan) {
                case 1 :
                    $bulan = "Jan";
                    break;
                case 2 :
                    $bulan = "Feb";
                    break;
                case 3 :
                    $bulan = "Mar";
                    break;
                case 4 :
                    $bulan = "Apr";
                    break;
                case 5 :
                    $bulan = "Mei";
                    break;
                case 6 :
                    $bulan = "Jun";
                    break;
                case 7 :
                    $bulan = "Jul";
                    break;
                case 8 :
                    $bulan = "Agu";
                    break;
                case 9 :
                    $bulan = "Sep";
                    break;
                case 10 :
                    $bulan = "Okt";
                    break;
                case 11 :
                    $bulan = "Nov";
                    break;
                case 12 :
                    $bulan = "Des";
                    break;
            }
            return $bulan;
        }

        $dataBulan = [];
        foreach ($monthly as $mr) {
            $dataBulan[] = bulan($mr->bulan);
        }


        $dataJumlahBulan = [];
        foreach ($monthly as $mr2) {
            $dataJumlahBulan[] = round($mr2->total);
        }

        $satker = Wilayah::orderBy('nama_wilayah')->get();
        return view('home.dashboard')
            ->with([
                'tahun' => $tahun,
                'wilayah' => $wilayah,
                'jumlahVisitor' => $jumlahVisitor,
                'jumlahProcess' => $jumlahProcess,
                'jumlahVisited' => $jumlahVisited,
                'jumlahClosed' => $jumlahClosed,
                'dataWilayah' => $satker,
                'instalasi' => $instalasi,
                'dataInstalasi' => $dataInstalasi,
                'dataJumlahInstalasi' => $dataJumlahInstalasi,
                'monthly' => $monthly,
                'dataBulan' => $dataBulan,
                'dataJumlahBulan' => $dataJumlahBulan,
                'dataArea' => $area,
            ]);
    }

    public function postYear(Request $request)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'wilayah' => 'required',
        ]);

        return redirect(route('indexYearDashboard', ['year' => encrypt($request->tahun), 'wil' => encrypt($request->wilayah)]));
    }

    public function indexYear($year, $wil)
    {
        try {
            $tahun = decrypt($year);
            $wilayah = decrypt($wil);
        } catch (Exception $e) {
            abort(404);
        }

        $jumlahVisitor = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            ->whereNotIn('status', ['Canceled'])
            ->count();

        $jumlahProcess = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            ->whereIn('status', ['Draft', 'Submited', 'Approved', 'Rejected'])
            ->count();
        $jumlahVisited = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            ->whereIn('status', ['Visited'])
            ->count();
        $jumlahClosed = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            ->whereIn('status', ['Closed'])
            ->count();

        $area = DB::table('v_simi')
            ->select(DB::raw('nama_area, count(*) as total'))
            ->whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            ->whereNotIn('status', ['Canceled'])
            ->groupBy('nama_area')
            ->get();

        $instalasi = DB::table('v_simi')
            ->select(DB::raw('nama_instalasi, count(*) as total'))
            ->whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            //->whereNotIn('status', ['Draft', 'Submited', 'Approved', 'Canceled', 'Rejected'])
            ->whereNotIn('status', ['Canceled'])
            ->groupBy('nama_instalasi')
            ->get();

        $dataInstalasi = [];
        foreach ($instalasi as $in) {
            $dataInstalasi[] = $in->nama_instalasi;
        }


        $dataJumlahInstalasi = [];
        foreach ($instalasi as $in2) {
            $dataJumlahInstalasi[] = round($in2->total);
        }

        $monthly = DB::table('v_simi')
            ->select(DB::raw('bulan, count(*) as total'))
            ->whereYear('tgl_simi', $tahun)
            ->where('nama_wilayah', $wilayah)
            //->whereNotIn('status', ['Draft', 'Submited', 'Approved', 'Canceled', 'Rejected'])
            ->whereNotIn('status', ['Canceled'])
            ->groupBy('bulan')
            ->get();

        function bulan($bln)
        {
            $bulan = $bln;
            switch ($bulan) {
                case 1 :
                    $bulan = "Jan";
                    break;
                case 2 :
                    $bulan = "Feb";
                    break;
                case 3 :
                    $bulan = "Mar";
                    break;
                case 4 :
                    $bulan = "Apr";
                    break;
                case 5 :
                    $bulan = "Mei";
                    break;
                case 6 :
                    $bulan = "Jun";
                    break;
                case 7 :
                    $bulan = "Jul";
                    break;
                case 8 :
                    $bulan = "Agu";
                    break;
                case 9 :
                    $bulan = "Sep";
                    break;
                case 10 :
                    $bulan = "Okt";
                    break;
                case 11 :
                    $bulan = "Nov";
                    break;
                case 12 :
                    $bulan = "Des";
                    break;
            }
            return $bulan;
        }

        $dataBulan = [];
        foreach ($monthly as $mr) {
            $dataBulan[] = bulan($mr->bulan);
        }


        $dataJumlahBulan = [];
        foreach ($monthly as $mr2) {
            $dataJumlahBulan[] = round($mr2->total);
        }

        $satker = Wilayah::orderBy('nama_wilayah')->get();
        return view('home.dashboard')
            ->with([
                'tahun' => $tahun,
                'wilayah' => $wilayah,
                'jumlahVisitor' => $jumlahVisitor,
                'jumlahProcess' => $jumlahProcess,
                'jumlahVisited' => $jumlahVisited,
                'jumlahClosed' => $jumlahClosed,
                'dataWilayah' => $satker,
                'dataInstalasi' => $dataInstalasi,
                'instalasi' => $instalasi,
                'dataJumlahInstalasi' => $dataJumlahInstalasi,
                'monthly' => $monthly,
                'dataBulan' => $dataBulan,
                'dataJumlahBulan' => $dataJumlahBulan,
                'dataArea' => $area,
            ]);

    }

    public function postArea(Request $request)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'area' => 'required',
        ]);

        return redirect(route('indexAreaDashboard', ['year' => encrypt($request->tahun), 'area' => encrypt($request->area)]));
    }

    public function indexArea($year, $area)
    {
        try {
            $tahun = decrypt($year);
            $zona = decrypt($area);
        } catch (Exception $e) {
            abort(404);
        }

        $jumlahVisitor = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_area', $zona)
            //->whereNotIn('status', ['Draft', 'Submited', 'Approved', 'Canceled', 'Rejected'])
            ->whereNotIn('status', ['Canceled'])
            ->count();
        $jumlahProcess = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_area', $zona)
            ->whereIn('status', ['Draft', 'Submited', 'Approved', 'Rejected'])
            ->count();
        $jumlahVisited = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_area', $zona)
            ->whereIn('status', ['Visited'])
            ->count();
        $jumlahClosed = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_area', $zona)
            ->whereIn('status', ['Closed'])
            ->count();

        $instalasi = DB::table('v_simi')
            ->select(DB::raw('nama_instalasi, count(*) as total'))
            ->whereYear('tgl_simi', $tahun)
            ->where('nama_area', $zona)
            ->whereNotIn('status', ['Canceled'])
            //->whereNotIn('status', ['Draft', 'Submited', 'Approved', 'Canceled', 'Rejected'])
            ->groupBy('nama_instalasi')
            ->get();

        $dataInstalasi = [];
        foreach ($instalasi as $in) {
            $dataInstalasi[] = $in->nama_instalasi;
        }


        $dataJumlahInstalasi = [];
        foreach ($instalasi as $in2) {
            $dataJumlahInstalasi[] = round($in2->total);
        }

        $monthly = DB::table('v_simi')
            ->select(DB::raw('bulan, count(*) as total'))
            ->whereYear('tgl_simi', $tahun)
            ->where('nama_area', $zona)
            ->whereNotIn('status', ['Canceled'])
//            ->whereNotIn('status', ['Draft', 'Submited', 'Approved', 'Canceled', 'Rejected'])
            ->groupBy('bulan')
            ->get();

        function bulan($bln)
        {
            $bulan = $bln;
            switch ($bulan) {
                case 1 :
                    $bulan = "Jan";
                    break;
                case 2 :
                    $bulan = "Feb";
                    break;
                case 3 :
                    $bulan = "Mar";
                    break;
                case 4 :
                    $bulan = "Apr";
                    break;
                case 5 :
                    $bulan = "Mei";
                    break;
                case 6 :
                    $bulan = "Jun";
                    break;
                case 7 :
                    $bulan = "Jul";
                    break;
                case 8 :
                    $bulan = "Agu";
                    break;
                case 9 :
                    $bulan = "Sep";
                    break;
                case 10 :
                    $bulan = "Okt";
                    break;
                case 11 :
                    $bulan = "Nov";
                    break;
                case 12 :
                    $bulan = "Des";
                    break;
            }
            return $bulan;
        }

        $dataBulan = [];
        foreach ($monthly as $mr) {
            $dataBulan[] = bulan($mr->bulan);
        }


        $dataJumlahBulan = [];
        foreach ($monthly as $mr2) {
            $dataJumlahBulan[] = round($mr2->total);
        }

        $satker = Area::orderBy('nama_area')->get();
        $simi = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_area', $zona)
            ->whereIn('status', ['Visited'])
            ->orderBy('simi_id', 'DESC')->get();
        $jumlahSimi = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_area', $zona)
            ->whereIn('status', ['Visited'])
            ->orderBy('simi_id', 'DESC')->count();
        return view('home.dashboardArea')
            ->with([
                'tahun' => $tahun,
                'dataSIMI' => $simi,
                'jumlahSIMI' => $jumlahSimi,
                'area' => $zona,
                'jumlahVisitor' => $jumlahVisitor,
                'jumlahProcess' => $jumlahProcess,
                'jumlahVisited' => $jumlahVisited,
                'jumlahClosed' => $jumlahClosed,
                'dataArea' => $satker,
                'dataInstalasi' => $dataInstalasi,
                'instalasi' => $instalasi,
                'dataJumlahInstalasi' => $dataJumlahInstalasi,
                'monthly' => $monthly,
                'dataBulan' => $dataBulan,
                'dataJumlahBulan' => $dataJumlahBulan,
            ]);

    }

    public function postInstalasi(Request $request)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'instalasi' => 'required',
        ]);

        return redirect(route('indexInstalasiDashboard', ['year' => encrypt($request->tahun), 'stasiun' => encrypt($request->instalasi)]));
    }

    public function indexInstalasi($year, $stasiun)
    {
        try {
            $tahun = decrypt($year);
            $instalasi = decrypt($stasiun);
        } catch (Exception $e) {
            abort(404);
        }

        $jumlahVisitor = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_instalasi', $instalasi)
            //->whereNotIn('status', ['Draft', 'Submited', 'Approved', 'Canceled', 'Rejected'])
            ->whereNotIn('status', ['Canceled'])
            ->count();
        $jumlahProcess = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_instalasi', $instalasi)
            ->whereIn('status', ['Draft', 'Submited', 'Approved', 'Rejected'])
            ->count();
        $jumlahVisited = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_instalasi', $instalasi)
            ->whereIn('status', ['Visited'])
            ->count();
        $jumlahClosed = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_instalasi', $instalasi)
            ->whereIn('status', ['Closed'])
            ->count();
        $monthly = DB::table('v_simi')
            ->select(DB::raw('bulan, count(*) as total'))
            ->whereYear('tgl_simi', $tahun)
            ->where('nama_instalasi', $instalasi)
            ->whereNotIn('status', ['Canceled'])
            //->whereNotIn('status', ['Draft', 'Submited', 'Approved', 'Canceled', 'Rejected'])
            ->groupBy('bulan')
            ->get();

        function bulan($bln)
        {
            $bulan = $bln;
            switch ($bulan) {
                case 1 :
                    $bulan = "Jan";
                    break;
                case 2 :
                    $bulan = "Feb";
                    break;
                case 3 :
                    $bulan = "Mar";
                    break;
                case 4 :
                    $bulan = "Apr";
                    break;
                case 5 :
                    $bulan = "Mei";
                    break;
                case 6 :
                    $bulan = "Jun";
                    break;
                case 7 :
                    $bulan = "Jul";
                    break;
                case 8 :
                    $bulan = "Agu";
                    break;
                case 9 :
                    $bulan = "Sep";
                    break;
                case 10 :
                    $bulan = "Okt";
                    break;
                case 11 :
                    $bulan = "Nov";
                    break;
                case 12 :
                    $bulan = "Des";
                    break;
            }
            return $bulan;
        }

        $dataBulan = [];
        foreach ($monthly as $mr) {
            $dataBulan[] = bulan($mr->bulan);
        }


        $dataJumlahBulan = [];
        foreach ($monthly as $mr2) {
            $dataJumlahBulan[] = round($mr2->total);
        }

        $simi = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_instalasi', $instalasi)
            ->whereIn('status', ['Visited'])
            ->orderBy('simi_id', 'DESC')->get();
        $jumlahSimi = ViewSIMI::whereYear('tgl_simi', $tahun)
            ->where('nama_instalasi', $instalasi)
            ->whereIn('status', ['Visited'])
            ->orderBy('simi_id', 'DESC')->count();
        return view('home.dashboardInstalasi')
            ->with([
                'tahun' => $tahun,
                'dataSIMI' => $simi,
                'jumlahSIMI' => $jumlahSimi,
                'instalasi' => $instalasi,
                'jumlahVisitor' => $jumlahVisitor,
                'jumlahProcess' => $jumlahProcess,
                'jumlahVisited' => $jumlahVisited,
                'jumlahClosed' => $jumlahClosed,
                'monthly' => $monthly,
                'dataBulan' => $dataBulan,
                'dataJumlahBulan' => $dataJumlahBulan,
            ]);

    }
}
