<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class LoginController extends Controller
{
    public function auth()
    {
        if (!empty(session('user'))) {
            return redirect(route('dashboard'));
        } else {
            return redirect(route('login'));
        }
    }

    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $tahun = date("Y");

        $user = DB::table('tbl_auth')
            ->select(DB::raw('count(*) as jumlah'))
            ->where([
                ['username', '=', strtolower($request->username)],
                ['status', 'Active'],
            ])
            ->first();

        if ($user->jumlah == 0) {
            return redirect()->back()->with('credential', 'Username yang anda masukkan tidak dikenal');
        } else {

            $credentialUsername = [
                'username' => strtolower($request->username),
                'password' => $request->password
            ];

            if (auth()->attempt($credentialUsername)) {
                $data = DB::table('tbl_auth')
                    ->select('tbl_auth.*')
                    ->where([
                        ['username', strtolower($request->username)],
                        ['status', 'Active'],
                    ])
                    ->first();

                if ($data->role == "Admin"){
                    session(['user' => $data]);
                    return redirect(route('dashboard'));
                }elseif ($data->role == "Approver"){
                    $zona = DB::table('v_instalasi')
                        ->select(DB::raw('nama_area'))
                        ->where('email_pic', $data->email)
                        ->groupBy('nama_area')
                        ->first();
                    session(['user' => $data, 'area' => $zona->nama_area]);
                    return redirect(route('indexAreaDashboard', ['year' => encrypt($tahun), 'area' => encrypt($zona->nama_area)]));
                }elseif ($data->role == "Admin Instalasi"){
                    $instalasi = DB::table('v_instalasi')
                        ->select(DB::raw('nama_instalasi'))
                        ->where('nama_instalasi', $data->nama)
                        ->groupBy('nama_instalasi')
                        ->first();
                    session(['user' => $data, 'instalasi' => $instalasi->nama_instalasi]);
                    return redirect(route('indexInstalasiDashboard', ['year' => encrypt($tahun), 'stasiun' => encrypt($instalasi->nama_instalasi)]));
                }else{
                    session(['user' => $data]);
                    return redirect(route('dashboard'));
                }
            } else {
                $domain = 'pertamina\\';
                $ldap['sAMAccountName'] = strtolower($request->username);
                $ldap['pass'] = $request->password;
                $ldap['host'] = '10.10.10.17';
                $ldap['port'] = 389;

                $ldap['conn'] = ldap_connect($ldap['host'], $ldap['port'])
                or die("Could not conenct to {$ldap['host']}");

                ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
                try {
                    $ldap['bind'] = ldap_bind($ldap['conn'], $domain . $ldap['sAMAccountName'], $ldap['pass']);
                } catch (Exception $e) {
                    $domain = 'corp\\';
                    $ldap['user'] = strtolower($request->username);
                    $ldap['pass'] = $request->password;
                    $ldap['host'] = '10.10.10.17';
                    $ldap['port'] = 389;

                    $ldap['conn'] = ldap_connect($ldap['host'], $ldap['port'])
                    or die("Could not conenct to {$ldap['host']}");

                    ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
                    try {
                        $ldap['bind'] = ldap_bind($ldap['conn'], $domain . $ldap['user'], $ldap['pass']);
                    } catch (Exception $e) {
                        return redirect()->back()->with('credential', 'Kata sandi yang anda masukkan salah');
                    }
                }

                if (!$ldap['bind']) {
                    return redirect()->back()->with('credential', 'Username yang anda masukkan tidak dikenal');
                } else {
                    $data = DB::table('tbl_auth')
                        ->select('tbl_auth.*')
                        ->where([
                            ['username', $request->username],
                            ['status', 'Active'],
                        ])
                        ->first();
                    try {
                        if ($data->role == "Admin"){
                            session(['user' => $data]);
                            return redirect(route('dashboard'));
                        }elseif ($data->role == "Approver"){
                            $zona = DB::table('v_instalasi')
                                ->select(DB::raw('nama_area'))
                                ->where('email_pic', $data->email)
                                ->groupBy('nama_area')
                                ->first();
                            session(['user' => $data, 'area' => $zona->nama_area]);
                            return redirect(route('indexAreaDashboard', ['year' => encrypt($tahun), 'area' => encrypt($zona->nama_area)]));
                        }elseif ($data->role == "Admin Instalasi"){
                            $instalasi = DB::table('v_instalasi')
                                ->select(DB::raw('nama_instalasi'))
                                ->where('nama_instalasi', $data->nama)
                                ->groupBy('nama_instalasi')
                                ->first();
                            session(['user' => $data, 'instalasi' => $instalasi->nama_instalasi]);
                            return redirect(route('indexInstalasiDashboard', ['year' => encrypt($tahun), 'stasiun' => encrypt($instalasi->nama_instalasi)]));
                        }else{
                            session(['user' => $data]);
                            return redirect(route('dashboard'));
                        }
                    } catch (Exception $e) {
                        return redirect()->back()->with('credential', 'Akun tidak dikenal');
                    }
                }
                ldap_close($ldap['conn']);
            }
        }
    }

    public function logout()
    {
        auth()->logout();
        session()->flush();

        return redirect(route('login'));
    }
}
