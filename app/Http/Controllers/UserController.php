<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Perusahaan;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Exception;

class UserController extends Controller
{
    public function index(){
        $data = User::orderBy('nama','ASC')->get();
        $perusahaan = Perusahaan::all();
        return view('master.indexUser')
            ->with([
                'dataUser' => $data,
                'dataPerusahaan' => $perusahaan,
            ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email',
            'nama' => 'required',
            'jabatan' => 'required',
            'divisi' => 'required',
            'perusahaan' => 'required',
            'kategori' => 'required',
            'role' => 'required',
        ]);

        $dataUser = [
            'username' => strtolower($request->username),
            'email' => strtolower($request->email),
            'nama' => $request->nama,
            'jabatan' => $request->jabatan,
            'divisi' => $request->divisi,
            'perusahaan' => $request->perusahaan,
            'password' => bcrypt('corp.PGN'),
            'kategori' => $request->kategori,
            'role' => $request->role,
            'status' => 'Active',
            'remember_token' => Str::random(50),
        ];

        try {
            User::create($dataUser);
            return redirect()->back()->with('berhasil', 'User baru berhasil disimpan');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'User baru gagal disimpan');
        }
    }

    public function update(Request $request)
    {
        try {
            $logID = decrypt($request->userID);
        } catch (Exception $e) {
            abort(404);
        }

        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email',
            'nama' => 'required',
            'jabatan' => 'required',
            'divisi' => 'required',
            'perusahaan' => 'required',
            'kategori' => 'required',
            'role' => 'required',
        ]);

        $dataUser = [
            'username' => strtolower($request->username),
            'email' => strtolower($request->email),
            'nama' => $request->nama,
            'jabatan' => $request->jabatan,
            'divisi' => $request->divisi,
            'perusahaan' => $request->perusahaan,
            'kategori' => $request->kategori,
            'role' => $request->role,
            'status' => $request->status,
        ];

        try {
            User::where('user_id', $logID)->update($dataUser);
            return redirect()->back()->with('berhasil', 'Data user berhasil diubah');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Data user gagal diubah');
        }
    }

    public function resetPassword($userID)
    {
        try {
            $logID = decrypt($userID);
        } catch (Exception $e) {
            abort(404);
        }

        $dataUser = [
            'password' => bcrypt('corp.PGN'),
            'remember_token' => Str::random(50),
        ];

        try {
            User::where('user_id', $logID)->update($dataUser);
            return redirect()->back()->with('berhasil', 'Reset password berhasil');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Reset password gagal');
        }
    }

    public function delete($userID)
    {
        try {
            $logID = decrypt($userID);
        } catch (Exception $e) {
            abort(404);
        }

        User::where('user_id', $logID)->delete();
        return redirect()->back()->with('berhasil', 'Data user berhasil dihapus');
    }
}
