<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\PIC;
use App\Models\ViewPIC;
use App\Models\Wilayah;
use Illuminate\Http\Request;
use DB;
use Exception;

class APIController extends Controller
{
    public function dataZona($wilayahID)
    {
        $data = Area::where('nama_wilayah', $wilayahID)->get();

        echo $output = '<option value=""></option>';
        foreach ($data as $row) {
            echo $output = '<option value="' . encrypt($row->area_id) . '">' . $row->nama_area . '</option>';
        }
    }

    public function dataCariZona($wilayahID)
    {
        $data = Area::where('nama_wilayah', $wilayahID)->get();

        echo $output = '<option value="All Area/Zona">All Area/Zona</option>';
        foreach ($data as $row) {
            echo $output = '<option value="' . $row->nama_area . '">' . $row->nama_area . '</option>';
        }
    }

    public function dataPIC($areaID)
    {
        try {
            $zonaID = decrypt($areaID);
        } catch (Exception $e) {
            abort(404);
        }

        $data = PIC::where('area_id', $zonaID)->get();

        echo $output = '<option value=""></option>';
        foreach ($data as $row) {
            echo $output = '<option value="' . $row->pic_id . '">' . $row->nama_pic . '</option>';
        }
    }
}
