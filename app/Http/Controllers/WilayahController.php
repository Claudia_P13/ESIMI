<?php

namespace App\Http\Controllers;

use App\Models\Wilayah;
use Illuminate\Http\Request;
use DB;
use Exception;

class WilayahController extends Controller
{
    public function index(){
        $data = Wilayah::orderBy('nama_wilayah','ASC')->get();
        return view('master.indexWilayah')
            ->with([
                'dataWilayah' => $data,
            ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'inisial' => 'required',
            'nama' => 'required',
        ]);

        $dataWilayah = [
            'inisial' => $request->inisial,
            'nama_wilayah' => $request->nama,
        ];

        try {
            Wilayah::create($dataWilayah);
            return redirect()->back()->with('berhasil', 'Wilayah baru berhasil disimpan');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Wilayah baru gagal disimpan');
        }
    }

    public function update(Request $request)
    {
        try {
            $logID = decrypt($request->wilayahID);
        } catch (Exception $e) {
            abort(404);
        }

        $this->validate($request, [
            'inisial' => 'required',
            'nama' => 'required',
        ]);

        $dataWilayah = [
            'inisial' => $request->inisial,
            'nama_wilayah' => $request->nama,
        ];

        try {
            Wilayah::where('wilayah_id', $logID)->update($dataWilayah);
            return redirect()->back()->with('berhasil', 'Data wilayah berhasil diubah');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Data wilayah gagal diubah');
        }
    }

    public function delete($wilayahID)
    {
        try {
            $logID = decrypt($wilayahID);
        } catch (Exception $e) {
            abort(404);
        }

        Wilayah::where('wilayah_id', $logID)->delete();
        return redirect()->back()->with('berhasil', 'Data wilayah berhasil dihapus');
    }
}
