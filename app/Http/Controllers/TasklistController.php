<?php

namespace App\Http\Controllers;

use App\Models\DetailKegiatan;
use App\Models\DetailLokasiKegiatan;
use App\Models\DetailPengikut;
use App\Models\Instalasi;
use App\Models\Log;
use App\Models\SIMI;
use App\Models\User;
use App\Models\ViewDetailKegiatan;
use App\Models\ViewInstalasi;
use App\Models\ViewSIMI;
use Illuminate\Http\Request;
use DB;
use Exception;
use Mail;

class TasklistController extends Controller
{
    public function index()
    {
        if (session('user')->role == "Approver"){
            $email = session('user')->email;
            $jumlah = ViewSIMI::where('status', 'Submited')->where('email_penanggung_jawab', $email)->count();
            $data = ViewSIMI::where('status', 'Submited')->where('email_penanggung_jawab', $email)->orderBy('simi_id', 'DESC')->get();
        }elseif (session('user')->role == "Admin Instalasi"){
            $instalasi = session('instalasi');
            $jumlah = ViewSIMI::where('status', 'Approved')->where('nama_instalasi', $instalasi)->count();
            $data = ViewSIMI::where('status', 'Approved')->where('nama_instalasi', $instalasi)->orderBy('simi_id', 'DESC')->get();
        }
        return view('home.tasklist')
            ->with([
                'dataSIMI' => $data,
                'jumlahSIMI' => $jumlah,
            ]);
    }

    public function view($simiID)
    {
        try {
            $logID = decrypt($simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $simi = ViewSIMI::where('simi_id', $logID)->first();
        $instalasi = Instalasi::where('instalasi_id', $logID)->first();
        $detailKegiatan = DetailKegiatan::where('no_simi', $simi->no_simi)->get();
        $jumlahDetailKegiatan = DetailKegiatan::where('no_simi', $simi->no_simi)->count();
        $detailLokasi = DetailLokasiKegiatan::where('no_simi', $simi->no_simi)->get();
        $jumlahDetailLokasi = DetailLokasiKegiatan::where('no_simi', $simi->no_simi)->count();
        $detailPengikut = DetailPengikut::where('no_simi', $simi->no_simi)->get();
        $jumlahDetailPengikut = DetailPengikut::where('no_simi', $simi->no_simi)->count();
        $dok = ViewDetailKegiatan::where('no_simi', $simi->no_simi)->max('dokumen');

        $tglAwal = date("Y-m-d", strtotime($simi->tgl_mulai));
        $tglAkhir = date("Y-m-d", strtotime($simi->tgl_selesai));

        #Validasi Tanggal
        $tanggal1 = new \DateTime($tglAwal);
        $tanggal2 = new \DateTime($tglAkhir);

        $interval = date_diff($tanggal1, $tanggal2);
        $hari = $interval->format('%R%a');
        $jumlahHari = $hari + 1;

        $dataLog = Log::where('no_simi', $simi->no_simi)->get();
        $jumlahLog = Log::where('no_simi', $simi->no_simi)->count();

        return view('home.detailTasklist')
            ->with([
                'data' => $simi,
                'dataInstalasi' => $instalasi,
                'jumlahHari' => $jumlahHari,
                'dataDetailKegiatan' => $detailKegiatan,
                'jumlahDetailKegiatan' => $jumlahDetailKegiatan,
                'dataDetailLokasi' => $detailLokasi,
                'jumlahDetailLokasi' => $jumlahDetailLokasi,
                'dataDetailPengikut' => $detailPengikut,
                'jumlahDetailPengikut' => $jumlahDetailPengikut,
                'dok' => $dok,
                'dataLog' => $dataLog,
                'jumlahLog' => $jumlahLog,
            ]);
    }

    public function approve(Request $request)
    {
        try {
            $logID = decrypt($request->simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $tanggalMenit = date("Y-m-d H:i:s");
        $simi = ViewSIMI::where('simi_id', $logID)->first();

        $dataApprove = [
            'petunjuk' => $request->catatan,
            'status' => 'Approved',
            'approved_date' => $tanggalMenit,
        ];

        $dataLog = [
            'no_simi' => $simi->no_simi,
            'keterangan' => "Menyetujui SIMI",
            'created_by' => session('user')->email,
        ];

        //SEND EMAIL
        $receiver = User::where('email', $simi->email_pemohon)->first();

        $tanggalSIMI = date('d-m-Y', strtotime($simi->tgl_simi));
        $tanggalMulai = date('d-m-Y', strtotime($simi->tgl_mulai));
        $tanggalSelesai = date('d-m-Y', strtotime($simi->tgl_selesai));

        $dataEmail = [
            'namaPenerima' => $receiver->nama,
            'noSIMI' => $simi->no_simi,
            'tanggalSIMI' => $tanggalSIMI,
            'periodeSIMI' => "$tanggalMulai S.d $tanggalSelesai",
            'kegiatan' => $simi->keperluan,
            'instalasi' => $simi->nama_instalasi,
            'namaArea' => $simi->nama_area,
        ];

//        Mail::send('mail.notifikasi_approved', $dataEmail, function ($message) use ($receiver) {
//            $message->to($receiver->email, $receiver->nama)
//                ->subject('[APPROVED] Surat Izin Masuk Instalasi (SIMI)')
//                ->from('pgn.no.reply@pertamina.com', 'e-SIMI');
//        });

        try {
            SIMI::where('simi_id', $logID)->update($dataApprove);
            Log::create($dataLog);
            return redirect()->route('tasklist')->with('berhasil', 'Surat Izin Masuk Instalasi berhasil disetujui');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Surat Izin Masuk Instalasi gagal disetujui');
        }
    }

    public function verifikasi(Request $request)
    {
        try {
            $logID = decrypt($request->simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $tanggalMenit = date("Y-m-d H:i:s");
        $simi = ViewSIMI::where('simi_id', $logID)->first();

        $dataApprove = [
            'status' => 'Visited',
            'verified_date' => $tanggalMenit,
            'verified_by' => session('user')->email,
        ];

        $dataLog = [
            'no_simi' => $simi->no_simi,
            'keterangan' => "Verifikasi SIMI",
            'created_by' => session('user')->email,
        ];

        try {
            SIMI::where('simi_id', $logID)->update($dataApprove);
            Log::create($dataLog);
            return redirect()->route('tasklist')->with('berhasil', 'Surat Izin Masuk Instalasi berhasil disetujui');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Surat Izin Masuk Instalasi gagal disetujui');
        }
    }

    public function reject(Request $request)
    {
        try {
            $logID = decrypt($request->simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $this->validate($request, [
            'catatan' => 'required|max:150',
        ]);

        $tanggalMenit = date("Y-m-d H:i:s");
        $simi = ViewSIMI::where('simi_id', $logID)->first();

        $dataReject = [
            'status' => 'Rejected',
            'rejected_date' => $tanggalMenit,
            'keterangan' => $request->catatan,
        ];

        $dataLog = [
            'no_simi' => $simi->no_simi,
            'keterangan' => "Rejected SIMI dengan alasan $request->catatan",
            'created_by' => session('user')->email,
        ];

        //SEND EMAIL
        $receiver = User::where('email', $simi->email_pemohon)->first();

        $tanggalSIMI = date('d-m-Y', strtotime($simi->tgl_simi));
        $tanggalMulai = date('d-m-Y', strtotime($simi->tgl_mulai));
        $tanggalSelesai = date('d-m-Y', strtotime($simi->tgl_selesai));

        $dataEmail = [
            'namaPenerima' => $receiver->nama,
            'noSIMI' => $simi->no_simi,
            'tanggalSIMI' => $tanggalSIMI,
            'periodeSIMI' => "$tanggalMulai S.d $tanggalSelesai",
            'kegiatan' => $simi->keperluan,
            'instalasi' => $simi->nama_instalasi,
            'namaArea' => $simi->nama_area,
            'alasan' => $request->catatan,
        ];

//        Mail::send('mail.notifikasi_rejected', $dataEmail, function ($message) use ($receiver) {
//            $message->to($receiver->email, $receiver->nama)
//                ->subject('[REJECTED] Surat Izin Masuk Instalasi (SIMI)')
//                ->from('pgn.no.reply@pertamina.com', 'e-SIMI');
//        });

        try {
            SIMI::where('simi_id', $logID)->update($dataReject);
            Log::create($dataLog);
            return redirect()->route('tasklist')->with('berhasil', 'Surat Izin Masuk Instalasi berhasil ditolak');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Surat Izin Masuk Instalasi gagal ditolak');
        }
    }
}
