<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\PIC;
use App\Models\User;
use App\Models\Wilayah;
use Illuminate\Http\Request;
use DB;
use Exception;

class AreaController extends Controller
{
    public function index(){
        $data = Area::orderBy('nama_wilayah','ASC')->get();
        $wilayah = Wilayah::orderBy('nama_wilayah','ASC')->get();
        $approver = User::where('role', 'Approver')->orderBy('nama','ASC')->get();
        //$approver = DB::select("select * from tbl_auth where user_id not in (select user_id from tbl_pic) and role = 'Approver' order by nama asc");
        return view('master.indexArea')
            ->with([
                'dataArea' => $data,
                'dataWilayah' => $wilayah,
                'dataApprover' => $approver,
            ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'namaWilayah' => 'required',
            'namaArea' => 'required',
        ]);

        $dataArea = [
            'nama_wilayah' => $request->namaWilayah,
            'nama_area' => $request->namaArea,
        ];

        try {
            Area::create($dataArea);
            return redirect()->back()->with('berhasil', 'Area baru berhasil disimpan');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Area baru gagal disimpan');
        }
    }

    public function update(Request $request)
    {
        try {
            $logID = decrypt($request->areaID);
        } catch (Exception $e) {
            abort(404);
        }

        $this->validate($request, [
            'namaWilayah' => 'required',
            'namaArea' => 'required',
        ]);

        $dataArea = [
            'nama_wilayah' => $request->namaWilayah,
            'nama_area' => $request->namaArea,
        ];

        try {
            Area::where('area_id', $logID)->update($dataArea);
            return redirect()->back()->with('berhasil', 'Data area berhasil diubah');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Data area gagal diubah');
        }
    }

    public function delete($areaID)
    {
        try {
            $logID = decrypt($areaID);
        } catch (Exception $e) {
            abort(404);
        }

        Area::where('area_id', $logID)->delete();
        return redirect()->back()->with('berhasil', 'Data area berhasil dihapus');
    }

    public function addPIC($areaID, $userID)
    {
        $dataAll = User::whereIn('user_id', explode(",", $userID))->get();

        foreach ($dataAll as $approver) {
            $data[] = [
                [
                    $dataPIC = [
                        'area_id' => decrypt($areaID),
                        'user_id' => $approver->user_id,
                        'nama_pic' => $approver->nama,
                        'jabatan_pic' => $approver->jabatan,
                        'email_pic' => $approver->email,
                    ],

                    #INSERT PIC
                    PIC::create($dataPIC),
                ]
            ];
        }
        return redirect()->back()->with('berhasil', 'Penanggung jawab area/zona berhasil ditambahkan');
    }

    public function deletePIC($picID)
    {
        try {
            $logID = decrypt($picID);
        } catch (Exception $e) {
            abort(404);
        }

        PIC::where('pic_id', $logID)->delete();
        return redirect()->back()->with('berhasil', 'Penanggung jawab area/zona berhasil dihapus');
    }

}
