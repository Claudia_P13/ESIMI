<?php

namespace App\Http\Controllers;

use App\Models\DetailKegiatan;
use App\Models\DetailLokasiKegiatan;
use App\Models\DetailPengikut;
use App\Models\Instalasi;
use App\Models\Kegiatan;
use App\Models\Kelayakan;
use App\Models\Lampiran;
use App\Models\Log;
use App\Models\LokasiKegiatan;
use App\Models\SIMI;
use App\Models\User;
use App\Models\ViewDetailKegiatan;
use App\Models\ViewDetailLokasi;
use App\Models\ViewInstalasi;
use App\Models\ViewSIMI;
use App\Models\Wilayah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use Mail;

class SIMIController extends Controller
{

    public function index()
    {
        $ket = "All Data";
        $tahun = date("Y");
        $email = session('user')->email;
        $role = session('user')->role;
        $wilayah = Wilayah::orderBy('nama_wilayah','ASC')->get();

        if ($role == 'Admin') {
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_wilayah', 'ORF Muara Karang')->orderBy('simi_id', 'DESC')->get();
        }elseif ($role == 'Admin Instalasi'){
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_instalasi', session('instalasi'))->orderBy('simi_id', 'DESC')->get();
        }elseif ($role == 'Approver'){
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_area', session('area'))->orderBy('simi_id', 'DESC')->get();
        }else{
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('email_pemohon', $email)->orderBy('simi_id', 'DESC')->get();
        }
        return view('transaksi.indexSIMI')
            ->with([
                'ket' => $ket,
                'tahun' => $tahun,
                'dataSIMI' => $simi,
                'dataWilayah' => $wilayah,
            ]);
    }

    public function postYearSIMI(Request $request)
    {
        $this->validate($request, [
            'tahun' => 'required',
        ]);

        return redirect(route('indexYearSIMI', ['year' => encrypt($request->tahun)]));
    }

    public function indexTahun($year)
    {
        try {
            $tahun = decrypt($year);
        } catch (Exception $e) {
            abort(404);
        }

        $ket = "All Data";
        $email = session('user')->email;
        $role = session('user')->role;
        $wilayah = Wilayah::orderBy('nama_wilayah','ASC')->get();

        if ($role == 'Admin'){
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_wilayah', 'Operation & Maintenance Management')->orderBy('simi_id', 'DESC')->get();
        }elseif ($role == 'Approver'){
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_area', session('area'))->orderBy('simi_id', 'DESC')->get();
        }elseif ($role == 'Admin Instalasi'){
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_instalasi', session('instalasi'))->orderBy('simi_id', 'DESC')->get();
        }else{
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('email_pemohon', $email)->orderBy('simi_id', 'DESC')->get();
        }

        return view('transaksi.indexSIMI')
            ->with([
                'ket' => $ket,
                'tahun' => $tahun,
                'dataSIMI' => $simi,
                'dataWilayah' => $wilayah,
            ]);
    }

    public function indexToday()
    {
        $tahun = date("Y");
        $tanggal = date("Y-m-d");

        $ket = "Today";
        $email = session('user')->email;
        $role = session('user')->role;
        $wilayah = Wilayah::orderBy('nama_wilayah','ASC')->get();

        if ($role == 'Admin'){
            $simi = ViewSIMI::whereDate('tgl_simi', $tanggal)->orderBy('simi_id', 'DESC')->get();
        }elseif ($role == 'Approver'){
            $simi = ViewSIMI::whereDate('tgl_simi', $tanggal)->where('nama_area', session('area'))->orderBy('simi_id', 'DESC')->get();
        }elseif ($role == 'Admin Instalasi'){
            $simi = ViewSIMI::whereDate('tgl_simi', $tanggal)->where('nama_instalasi', session('instalasi'))->orderBy('simi_id', 'DESC')->get();
        }else{
            $simi = ViewSIMI::whereDate('tgl_simi', $tanggal)->where('email_pemohon', $email)->orderBy('simi_id', 'DESC')->get();
        }

        return view('transaksi.indexSIMI')
            ->with([
                'ket' => $ket,
                'tahun' => $tahun,
                'tanggal' => $tanggal,
                'dataSIMI' => $simi,
                'dataWilayah' => $wilayah,
            ]);
    }

    public function postMonthlySIMI(Request $request)
    {
        $this->validate($request, [
            'bulan1' => 'required',
            'bulan2' => 'required',
        ]);

        return redirect()->route('indexMonthlySIMI', ['bulan1' => encrypt($request->bulan1), 'bulan2' => encrypt($request->bulan2), 'year' => encrypt($request->tahun)]);
    }

    public function indexMonthly($bulan1, $bulan2, $year)
    {
        try {
            $tahun = decrypt($year);
            $bln1 = decrypt($bulan1);
            $bln2 = decrypt($bulan2);
        } catch (Exception $e) {
            abort(404);
        }

        $ket = "Monthly";
        $email = session('user')->email;
        $role = session('user')->role;
        $wilayah = Wilayah::orderBy('nama_wilayah','ASC')->get();

        if ($role == 'Admin'){
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->whereBetween('bulan', [$bln1, $bln2])->orderBy('simi_id', 'DESC')->get();
        }elseif ($role == 'Approver'){
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->whereBetween('bulan', [$bln1, $bln2])->where('nama_area', session('area'))->orderBy('simi_id', 'DESC')->get();
        }elseif ($role == 'Admin Instalasi'){
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->whereBetween('bulan', [$bln1, $bln2])->where('nama_instalasi', session('instalasi'))->orderBy('simi_id', 'DESC')->get();
        }else{
            $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->whereBetween('bulan', [$bln1, $bln2])->where('email_pemohon', $email)->orderBy('simi_id', 'DESC')->get();
        }

        return view('transaksi.indexSIMI')
            ->with([
                'ket' => $ket,
                'tahun' => $tahun,
                'bulan1' => $bln1,
                'bulan2' => $bln2,
                'dataSIMI' => $simi,
                'dataWilayah' => $wilayah,
            ]);
    }

    public function postCustomSIMI(Request $request)
    {
        $this->validate($request, [
            'tanggal1' => 'required',
            'tanggal2' => 'required',
        ]);

        return redirect()->route('indexCustomSIMI', ['tanggal1' => encrypt($request->tanggal1), 'tanggal2' => encrypt($request->tanggal2)]);
    }

    public function indexCustom($tanggal1, $tanggal2)
    {
        try {
            $date1 = decrypt($tanggal1);
            $date2 = decrypt($tanggal2);
        } catch (Exception $e) {
            abort(404);
        }

        $tgl1 = date("Y-m-d", strtotime($date1));
        $tgl2 = date("Y-m-d", strtotime($date2));

        $tahun = date("Y", strtotime($date1));

        $ket = "Custom";
        $email = session('user')->email;
        $role = session('user')->role;
        $wilayah = Wilayah::orderBy('nama_wilayah','ASC')->get();

        if ($role == 'Admin'){
            $simi = ViewSIMI::whereBetween('tanggal', [$tgl1, $tgl2])->orderBy('simi_id', 'DESC')->get();
        }elseif ($role == 'Approver'){
            $simi = ViewSIMI::whereBetween('tanggal', [$tgl1, $tgl2])->where('nama_area', session('area'))->orderBy('simi_id', 'DESC')->get();
        }elseif ($role == 'Admin Instalasi'){
            $simi = ViewSIMI::whereBetween('tanggal', [$tgl1, $tgl2])->where('nama_instalasi', session('instalasi'))->orderBy('simi_id', 'DESC')->get();
        }else{
            $simi = ViewSIMI::whereBetween('tanggal', [$tgl1, $tgl2])->where('email_pemohon', $email)->orderBy('simi_id', 'DESC')->get();
        }

        return view('transaksi.indexSIMI')
            ->with([
                'ket' => $ket,
                'tahun' => $tahun,
                'tanggal1' => $tgl1,
                'tanggal2' => $tgl2,
                'dataSIMI' => $simi,
                'dataWilayah' => $wilayah,
            ]);
    }

    public function postWilayahSIMI(Request $request)
    {
        $this->validate($request, [
            'tahun' => 'required',
            'namaWilayah' => 'required',
            'namaArea' => 'required',
        ]);

        return redirect(route('indexWilayahSIMI', ['year' => encrypt($request->tahun), 'wilayah' => $request->namaWilayah, 'area' => encrypt($request->namaArea)]));
    }

    public function indexWilayah($year, $wilayah, $area)
    {
        try {
            $tahun = decrypt($year);
            $zona = decrypt($area);
        } catch (Exception $e) {
            abort(404);
        }

        $ket = "Wilayah";
        $email = session('user')->email;
        $role = session('user')->role;
        $dataWilayah = Wilayah::orderBy('nama_wilayah','ASC')->get();

        if ($role == 'Admin'){
            if ($zona == 'All Area/Zona'){
                $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_wilayah', $wilayah)->orderBy('simi_id', 'DESC')->get();
            }else{
                $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_wilayah', $wilayah)->where('nama_area', $zona)->orderBy('simi_id', 'DESC')->get();
            }
        }else{
            if ($zona == 'All Area/Zona'){
                $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_wilayah', $wilayah)->where('nama_area', $zona)->where('email_pemohon', $email)->orderBy('simi_id', 'DESC')->get();
            }else{
                $simi = ViewSIMI::whereYear('tgl_simi', $tahun)->where('nama_wilayah', $wilayah)->where('email_pemohon', $email)->orderBy('simi_id', 'DESC')->get();
            }
        }

        return view('transaksi.indexSIMI')
            ->with([
                'ket' => $ket,
                'tahun' => $tahun,
                'wilayah' => $wilayah,
                'area' => $zona,
                'dataSIMI' => $simi,
                'dataWilayah' => $dataWilayah,
            ]);
    }

    public function agreement()
    {
        $instalasi = ViewInstalasi::orderBy('nama_instalasi', 'ASC')->get();
        return view('transaksi.agreement')
            ->with([
                'dataInstalasi' => $instalasi,
            ]);
    }

    public function create(Request $request)
    {
        try {
            $logID = decrypt($request->instalasi);
        } catch (Exception $e) {
            abort(404);
        }

        $this->validate($request, [
            'instalasi' => 'required',
        ]);

        return redirect()->route('inputSIMI', encrypt($logID));
    }

    public function inputSIMI($instalasiID)
    {
        try {
            $logID = decrypt($instalasiID);
        } catch (Exception $e) {
            abort(404);
        }

        $instalasi = ViewInstalasi::where('instalasi_id', $logID)->first();
        return view('transaksi.inputSIMI')
            ->with([
                'data' => $instalasi,
            ]);
    }

    public function store(Request $request)
    {
        try {
            $instalasiID = decrypt($request->instalasiID);
        } catch (Exception $e) {
            abort(404);
        }

        $this->validate($request, [
            'tanggalMulai' => 'required|date_format:d-m-Y',
            'tanggalSelesai' => 'required|date_format:d-m-Y',
            'namaKegiatan' => 'required',
            'lampiran' => 'required',
        ]);

        $tahun = date("Y");
        $tanggalMenit = date("Y-m-d H:i:s");

        $tglMulai = date("Y-m-d", strtotime($request->tanggalMulai));
        $tglSelesai = date("Y-m-d", strtotime($request->tanggalSelesai));

        #Validasi Tanggal
        $tanggal1 = new \DateTime($tglMulai);
        $tanggal2 = new \DateTime($tglSelesai);

        $interval = date_diff($tanggal1, $tanggal2);
        $hari = $interval->format('%R%a');
        $jumlahHari = $hari + 1;

        if ($jumlahHari > 7){
            return redirect()->back()->with('peringatan', 'Kegiatan/pekerjaan maksimal 7 hari');
        }

        if ($jumlahHari <= 0 ){
            return redirect()->back()->with('peringatan', 'Tanggal kegiatan tidak valid!');
        }

        $bulanRomawi = array("", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");

        $nomorUrut = DB::table('tbl_simi')
            ->select(DB::raw('count(*) as jumlah'))
            ->whereYear('tgl_simi', $tahun)
            ->first();

        $nomor = $nomorUrut->jumlah + 1;

        if ($nomor < 10 && $nomor >= 0) {
            $nomor = "0000" . $nomor;
        } else if ($nomor < 100 && $nomor >= 10) {
            $nomor = "000" . $nomor;
        } else if ($nomor < 1000 && $nomor >= 100) {
            $nomor = "00" . $nomor;
        } else if ($nomor < 10000 && $nomor >= 1000) {
            $nomor = "0" . $nomor;
        } else {
            $nomor = $nomor;
        }

        $instalasi = ViewInstalasi::where('instalasi_id', $instalasiID)->first();
        $wilayah = Wilayah::where('nama_wilayah', $instalasi->nama_wilayah)->first();

        $noSIMI = $nomor.".SIMI" . "/" .$wilayah->inisial . "/" . $bulanRomawi[date('n')] . "/" . date('Y');

        $dataSIMI = [
            'no_simi' => $noSIMI,
            'tgl_simi' => $tanggalMenit,
            'nama_pemohon' => session('user')->nama,
            'jabatan_pemohon' => session('user')->jabatan,
            'email_pemohon' => session('user')->email,
            'divisi_pemohon' => session('user')->divisi,
            'perusahaan' => session('user')->perusahaan,
            'instalasi_id' => $instalasiID,
            'nama_penanggung_jawab' => $instalasi->nama_pic,
            'jabatan_penanggung_jawab' => $instalasi->jabatan_pic,
            'email_penanggung_jawab' => $instalasi->email_pic,
            'tgl_mulai' => date('Y-m-d', strtotime($request->tanggalMulai)),
            'tgl_selesai' => date('Y-m-d', strtotime($request->tanggalSelesai)),
            'keperluan' => htmlspecialchars($request->namaKegiatan),
            'status' => 'Draft',
        ];

        $dataLog = [
            'no_simi' => $noSIMI,
            'keterangan' => "Create SIMI",
            'created_by' => session('user')->email,
        ];

        SIMI::create($dataSIMI);
        Log::create($dataLog);

        if ($request->hasFile('lampiran')) {

            $image = $request->file('lampiran');
            $name = $image->getClientOriginalName();
            $type = $image->getClientOriginalExtension();
            $featured_new_name = time() . $name ."." . $type;
            $image->move('attachment', $featured_new_name);

            $dataLampiran = [
                'lampiran' => $featured_new_name,
            ];

            SIMI::where('no_simi', $noSIMI)->update($dataLampiran);
        }

        try {
            return redirect()->route('indexSIMI')->with('berhasil', 'Surat izin masuk instalasi berhasil disimpan');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Surat izin masuk instalasi gagal disimpan');
        }
    }

    public function update(Request $request)
    {
        try {
            $logID = decrypt($request->simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $this->validate($request, [
            'tanggalMulai' => 'required|date_format:d-m-Y',
            'tanggalSelesai' => 'required|date_format:d-m-Y',
            'namaKegiatan' => 'required|string',
        ]);

        $tglMulai = date("Y-m-d", strtotime($request->tanggalMulai));
        $tglSelesai = date("Y-m-d", strtotime($request->tanggalSelesai));

        #Validasi Tanggal
        $tanggal1 = new \DateTime($tglMulai);
        $tanggal2 = new \DateTime($tglSelesai);

        $interval = date_diff($tanggal1, $tanggal2);
        $hari = $interval->format('%R%a');
        $jumlahHari = $hari + 1;

        if ($jumlahHari > 7){
            return redirect()->back()->with('peringatan', 'Kegiatan/pekerjaan maksimal 7 hari');
        }

        if ($jumlahHari <= 0 ){
            return redirect()->back()->with('peringatan', 'Tanggal kegiatan tidak valid!');
        }

        $simi = SIMI::where('simi_id', $logID)->first();

        $dataSIMI = [
            'tgl_mulai' => date('Y-m-d', strtotime($request->tanggalMulai)),
            'tgl_selesai' => date('Y-m-d', strtotime($request->tanggalSelesai)),
            'keperluan' => htmlspecialchars($request->namaKegiatan),
        ];

        $dataLog = [
            'no_simi' => $simi->no_simi,
            'keterangan' => "Edit SIMI",
            'created_by' => session('user')->email,
        ];

        try {
            SIMI::where('simi_id', $logID)->update($dataSIMI);
            Log::create($dataLog);
            return redirect()->back()->with('berhasil', 'Surat izin masuk instalasi berhasil diubah');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Surat izin masuk instalasi gagal diubah');
        }
    }

    public function uploadLampiran(Request $request)
    {
        try {
            $simiID = decrypt($request->simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $this->validate($request, [
            'lampiran' => 'required',
        ]);

        $image = $request->file('lampiran');
        $name = $image->getClientOriginalName();
        $type = $image->getClientOriginalExtension();
        $featured_new_name = time() . $name ."." . $type;
        $image->move('attachment', $featured_new_name);

        $dataLampiran = [
            'lampiran' => $featured_new_name,
        ];

        try {
            SIMI::where('simi_id', $simiID)->update($dataLampiran);
            return redirect()->back()->with('berhasil', 'Lampiran pendukung berhasil diunggah');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Lampiran pendukung gagal diunggah');
        }
    }

    public function view($simiID)
    {
        try {
            $logID = decrypt($simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $simi = ViewSIMI::where('simi_id', $logID)->first();

        $instalasi = Instalasi::where('instalasi_id', $logID)->first();
        $detailKegiatan = DetailKegiatan::where('no_simi', $simi->no_simi)->get();
        $jumlahDetailKegiatan = DetailKegiatan::where('no_simi', $simi->no_simi)->count();
        $detailLokasi = DetailLokasiKegiatan::where('no_simi', $simi->no_simi)->get();
        $jumlahDetailLokasi = DetailLokasiKegiatan::where('no_simi', $simi->no_simi)->count();
        $detailPengikut = DetailPengikut::where('no_simi', $simi->no_simi)->get();
        $jumlahDetailPengikut = DetailPengikut::where('no_simi', $simi->no_simi)->count();
        $dok = ViewDetailKegiatan::where('no_simi', $simi->no_simi)->max('dokumen');

        $tglAwal = date("Y-m-d", strtotime($simi->tgl_mulai));
        $tglAkhir = date("Y-m-d", strtotime($simi->tgl_selesai));

        #Validasi Tanggal
        $tanggal1 = new \DateTime($tglAwal);
        $tanggal2 = new \DateTime($tglAkhir);

        $interval = date_diff($tanggal1, $tanggal2);
        $hari = $interval->format('%R%a');
        $jumlahHari = $hari + 1;

        $dataLog = Log::where('no_simi', $simi->no_simi)->get();
        $jumlahLog = Log::where('no_simi', $simi->no_simi)->count();

        return view('transaksi.detailSIMI')
            ->with([
                'data' => $simi,
                'dataInstalasi' => $instalasi,
                'jumlahHari' => $jumlahHari,
                'dataDetailKegiatan' => $detailKegiatan,
                'jumlahDetailKegiatan' => $jumlahDetailKegiatan,
                'dataDetailLokasi' => $detailLokasi,
                'jumlahDetailLokasi' => $jumlahDetailLokasi,
                'dataDetailPengikut' => $detailPengikut,
                'jumlahDetailPengikut' => $jumlahDetailPengikut,
                'dok' => $dok,
                'dataLog' => $dataLog,
                'jumlahLog' => $jumlahLog,
            ]);
    }

    public function addKegiatan($simiID, $kegiatanID)
    {
        try {
            $logID = decrypt($simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $simi = ViewSIMI::where('simi_id', $logID)->first();
        $dataAll = Kegiatan::whereIn('kegiatan_id', explode(",", $kegiatanID))->get();

        foreach ($dataAll as $kegiatan) {
            $data[] = [
                [
                    $dataKegiatan = [
                        'no_simi' => $simi->no_simi,
                        'kegiatan' => $kegiatan->nama_kegiatan,
                    ],

                    #INSERT KEGIATAN
                    DetailKegiatan::create($dataKegiatan),
                ]
            ];
        }
        return redirect()->back()->with('berhasil', 'Kegiatan/pekerjaan berhasil ditambahkan');
    }

    public function deleteKegiatan($detailID)
    {
        try {
            $logID = decrypt($detailID);
        } catch (Exception $e) {
            abort(404);
        }

        DetailKegiatan::where('detail_kegiatan_id', $logID)->delete();
        return redirect()->back()->with('berhasil', 'Kegiatan/pekerjaan berhasil dihapus');
    }

    public function addLokasi($simiID, $lokasiID)
    {
        try {
            $logID = decrypt($simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $simi = ViewSIMI::where('simi_id', $logID)->first();
        $dataAll = LokasiKegiatan::whereIn('lokasi_id', explode(",", $lokasiID))->get();

        foreach ($dataAll as $lokasi) {
            $data[] = [
                [
                    $dataLokasi = [
                        'no_simi' => $simi->no_simi,
                        'lokasi' => $lokasi->nama_lokasi,
                    ],

                    #INSERT KEGIATAN
                    DetailLokasiKegiatan::create($dataLokasi),
                ]
            ];
        }
        return redirect()->back()->with('berhasil', 'Lokasi kegiatan berhasil ditambahkan');
    }

    public function deleteLokasi($detailID)
    {
        try {
            $logID = decrypt($detailID);
        } catch (Exception $e) {
            abort(404);
        }

        DetailLokasiKegiatan::where('detail_lokasi_id', $logID)->delete();
        return redirect()->back()->with('berhasil', 'Lokasi kegiatan berhasil dihapus');
    }

    public function storePengikut(Request $request)
    {
        try {
            $logID = decrypt($request->simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $this->validate($request, [
            'nama' => 'required',
            'perusahaan' => 'required',
        ]);

        $simi = ViewSIMI::where('simi_id', $logID)->first();

        $dataPengikut = [
            'no_simi' => $simi->no_simi,
            'nama_pengikut' => ucwords($request->nama),
            'perusahaan' => strtoupper($request->perusahaan),
        ];

        try {
            DetailPengikut::create($dataPengikut);
            return redirect()->back()->with('berhasil', 'Data pengikut berhasil ditambahkan');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Data pengikut gagal ditambahkan');
        }
    }

    public function addPengikut($simiID, $pengikutID)
    {
        try {
            $logID = decrypt($simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $simi = ViewSIMI::where('simi_id', $logID)->first();
        $user = User::where('user_id', $pengikutID)->first();

        $dataPengikut = [
            'no_simi' => $simi->no_simi,
            'user_id' => $user->user_id,
            'nama_pengikut' => $user->nama,
            'perusahaan' => $user->perusahaan,
            'status' => 'Waiting Confirmed',
        ];

        try {
            DetailPengikut::create($dataPengikut);
            return redirect()->back()->with('berhasil', 'Data pengikut berhasil ditambahkan');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Data pengikut gagal ditambahkan');
        }
    }

    public function deletePengikut($detailID)
    {
        try {
            $logID = decrypt($detailID);
        } catch (Exception $e) {
            abort(404);
        }

        DetailPengikut::where('pengikut_id', $logID)->delete();
        return redirect()->back()->with('berhasil', 'Pengikut berhasil dihapus');
    }

    public function submit($simiID)
    {
        try {
            $logID = decrypt($simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $tanggalMenit = date("Y-m-d H:i:s");
        $simi = ViewSIMI::where('simi_id', $logID)->first();

        if (empty($simi->lampiran)) {
            return redirect()->back()->with('peringatan', 'Anda belum melampirkan dokumen persyaratan untuk masuk ke instalasi');
        }

        $kegiatan = DetailKegiatan::where('no_simi', $simi->no_simi)->count();

        if ($kegiatan <= 0) {
            return redirect()->back()->with('peringatan', 'Anda belum menambahkan detail kegiatan/pekerjaan');
        }

        $lokasi = DetailLokasiKegiatan::where('no_simi', $simi->no_simi)->count();

        if ($lokasi <= 0) {
            return redirect()->back()->with('peringatan', 'Anda belum menambahkan detail lokasi');
        }

        $dataSIMI = [
            'status' => 'Submited',
            'submited_date' => $tanggalMenit,
        ];

        $dataLog = [
            'no_simi' => $simi->no_simi,
            'keterangan' => "Submit SIMI ke penanggung jawab area",
            'created_by' => session('user')->email,
        ];

        //SEND EMAIL
        $receiver = User::where('email', $simi->email_penanggung_jawab)->first();

        $tanggalSIMI = date('d-m-Y', strtotime($simi->tgl_simi));
        $tanggalMulai = date('d-m-Y', strtotime($simi->tgl_mulai));
        $tanggalSelesai = date('d-m-Y', strtotime($simi->tgl_selesai));

        $dataEmail = [
            'namaPenerima' => $receiver->nama,
            'noSIMI' => $simi->no_simi,
            'tanggalSIMI' => $tanggalSIMI,
            'periodeSIMI' => "$tanggalMulai s.d $tanggalSelesai",
            'kegiatan' => $simi->keperluan,
            'instalasi' => $simi->nama_instalasi,
            'namaArea' => $simi->nama_area,
        ];

//        Mail::send('mail.notifikasi_approval', $dataEmail, function ($message) use ($receiver) {
//            $message->to($receiver->email, $receiver->nama)
//                ->subject('[APPROVAL] Surat Izin Masuk Instalasi (SIMI)')
//                ->from('pgn.no.reply@pertamina.com', 'e-SIMI');
//        });

        try {
            SIMI::where('simi_id', $logID)->update($dataSIMI);
            Log::create($dataLog);
            return redirect()->back()->with('berhasil', 'Surat izin masuk instalasi berhasil disubmit ke penaggung jawab area');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Surat izin masuk instalasi gagal disubmit ke penaggung jawab area');
        }
    }

    public function resubmit(Request $request)
    {
        try {
            $logID = decrypt($request->simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $this->validate($request, [
            'catatan' => 'required|max:150',
        ]);

        $tanggalMenit = date("Y-m-d H:i:s");
        $simi = ViewSIMI::where('simi_id', $logID)->first();

        $dataSIMI = [
            'status' => 'Submited',
            'submited_date' => $tanggalMenit,
        ];

        $dataLog = [
            'no_simi' => $simi->no_simi,
            'keterangan' => "Resubmit to Approver",
            'created_by' => session('user')->email,
        ];

        //SEND EMAIL
        $receiver = User::where('email', $simi->email_penanggung_jawab)->first();

        $tanggalSIMI = date('d-m-Y', strtotime($simi->tgl_simi));
        $tanggalMulai = date('d-m-Y', strtotime($simi->tgl_mulai));
        $tanggalSelesai = date('d-m-Y', strtotime($simi->tgl_selesai));

        $dataEmail = [
            'namaPenerima' => $receiver->nama,
            'noSIMI' => $simi->no_simi,
            'tanggalSIMI' => $tanggalSIMI,
            'periodeSIMI' => "$tanggalMulai s.d $tanggalSelesai",
            'kegiatan' => $simi->keperluan,
            'instalasi' => $simi->nama_instalasi,
            'namaArea' => $simi->nama_area,
        ];

//        Mail::send('mail.notifikasi_approval', $dataEmail, function ($message) use ($receiver) {
//            $message->to($receiver->email, $receiver->nama)
//                ->subject('[APPROVAL] Surat Izin Masuk Instalasi (SIMI)')
//                ->from('pgn.no.reply@pertamina.com', 'e-SIMI');
//        });

        try {
            SIMI::where('simi_id', $logID)->update($dataSIMI);
            Log::create($dataLog);
            return redirect()->back()->with('berhasil', 'Surat izin masuk instalasi berhasil disubmit ke penaggung jawab area');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Surat izin masuk instalasi gagal disubmit ke penaggung jawab area');
        }
    }

    public function canceled($simiID)
    {
        try {
            $logID = decrypt($simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $tanggalMenit = date("Y-m-d H:i:s");

        $simi = SIMI::where('simi_id', $logID)->first();

        $dataSIMI = [
            'status' => 'Canceled',
            'canceled_date' => $tanggalMenit,
            'canceled_by' => session('user')->user_id,
        ];

        $dataLog = [
            'no_simi' => $simi->no_simi,
            'keterangan' => "Cancel SIMI",
            'created_by' => session('user')->email,
        ];

        try {
            SIMI::where('simi_id', $logID)->update($dataSIMI);
            Log::create($dataLog);
            return redirect()->back()->with('berhasil', 'Surat izin masuk instalasi berhasil dibatalkan');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Surat izin masuk instalasi gagal dibatalkan');
        }
    }

    public function resetStatus($simiID)
    {
        try {
            $logID = decrypt($simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $simi = SIMI::where('simi_id', $logID)->first();

        $dataSIMI = [
            'status' => 'Draft',
        ];

        $dataLog = [
            'no_simi' => $simi->no_simi,
            'keterangan' => "Reset Status",
            'created_by' => session('user')->email,
        ];

        try {
            SIMI::where('simi_id', $logID)->update($dataSIMI);
            Log::create($dataLog);
            return redirect()->back()->with('berhasil', 'Status permohonan berhasil direset');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Status permohonan gagal direset');
        }
    }

    public function closeSIMI(Request $request)
    {
        try {
            $logID = decrypt($request->simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $this->validate($request, [
            'tanggalSelesai' => 'required|date_format:d-m-Y',
            'jamKeluar' => 'required',
        ]);

        $simi = ViewSIMI::where('simi_id', $logID)->first();
        $tglSelesai = date("Y-m-d H:i", strtotime($request->tanggalSelesai." ".$request->jamKeluar));

        $dataApprove = [
            'status' => 'Closed',
            'closed_date' => $tglSelesai,
            'closed_by' => session('user')->email,
        ];

        $dataLog = [
            'no_simi' => $simi->no_simi,
            'keterangan' => "Close SIMI",
            'created_by' => session('user')->email,
        ];

        try {
            SIMI::where('simi_id', $logID)->update($dataApprove);
            Log::create($dataLog);
            return redirect()->back()->with('berhasil', 'Surat Izin Masuk Instalasi berhasil diselesaiakan');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Surat Izin Masuk Instalasi gagal diselesaiakan');
        }
    }

    public function formSIMI($simiID)
    {
        try {
            $logID = decrypt($simiID);
        } catch (Exception $e) {
            abort(404);
        }

        $simi = ViewSIMI::where('simi_id', $logID)->first();
        $instalasi = Instalasi::where('instalasi_id', $logID)->first();
        $detailPengikut = DetailPengikut::where('no_simi', $simi->no_simi)->get();
        $pass = ViewDetailLokasi::where('no_simi', $simi->no_simi)->max('nilai');

        $tglAwal = date("Y-m-d", strtotime($simi->tgl_mulai));
        $tglAkhir = date("Y-m-d", strtotime($simi->tgl_selesai));

        #Validasi Tanggal
        $tanggal1 = new \DateTime($tglAwal);
        $tanggal2 = new \DateTime($tglAkhir);

        $interval = date_diff($tanggal1, $tanggal2);
        $hari = $interval->format('%R%a');
        $jumlahHari = $hari + 1;

        return view('transaksi.formSIMI')
            ->with([
                'data' => $simi,
                'dataInstalasi' => $instalasi,
                'jumlahHari' => $jumlahHari,
                'dataDetailPengikut' => $detailPengikut,
                'pass' => $pass,
            ]);
    }
}
