<?php

namespace App\Http\Controllers;

use App\Models\Instalasi;
use App\Models\PIC;
use App\Models\User;
use App\Models\ViewInstalasi;
use App\Models\Wilayah;
use Illuminate\Http\Request;
use DB;
use Exception;

class InstalasiController extends Controller
{
    public function index(){
        $data = ViewInstalasi::orderBy('nama_instalasi','ASC')->get();
        $wilayah = Wilayah::orderBy('nama_wilayah','ASC')->get();
        $approver = User::where('role', 'Approver')->orderBy('nama','ASC')->get();
        return view('master.indexInstalasi')
            ->with([
                'dataInstalasi' => $data,
                'dataWilayah' => $wilayah,
                'dataApprover' => $approver,
            ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'instalasi' => 'required',
            'alamat' => 'required',
            'namaWilayah' => 'required',
            'namaArea' => 'required',
            'namaPIC' => 'required',
            'jabatanPIC' => 'required',
            'emailPIC' => 'required',
        ]);

        $dataInstalasi = [
            'nama_instalasi' => $request->instalasi,
            'alamat' => $request->alamat,
            'no_telp' => $request->noTelp,
            'nama_wilayah' => $request->namaWilayah,
            'area_id' => decrypt($request->namaArea),
            'nama_pic' => $request->namaPIC,
            'email_pic' => $request->emailPIC,
            'jabatan_pic' => $request->jabatanPIC,
        ];

        try {
            Instalasi::create($dataInstalasi);
            return redirect()->back()->with('berhasil', 'Instalasi berhasil disimpan');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Instalasi baru gagal disimpan');
        }
    }

    public function edit($instalasiID)
    {
        try {
            $logID = decrypt($instalasiID);
        } catch (Exception $e) {
            abort(404);
        }

        $data = ViewInstalasi::where('instalasi_id', $logID)->first();
        $wilayah = Wilayah::orderBy('nama_wilayah','ASC')->get();
        $approver = User::where('role', 'Approver')->orderBy('nama','ASC')->get();
        return view('master.editInstalasi')
            ->with([
                'data' => $data,
                'dataWilayah' => $wilayah,
                'dataApprover' => $approver,
            ]);
    }

    public function update(Request $request)
    {

        try {
            $logID = decrypt($request->instalasiID);
        } catch (Exception $e) {
            abort(404);
        }

        $this->validate($request, [
            'instalasi' => 'required',
            'alamat' => 'required',
            'namaWilayah' => 'required',
            'namaArea' => 'required',
            'namaPIC' => 'required',
            'jabatanPIC' => 'required',
            'emailPIC' => 'required',
        ]);

        $dataInstalasi = [
            'nama_instalasi' => $request->instalasi,
            'alamat' => $request->alamat,
            'no_telp' => $request->noTelp,
            'nama_wilayah' => $request->namaWilayah,
            'area_id' => decrypt($request->namaArea),
            'nama_pic' => $request->namaPIC,
            'email_pic' => $request->emailPIC,
            'jabatan_pic' => $request->jabatanPIC,
        ];

        try {
            Instalasi::where('instalasi_id', $logID)->update($dataInstalasi);
            return redirect()->route('indexInstalasi')->with('berhasil', 'Instalasi berhasil diubah');
        } catch (Exception $e) {
            return redirect()->back()->with('gagal', 'Instalasi gagal diubah');
        }
    }

    public function delete($instalasiID)
    {
        try {
            $logID = decrypt($instalasiID);
        } catch (Exception $e) {
            abort(404);
        }

        Instalasi::where('instalasi_id', $logID)->delete();
        return redirect()->back()->with('berhasil', 'Instalasi berhasil dihapus');
    }
}
