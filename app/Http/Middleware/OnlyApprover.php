<?php

namespace App\Http\Middleware;

use Closure;

class OnlyApprover
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('user')->role == 'Approver' or session('user')->role == 'Admin Instalasi'){
            return $next($request);
        }else{
            abort(403);
        }
    }
}
