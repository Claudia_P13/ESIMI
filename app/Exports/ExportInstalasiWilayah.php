<?php

namespace App\Exports;

use App\Models\Instalasi;
use Maatwebsite\Excel\Concerns\FromCollection;

class ExportInstalasiWilayah implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Instalasi::all();
    }
}
