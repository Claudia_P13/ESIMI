<?php

namespace App\Exports;

use App\Models\ViewSIMI;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class ExportWilayahSIMI implements FromView, ShouldAutoSize
{
    public function __construct(string $tahun, string $wil, string $area)
    {
        $this->tahun = $tahun;
        $this->wilayah = $wil;
        $this->area = $area;
    }

    public function view(): View
    {
        $email = session('user')->email;
        $role = session('user')->role;

        if ($role == 'Admin'){
            if ($this->area == 'All Area/Zona'){
                $simi = ViewSIMI::whereYear('tgl_simi', $this->tahun)->where('nama_wilayah', $this->wilayah)->orderBy('simi_id', 'ASC')->get();
            }else{
                $simi = ViewSIMI::whereYear('tgl_simi', $this->tahun)->where('nama_wilayah', $this->wilayah)->where('nama_area', $this->area)->orderBy('simi_id', 'ASC')->get();
            }
        }else{
            if ($this->area == 'All Area/Zona'){
                $simi = ViewSIMI::whereYear('tgl_simi', $this->tahun)->where('nama_wilayah', $this->wilayah)->where('nama_area', $this->area)->where('email_pemohon', $email)->orderBy('simi_id', 'ASC')->get();
            }else{
                $simi = ViewSIMI::whereYear('tgl_simi', $this->tahun)->where('nama_wilayah', $this->wilayah)->where('email_pemohon', $email)->orderBy('simi_id', 'ASC')->get();
            }
        }

        return view('export.exportIndexSIMI', [
            'dataSIMI' => $simi,
            'tahun' => $this->tahun,
        ]);
    }
}
