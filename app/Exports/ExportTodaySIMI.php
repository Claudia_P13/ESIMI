<?php

namespace App\Exports;

use App\Models\ViewSIMI;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class ExportTodaySIMI implements FromView, ShouldAutoSize
{
    public function __construct(string $year, string $tanggal)
    {
        $this->year = $year;
        $this->tanggal = $tanggal;
    }

    public function view(): View
    {
        $email = session('user')->email;
        $role = session('user')->role;

        if ($role == 'Admin'){
            $simi = ViewSIMI::whereDate('tgl_simi', $this->tanggal)->orderBy('simi_id', 'DESC')->get();
        }else{
            $simi = ViewSIMI::whereDate('tgl_simi', $this->tanggal)->where('email_pemohon', $email)->orderBy('simi_id', 'DESC')->get();
        }

        return view('export.exportIndexSIMI', [
            'dataSIMI' => $simi,
            'tahun' => $this->year,
        ]);
    }
}
