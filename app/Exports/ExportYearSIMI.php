<?php

namespace App\Exports;

use App\Models\ViewSIMI;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class ExportYearSIMI implements FromView, ShouldAutoSize
{
    public function __construct(string $year)
    {
        $this->year = $year;
    }

    public function view(): View
    {
        $email = session('user')->email;
        $role = session('user')->role;

        if ($role == 'Admin'){
            $simi = ViewSIMI::whereYear('tgl_simi', $this->year)->orderBy('simi_id', 'ASC')->get();
        }else{
            $simi = ViewSIMI::whereYear('tgl_simi', $this->year)->where('email_pemohon', $email)->orderBy('simi_id', 'ASC')->get();
        }

        return view('export.exportIndexSIMI', [
            'dataSIMI' => $simi,
            'tahun' => $this->year,
        ]);
    }
}
