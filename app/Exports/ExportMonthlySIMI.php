<?php

namespace App\Exports;

use App\Models\ViewSIMI;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class ExportMonthlySIMI implements FromView, ShouldAutoSize
{
    public function __construct(string $bln1, string $bln2, string $tahun)
    {
        $this->bulan1 = $bln1;
        $this->bulan2 = $bln2;
        $this->tahun = $tahun;
    }

    public function view(): View
    {
        $email = session('user')->email;
        $role = session('user')->role;

        if ($role == 'Admin'){
            $simi = ViewSIMI::whereYear('tgl_simi', $this->tahun)->whereBetween('bulan', [$this->bulan1, $this->bulan2])->orderBy('simi_id', 'DESC')->get();
        }else{
            $simi = ViewSIMI::whereYear('tgl_simi', $this->tahun)->whereBetween('bulan', [$this->bulan1, $this->bulan2])->where('email_pemohon', $email)->orderBy('simi_id', 'DESC')->get();
        }

        return view('export.exportIndexSIMI', [
            'dataSIMI' => $simi,
            'tahun' => $this->tahun,
        ]);
    }
}
