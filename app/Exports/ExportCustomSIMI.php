<?php

namespace App\Exports;

use App\Models\ViewSIMI;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class ExportCustomSIMI implements FromView, ShouldAutoSize
{
    public function __construct(string $date1, string $date2)
    {
        $this->tanggal1 = $date1;
        $this->tanggal2 = $date2;
    }

    public function view(): View
    {
        $tahun = date("Y", strtotime($this->tanggal1));
        $email = session('user')->email;
        $role = session('user')->role;

        if ($role == 'Admin'){
            $simi = ViewSIMI::whereBetween('tanggal', [$this->tanggal1, $this->tanggal2])->orderBy('simi_id', 'ASC')->get();
        }else{
            $simi = ViewSIMI::whereBetween('tanggal', [$this->tanggal1, $this->tanggal2])->where('email_pemohon', $email)->orderBy('simi_id', 'ASC')->get();
        }

        return view('export.exportIndexSIMI', [
            'dataSIMI' => $simi,
            'tahun' => $tahun,
        ]);
    }
}
