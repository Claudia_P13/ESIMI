<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = "tbl_log";
    protected $primaryKey = "log_id";
    protected $guarded = ["log_id"];
}
