<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailKegiatan extends Model
{
    protected $table = "tbl_detail_kegiatan";
    protected $primaryKey = "detail_kegiatan_id";
    protected $guarded = ["detail_kegiatan_id"];
}
