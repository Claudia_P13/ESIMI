<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wilayah extends Model
{
    protected $table = "tbl_wilayah";
    protected $primaryKey = "wilayah_id";
    protected $guarded = ["wilayah_id"];
}
