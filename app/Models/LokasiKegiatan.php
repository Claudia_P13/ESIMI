<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LokasiKegiatan extends Model
{
    protected $table = "tbl_lokasi_kegiatan";
    protected $primaryKey = "lokasi_id";
    protected $guarded = ["lokasi_id"];
}
