<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailLokasiKegiatan extends Model
{
    protected $table = "tbl_detail_lokasi";
    protected $primaryKey = "detail_lokasi_id";
    protected $guarded = ["detail_lokasi_id"];
}
