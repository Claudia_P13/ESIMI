<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PIC extends Model
{
    protected $table = "tbl_pic";
    protected $primaryKey = "pic_id";
    protected $guarded = ["pic_id"];
}
