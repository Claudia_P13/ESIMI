<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = "tbl_auth";
    protected $primaryKey = "user_id";
    protected $guarded = ["user_id"];
}

