<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instalasi extends Model
{
    protected $table = "tbl_instalasi";
    protected $primaryKey = "instalasi_id";
    protected $guarded = ["instalasi_id"];
}
