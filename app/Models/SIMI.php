<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SIMI extends Model
{
    protected $table = "tbl_simi";
    protected $primaryKey = "simi_id";
    protected $guarded = ["simi_id"];
}
