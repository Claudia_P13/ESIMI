<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailPengikut extends Model
{
    protected $table = "tbl_detail_pengikut";
    protected $primaryKey = "pengikut_id";
    protected $guarded = ["pengikut_id"];
}
