<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Perusahaan extends Model
{
    protected $table = "tbl_perusahaan";
    protected $primaryKey = "perusahaan_id";
    protected $guarded = ["perusahaan_id"];
}
